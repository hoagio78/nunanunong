var GameLaucherStatus = GameLaucherStatus || {};
GameLaucherStatus.GetUpdate = 0;
GameLaucherStatus.TestVersion = 1;
GameLaucherStatus.TestHashFiles = 2;
GameLaucherStatus.Updating = 3;
GameLaucherStatus.UpdateFailure = 4;
GameLaucherStatus.LoadResource = 5;
GameLaucherStatus.LoadScript = 6;
GameLaucherStatus.LoadAndroidExt = 7;
GameLaucherStatus.Finished = 8;

var LoadingScene = cc.Scene.extend({
    ctor : function () {
        this._super();
        var thiz = this;

        var bg = new cc.Sprite("core/res/LoadingScene_bg_0.jpg");
        bg.setPosition(cc.p(cc.winSize.width/2, cc.winSize.height/2));
        this.addChild(bg);
        if(cc.winSize.width > bg.width){
            bg.setScaleX(cc.winSize.width / bg.width);
        }

        var rootLayer = new cc.Node();
        rootLayer.setAnchorPoint(cc.p(0.5,0.5));
        rootLayer.setContentSize(cc.size(1280,720));
        rootLayer.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        this.addChild(rootLayer);

        var logo = new cc.Sprite("core/res/LoadingScene_logo.png");
        logo.setPosition(628,452);
        rootLayer.addChild(logo);

        var loadingBarBg = new cc.Sprite("core/res/LoadingScene_bar_1.png");
        loadingBarBg.setPosition(cc.p(640, 170));
        rootLayer.addChild(loadingBarBg);

        var loadingBar = new cc.ProgressTimer(new cc.Sprite("core/res/LoadingScene_bar_2.png"));
        loadingBar.setType(cc.ProgressTimer.TYPE_BAR);
        loadingBar.setBarChangeRate(cc.p(1.0,0.0));
        loadingBar.setMidpoint(cc.p(0.0, 0.5));
        loadingBar.setPosition(loadingBarBg.getPosition());
        rootLayer.addChild(loadingBar);
        this.loadingBar = loadingBar;
        loadingBar.setPercentage(0.0);

        var gemIcon =  new cc.Sprite("core/res/LoadingScene_gem_logo.png");
        gemIcon.setPosition(532,207);
        rootLayer.addChild(gemIcon);

        var label = new cc.LabelBMFont("L...", "core/res/LoadingScene_font.fnt");
        label.setAnchorPoint(cc.p(0.0, 0.5));
        label.setPosition(554,207);
        rootLayer.addChild(label);
        this.title = label;

        this.perLabel = label;
    },

    _startLaucher: function () {
        this._perLoaded = 0;
        this._perLoadedTarget = 0;

        this._finishedLoaded = false;
        this.__setProcess(0);
        this.__setStatus("Loading...");
        SystemPlugin.getInstance().startLaucher();
    },

    _retryLoadModule: function (moduleName) {
        var module = ModuleManager.getInstance().getModule(moduleName);
        module.loadModule();
    },

    update : function (dt) {
        if(this._finishedLoaded){
            if(this._perLoaded > 100){
                this._finishedLoaded = false;
                setTimeout(function () {
                    window._cc_finished_Loading();
                }, 100);
            }
            else{
                var per = this._perLoaded + dt * 100;
                this.__setProcess(per);
            }
        }
        else{
            if(this._perLoaded < this._perLoadedTarget){
                var nextPer = this._perLoaded + dt * 100;
                if(nextPer > this._perLoadedTarget){
                    nextPer = this._perLoadedTarget;
                }
                this.__setProcess(nextPer);
            }
        }
    },

    onEnter : function () {
        this._super();
        GlobalEvent.getInstance().addListener("onUpdateModule", this.onUpdateModule, this);
        GlobalEvent.getInstance().addListener("onLoadModule", this.onLoadModule, this);
        GlobalEvent.getInstance().addListener("onLoadModuleStatus", this.onLoadModuleStatus, this);

        this._startLaucher();
        this.scheduleUpdate();
    },

    onExit : function () {
        this._super();
        GlobalEvent.getInstance().removeListener(this);
        this.unscheduleUpdate();
    },

    onUpdateModule : function (name, data) {
        var current = data["current"];
        var target = data["target"];
        if(data["module"] === "main"){
            var per = 25 * (current / target);
        }
        else{
            var _perModule = 50.0 / this.moduleReady.length;
            var _perStep = _perModule * 0.5;
            var _perLoaded = 50.0 + (this.moduleIndex - 1) *  _perStep;
            var per = (current / target) * _perStep  + _perLoaded;
        }
        this._perLoadedTarget = per;
    },

    onLoadModule : function (name, data) {
        var current = data["current"];
        var target = data["target"];
        if(data["module"] === "main"){
            var per = 25 * (current / target) + 25;
        }
        else{
            var _perModule = 50.0 / this.moduleReady.length;
            var _perStep = _perModule * 0.5;
            var _perLoaded = 50.0 + (this.moduleIndex-1) *  _perStep + (_perModule - _perStep);
            var per = (current / target) * _perStep  + _perLoaded;


        }

        this._perLoadedTarget = per;
    },

    onLoadModuleStatus : function (name, data) {
        var status = data["status"];
        if(status === ModuleStatus.UpdateFailure){
            this.__setStatus("Tải thất bại");
            this._retryLoadModule(data["module"]);
        }
        if(data["module"] === "main"){
            if(status === ModuleStatus.LoadResourceFinished){
                this._getAllReadyModule();
            }
        }
        else{
            if(status === ModuleStatus.LoadResourceFinished){
                this._loadReadyModule();
            }
        }
    },

    _getAllReadyModule : function () {
        if(ModuleManager.getInstance().getReadyModule){
            this.moduleReady = ModuleManager.getInstance().getReadyModule();
        }
        else{
            this.moduleReady = [];
        }

        this.moduleIndex = 0;
        if(this.moduleReady.length > 0){
            this._loadReadyModule();
        }
        else{
            this._finishedLoaded = true;
        }
    },

    _loadReadyModule : function () {
        if(this.moduleIndex >= this.moduleReady.length){
            this._finishedLoaded = true;
        }
        else{
            this.moduleIndex++;
            this.moduleReady[this.moduleIndex-1].loadModule();
        }
    },

    onProcessStatus : function (status) {
        if(status === 4){ //failure
            this.__setStatus("Tải thất bại");
            this._startLaucher();
        }
    },

    onStartLoadModule : function () {
        //this._getAllReadyModule();
    },

    __setProcess : function (per) {
        this._perLoaded = per;
        if(per > 100){
            per = 100;
        }
        this.loadingBar.setPercentage(per);
        this.perLabel.setString("L... " + Math.round(per) + "%");
    },

    __setStatus : function (status) {
        this.title.setString("L...");
    }
});