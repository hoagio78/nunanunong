/**
 * Created by cocos2d on 6/24/2016.
 */

/* lobby */
socket.LobbyClient.NotConnection = 0;
socket.LobbyClient.Connecting = 1;
socket.LobbyClient.Connected = 2;
socket.LobbyClient.ConnectFailure = 3;
socket.LobbyClient.LostConnection = 4;
socket.LobbyClient.Closed = 5;

socket.LobbyClient.prototype._ctor = function () {
    this.initClient();
    return true;
};

socket.LobbyClient.prototype.nativeOnEvent = function () {
    this.onEvent.apply(this, arguments);
};