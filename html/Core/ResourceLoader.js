
var ResourceLoader = cc.Class.extend({
    ctor: function () {
        this._resouces = [];
        this._textures = [];
        this._spineData = [];
        this._module = [];
        this._autoRetry = false;
    },

    addModule: function (module) {
        var res = module._resouces;
        for(var i=0;i<res.length;i++){
            if(this._resouces.indexOf(res[i]) === -1){
                this._resouces.push(res[i]);
            }
        }

        var tex = module._textures;
        for(var i=0;i<tex.length;i++){
            if(this._textures.indexOf(tex[i]) === -1){
                this._textures.push(tex[i]);
            }
        }

        var spine = module._spineData;
        for(var i=0;i<spine.length;i++){
            if(this._spineData.indexOf(spine[i]) === -1){
                this._spineData.push(spine[i]);
            }
        }

        this._module.push(module);
    },

    _updateLoadResources : function () {
        if(!this._resouces || this._resouces.length === 0){
            this._status = ResourceLoader.OnLoadTexture;
            return;
        }

        this._status = ResourceLoader.OnWaitingLoadResources;
        var thiz = this;
        var current = 0;
        var target = this._resouces.length;
        for(var i=0;i<this._resouces.length;i++){
            (function () {
                var res = thiz._resouces[i];
                var cb = function (success) {
                    if(success){
                        thiz._currentStep++;
                        thiz._onLoadProcess();
                    }
                    else{
                        if(thiz._autoRetry){
                            thiz.onLoadFailureWithAutoRetry();
                            setTimeout(function () {
                                ResourceLoader.load(res, cb);
                            }, 1000);
                            return;
                        }
                    }

                    current++;
                    if(current === target){ //finished
                        if(thiz._currentStep < thiz._resouces.length){
                            thiz.itemLoaded = 0;
                            thiz._status = ResourceLoader.OnLoadFinished;
                        }
                        else{
                            thiz.itemLoaded = 0;
                            thiz._status = ResourceLoader.OnLoadTexture;
                        }
                    }
                };
                ResourceLoader.load(res, cb);
            })();
        }
    },

    _updateLoadTexture : function () {
        if(this.itemLoaded >= this._textures.length){
            this.itemLoaded = 0;
            this._status = ResourceLoader.OnLoadSpine;
        }
        else{
            var step = 10;
            var maxStep = this._textures.length - this.itemLoaded;
            if(step >  maxStep){
                step = maxStep;
            }

            for(var i=0;i<step;i++){
                var texture = this._textures[this.itemLoaded + i];
                if(texture["plist"]){
                    cc.spriteFrameCache.addSpriteFrames(texture["plist"], texture["img"]);
                }
            }

            this.itemLoaded += step;
            this._currentStep += step;
            this._onLoadProcess();
        }
    },

    _updateLoadSpine : function () {
        if(this.itemLoaded >= this._spineData.length){
            this.itemLoaded = 0;
            this._status = ResourceLoader.OnLoadFinished;
        }
        else{
            var step = 10;
            var maxStep = this._spineData.length - this.itemLoaded;
            if(step >  maxStep){
                step = maxStep;
            }

            for(var i=0;i<step;i++){
                var spine = this._spineData[this.itemLoaded + i];
                spine._load();
            }

            this.itemLoaded += step;
            this._currentStep += step;
            this._onLoadProcess();
        }
    },

    _updateLoadFinished : function () {
        cc.director.getScheduler().unscheduleUpdate(this);
        if(this._currentStep < this._targetStep){
            this.onLoadFailure();
        }
        else{
            for(var i=0;i<this._module.length;i++){
                this._module[i]._moduleLoaed = true;
            }
            this.onLoadSuccess();
        }
    },

    _onLoadProcess : function () {
        this.onLoadProcess(this._currentStep, this._targetStep);
    },

    update : function () {
        switch (this._status){
            case (ResourceLoader.OnLoadResources) : {
                this._updateLoadResources();
                break;
            }
            case (ResourceLoader.OnLoadTexture) : {
                this._updateLoadTexture();
                break;
            }
            case (ResourceLoader.OnLoadSpine) : {
                this._updateLoadSpine();
                break;
            }
            case (ResourceLoader.OnLoadFinished) : {
                this._updateLoadFinished();
                break;
            }
        }
    },

    load : function () {
        this._targetStep = this._resouces.length;
        this._targetStep += this._textures.length;
        this._targetStep += this._spineData.length;
        this._currentStep = 0;

        this._status = LaucherStatus.OnLoadResources;
        cc.director.getScheduler().scheduleUpdate(this, 0, false);
    },

    onLoadProcess : function (current, target) {},
    onLoadSuccess : function () {},
    onLoadFailure : function () {},
    onLoadFailureWithAutoRetry : function () {}
});

ResourceLoader.OnLoadResources = 1;
ResourceLoader.OnLoadTexture = 2;
ResourceLoader.OnLoadSpine = 3;
ResourceLoader.OnLoadFinished = 4;

(function () {
    if (cc.sys.browserType === cc.sys.BROWSER_TYPE_FIREFOX) {
        window.ENABLE_IMAEG_POOL = false;
    }

    /****/
    var realUrlRegExp = new RegExp("^(?:https?|ftp)://\\S*$", "i");
    var HttpDownloader = cc.Class.extend({
        ctor: function (host) {
            this._host = host;
            this._currentThread = 0;
            this._maxThread = 6;
            this._penddingUrl = [];
        },

        _loadRes: function(url){
            var realUrl = url;
            var mapping = false;

            if(this._host && !realUrlRegExp.test(url)){
                realUrl = this.getRealUrl(url);
                mapping = true;
            }

            var thiz = this;
            thiz._currentThread++;
            cc.loader.load(realUrl,
                function () {},
                function (err) {
                    if(err){
                        thiz.onLoadFinished(url, false);
                    }
                    else{
                        if(mapping){
                            cc.loader.cache[url] = cc.loader.cache[thiz.getRealUrl(url)];
                        }
                        thiz.onLoadFinished(url, true);
                    }
                    thiz._currentThread--;
                    if(thiz._penddingUrl.length > 0){
                        var nextUrl = thiz._penddingUrl[0];
                        thiz._penddingUrl.splice(0, 1);
                        thiz._loadRes(nextUrl);
                    }
                });
        },

        getRealUrl: function(url){
            if(this._host){
                return (this._host + url);
            }
            return url;
        },

        download: function (url) {
            if(this._currentThread >= this._maxThread){
                this._penddingUrl.push(url);
                return;
            }
            this._loadRes(url);
        },

        onLoadFinished: function (url, success) {}
    });

    /***/
    var _request = {};
    var _cache = {};
    var _downloader = [];
    var realDownloader = new HttpDownloader();
    _downloader.push(realDownloader);

    if(window._multiHost) {
        var multiHost = window._multiHost;
        for (var i = 0; i < multiHost.length; i++) {
            var host = multiHost[i];
            if (host) {
                if (host.endsWith("/")) {
                    host = host.substr(0, host.length - 1);
                }
                _downloader.push(new HttpDownloader(host + window.location.pathname));
            }
        }
    }

    //initCallback
    realDownloader.onLoadFinished = function (url, success) {
        if(success){
            _cache[url] = cc.loader.cache[url];
        }
        for(var i=0;i<_request[url].length;i++){
            _request[url][i](success);
        }
        _request[url] = undefined;
    };

    for(var i=1;i<_downloader.length;i++){
        _downloader[i].onLoadFinished = function (url, success) {
            if(success){
                _cache[url] = cc.loader.cache[url];
                _request[url] = undefined;
            }
            else{
                setTimeout(function () {
                    realDownloader.download(url);
                }, 0);
            }
        };
    }

    var takeDownloader = function () {
        var downloader = _downloader[0];
        for(var i=1;i<_downloader.length;i++){
            if(_downloader[i]._penddingUrl.length < downloader._penddingUrl.length){
                downloader = _downloader[i];
            }
        }
        return downloader;
    };

    var mLoader = function (resPath, cb) {
        if(cc.isArray(resPath)){
            var isOk = 0;
            var loaded = 0;
            var finisedFunc = function (isSuccess) {
                loaded++;
                if(isSuccess){
                    isOk++;
                }
                if(loaded === resPath.length){
                    cb(loaded === isOk);
                }
            };
            for(var i=0;i<resPath.length;i++){
                mLoader(resPath[i], function (ok) {
                    finisedFunc(ok);
                })
            }
            return;
        }

        if(_cache[resPath]){
            cb(true);
            return;
        }

        if(_request[resPath]){
            _request[resPath].push(cb);
            return;
        }

        _request[resPath] = [];
        _request[resPath].push(cb);
        if(resPath.toLowerCase().endsWith(".js") ||
            resPath.toLowerCase().endsWith(".ttf"))
        {
            var downloader = realDownloader;
        }
        else{
            var downloader = takeDownloader();
        }
        downloader.download(resPath);
    };
    ResourceLoader.load = mLoader;
})();
