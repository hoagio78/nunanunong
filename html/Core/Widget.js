/**
 * Created by cocos2d on 11/9/2016.
 */
var newui = newui || {};
newui.Widget = ccui.Widget.extend({
    ctor : function (containerSize) {
        ccui.Widget.prototype.ctor.call(this);
        this.ignoreContentAdaptWithSize(true);

        this.setVirtualRendererSize(containerSize);
    },
    setVirtualRendererSize : function (containerSize) {
        this._vitualSize = cc.size(containerSize);
        this.setContentSize(containerSize);
    },
    getVirtualRendererSize : function () {
        if(this._vitualSize){
            return cc.size(this._vitualSize);
        }
        else{
            return ccui.Widget.prototype.getVirtualRendererSize.call(this);
        }
    }
});

//add hover event
(function () {
    //initGlobal mouse event
    var onMouseMoveTarget = [];
    var newListener = false;

    var sortListenerTarget = function () {
        if(newListener){
            //create map
            var globalIndex = -1;
            var nodeMap = {};

            var visitNode = function (node) {
                globalIndex++;

                var children = node.getChildren();
                for(var i=0;i<children.length;i++){
                    var child = children[i];
                    if(child && child.getLocalZOrder() < 0){
                        visitNode(child);
                    }
                }
                nodeMap[node.__instanceId] = globalIndex;
                for(var i=0;i<children.length;i++){
                    var child = children[i];
                    if(child && child.getLocalZOrder() >= 0){
                        visitNode(child);
                    }
                }
            };
            var rootNode = cc.director.getRunningScene();
            if(rootNode){
                visitNode(rootNode);
            }
            else{
                return;
            }

            //sort
            while(true){
                var flag = true;
                for(var i=1;i<onMouseMoveTarget.length;i++){
                    var z1 = nodeMap[onMouseMoveTarget[i-1].__instanceId];
                    var z2 = nodeMap[onMouseMoveTarget[i].__instanceId];
                    if(z1 > z2){
                        var target = onMouseMoveTarget[i-1];
                        onMouseMoveTarget[i-1] = onMouseMoveTarget[i];
                        onMouseMoveTarget[i] = target;
                        flag = false;
                    }
                }
                if(flag){
                    break;
                }
            }

            newListener = false;
        }
    };

    var invokeEvent = function (event) {
        var swallowEvent = false;
        for(var i=onMouseMoveTarget.length-1;i>=0;i--){
            var target = onMouseMoveTarget[i];
            if(!swallowEvent){
                if(invokeEventForTarget(event, target)){
                    swallowEvent = true;
                }
            }
            else{
                if(target._isMouseEnter === true){
                    target._isMouseEnter = false;
                    if (target._onMouseOver) {
                        target._onMouseOver(false);
                    }
                }
            }
        }
    };

    var invokeEventForTarget = function(event, target){
        var node = target;
        while(node){
            if(node.isVisible() === false){
                return false;
            }
            node = node.getParent();
        }

        if(target._onMouseMove(event)){
            return true;
        }
        return false;
    };

    setTimeout(function () {
        var listener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseMove: function(event){
                sortListenerTarget();
                invokeEvent(event);
            }
        });
        cc.eventManager.addListener(listener, -1);

    }, 0);
    
    //init
    cc.Node.prototype.setMouseOverEnable = function () {
        var thiz = this;
        thiz._isMouseEnter = false;
        if(thiz._onMouseOver === undefined){
            thiz._onMouseOver = function (isOver) {
                cc.log("_onMouseOver:" + isOver);
            };
        }

        if(thiz._onMouseMove === undefined){
            thiz._onMouseMove = function (event) {

                var locationInNode = this.convertToNodeSpace(event.getLocation());
                var rect = cc.rect(0, 0, this.width, this.height);

                var mouseOver = false;
                if (cc.rectContainsPoint(rect, locationInNode)) {
                    mouseOver = true;
                    //ccui.Widget
                    if(thiz.isClippingParentContainsPoint && !thiz.isClippingParentContainsPoint(event.getLocation())){
                        mouseOver = false;
                    }
                }

                if (this._isMouseEnter !== mouseOver) {
                    this._isMouseEnter = mouseOver;
                    if (this._onMouseOver) {
                        this._onMouseOver(mouseOver);
                    }
                }
                return mouseOver;
            }
        }

        var idx = onMouseMoveTarget.indexOf(thiz);
        if(idx < 0){
            onMouseMoveTarget.push(thiz);
            newListener = true;
        }

        var oldFunc = thiz.onExit;
        thiz.onExit = function () {
            oldFunc.apply(this, arguments);
            var idx = onMouseMoveTarget.indexOf(thiz);
            if(idx >= 0){
                onMouseMoveTarget.splice(idx, 1);
            }
        }
    };

    //add for button
    ccui.Widget.prototype._onMouseOver = function(isOver){
        if(this.isEnabled() && this.isTouchEnabled()){
            if(isOver){
                this._onPressStateChangedToPressed();
            }
            else{
                this._onPressStateChangedToNormal();
            }
        }
    };

    ccui.Widget.prototype.setMouseOverEnable = function () {
        if(this.isRunning()){
            cc.Node.prototype.setMouseOverEnable.apply(this, arguments);
        }
        this._isMouseOverEnable = true;
    };

    var uiWidgetOnEnter = ccui.Widget.prototype.onEnter;
    ccui.Widget.prototype.onEnter = function () {
        uiWidgetOnEnter.apply(this, arguments);
        if(this._isMouseOverEnable){
            this.setMouseOverEnable();
        }
    };

    var uiButtonCtor = ccui.Button.prototype.ctor;
    ccui.Button.prototype.ctor = function () {
        uiButtonCtor.apply(this, arguments);
        this.setMouseOverEnable();
    };
})();