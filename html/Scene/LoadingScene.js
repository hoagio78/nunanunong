
var LoadingScene = cc.Scene.extend({
    ctor : function () {
        this._super();

        var sceneLayer = new cc.Node();
        this.addChild(sceneLayer);
        this.sceneLayer = sceneLayer;

        sceneLayer.setContentSize(cc.size(1920, 1080));

        var bg = new cc.Sprite("res/LoadingScene_bg.jpg");
        bg.setPosition(sceneLayer.width/2, sceneLayer.height/2);
        bg.setScale(1080/720);
        sceneLayer.addChild(bg);

        var rootNode = new cc.Node();
        rootNode.setContentSize(cc.size(1280,720));
        rootNode.setAnchorPoint(cc.p(0.5, 1.0));
        rootNode.setPosition(sceneLayer.width/2, sceneLayer.height);
        sceneLayer.addChild(rootNode);

        var logo1 = new cc.Sprite("res/LoadingScene_logo_1.png");
        logo1.setPosition(633,458);
        rootNode.addChild(logo1);

        var logo2 = new cc.Sprite("res/LoadingScene_logo_2.png");
        logo2.setPosition(532,205);
        rootNode.addChild(logo2);

        var loadingBarBg = new cc.Sprite("res/LoadingScene_bar_1.png");
        loadingBarBg.setPosition(640,169);
        rootNode.addChild(loadingBarBg);

        var loadingBar = new cc.ProgressTimer(new cc.Sprite("res/LoadingScene_bar_2.png"));
        loadingBar.setType(cc.ProgressTimer.TYPE_BAR);
        loadingBar.setBarChangeRate(cc.p(1.0,0.0));
        loadingBar.setMidpoint(cc.p(0.0, 0.5));
        loadingBar.setPosition(loadingBarBg.getPosition());
        rootNode.addChild(loadingBar);
        this.loadingBar = loadingBar;
        loadingBar.setPercentage(0.0);

        // var label = new ccui.Text("", "arial", 32);
        var label = new cc.LabelBMFont("L...0%", "res/LoadingScene_font.fnt");
        label.setAnchorPoint(cc.p(0.0, 0.5));
        label.setPosition(552, 207);
        this.title = label;
        rootNode.addChild(label);
        this.loadingLabel = label;

        var thiz = this;
        var resLoader = new ResourceLoader();
        resLoader._autoRetry = true;
        resLoader.onLoadProcess = function (current, target) {
            thiz.updateProcess(100 * current / target);
        };
        resLoader.onLoadSuccess = function () {
            thiz.updateProcess(100);
        };
        resLoader.onLoadFailureWithAutoRetry = function () {
            thiz.__setStatus("Mạng tải chậm, đang tối ưu đường truyền");
        };

        this.gameLaucher = new GameLaucher();
        this.gameLaucher.loadMainModule = function () {
            resLoader.addModule(ModuleManager.getInstance().getModule("main"));
            // resLoader.addModule(ModuleManager.getInstance().getModule("GameBaiAll"));
            resLoader.load();
        };
        this.gameLaucher.onLoadResourceFailure = function () {
            thiz.__setStatus("Mạng tải chậm, đang tối ưu đường truyền");
            setTimeout(function () {
                thiz.startLoadResources();
            }, 1000)
        };
    },

    onCanvasResize: function () {
        var scaleY = cc.winSize.height / this.sceneLayer.height;
        if(scaleY < 1.0){
            scaleY = 1.0;
        }
        this.sceneLayer.setScale(scaleY);
        this.sceneLayer.setAnchorPoint(cc.p(0.5,1.0));
        this.sceneLayer.setPosition(cc.winSize.width/2, cc.winSize.height);
    },

    updateProcess: function (percentage) {
        this.currentPer = percentage;
        this._refreshProcess = true;
    },

    update : function (dt) {
        var currentPer = this.loadingBar.getPercentage();
        if(currentPer >= 100){
            this.unscheduleUpdate();
            setTimeout(function () {
                window._cc_finished_Loading();
            }, 0);
        }
        else{
            if(this._refreshProcess){
                var nextPer = dt * 100 + currentPer;
                if(nextPer >= this.currentPer){
                    nextPer = this.currentPer;
                    this._refreshProcess = false;
                }
                this.__setProcess(nextPer);
            }
        }
    },

    onEnter : function () {
        this._super();
        //fix
        cc.director.replaceScene = cc.director.replaceScene || function (scene) {
            cc.director.runScene(scene);
        };

        this.onCanvasResize();
        var thiz = this;
        cc.eventManager.addListener({
            event: cc.EventListener.CUSTOM,
            eventName : "canvas-resize",
            callback : function () {
                thiz.onCanvasResize();
            }
        }, this);

        this.scheduleUpdate();
        this.startLoadResources();
    },

    onExit : function () {
        this._super();
        this.gameLaucher = null;
        this.unscheduleUpdate();
    },

    startLoadResources : function () {
        if(window.cc_resources_search_path){
            cc.loader.resPath = window.cc_resources_search_path;
        }
        else{
            cc.loader.resPath = "";
        }
        this.gameLaucher.start();
    },

    __setProcess : function (per) {
        if(per > 100){
            per = 100;
        }
        this.loadingBar.setPercentage(per);
        this.loadingLabel.setString("L... " + Math.round(per) + "%");
    },

    __setStatus : function (status) {
      //  this.title.setString(status);
    }
});