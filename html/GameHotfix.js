/**
 * Created by cocos2d on 1/17/2017.
 */

var cc = cc || {};
cc.hotfixFunction = function () {
    cc.log("game hot fix");

    //fix update background
    var gamePaused = false;
    var lastUpdateTime = null;
    var updateBackgroundFunc = null;
    var frameRate = 1000.0 / 60;

    var updateBackground = function () {
        var now = Date.now();
        var dt = (now - lastUpdateTime) / 1000.0;
        lastUpdateTime = now;

        while(dt > 0.0){
            var deltaTime = dt < frameRate ? dt : frameRate;
            cc.director._scheduler.update(deltaTime);
            dt -= frameRate;
        }
    };

    var startUpdateBackground = function () {
        if(updateBackgroundFunc === null){
            lastUpdateTime = Date.now();
            updateBackgroundFunc = setInterval(updateBackground, frameRate);
        }
    };

    var stopUpdateBackground = function () {
        if(updateBackgroundFunc){
            clearInterval(updateBackgroundFunc);
            updateBackgroundFunc = null;
            updateBackground();
        }
    };
    cc.eventManager.addCustomListener(cc.game.EVENT_HIDE, function () {
        if(!gamePaused){
            gamePaused = true;
            startUpdateBackground();
            cc.log("cc.game.EVENT_HIDE");
        }
    });
    cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function () {
        if(gamePaused){
            gamePaused = false;
            stopUpdateBackground();
            cc.log("cc.game.EVENT_SHOW");
        }
    });

    //fix mouse move out canvas (iframe)
    cc._canvas.addEventListener("mouseout", function (event) {
        var selfPointer = cc.inputManager;
        if(selfPointer._mousePressed){
            selfPointer._mousePressed = false;
            var pos = selfPointer.getHTMLElementPosition(cc._canvas);
            var location = selfPointer.getPointByEvent(event, pos);
            selfPointer.handleTouchesEnd([selfPointer.getTouchByXY(location.x, location.y, pos)]);

            var mouseEvent = selfPointer.getMouseEvent(location,pos,cc.EventMouse.UP);
            mouseEvent.setButton(event.button);
            cc.eventManager.dispatchEvent(mouseEvent);

            event.stopPropagation();
            event.preventDefault();
        }
    }, false);

    /* Fix by cocos2d, bangmatiengviet */
    cc.LabelTTF._wordRex = /([a-zA-Z0-9đĐÁáÀàÃãẢảẠạÂâẤấẦầẪẫẨẩẬậĂăẮắẰằẴẵẲẳẶặÉéÈèẼẽẺẻẸẹêẾếỀềỄễỂểỆệÍíÌìĨĩỈỉỊịÓóÒòÕõỎỏỌọÔôỐốỒồỖỗỔổỘộƠơỚớỜờỠỡỞởỢợÚúÙùŨũỦủỤụƯưỨứỪừỮữỬửỰựÝýỲỳỸỹỶỷỴỵ]+|\S)/;
    cc.LabelTTF._symbolRex = /^[!,.:;}\]%\?>、‘“》？。，！]/;
    cc.LabelTTF._lastWordRex = /([a-zA-Z0-9đĐÁáÀàÃãẢảẠạÂâẤấẦầẪẫẨẩẬậĂăẮắẰằẴẵẲẳẶặÉéÈèẼẽẺẻẸẹêẾếỀềỄễỂểỆệÍíÌìĨĩỈỉỊịÓóÒòÕõỎỏỌọÔôỐốỒồỖỗỔổỘộƠơỚớỜờỠỡỞởỢợÚúÙùŨũỦủỤụƯưỨứỪừỮữỬửỰựÝýỲỳỸỹỶỷỴỵ]+|\S)$/;
    cc.LabelTTF._lastEnglish = /[a-zA-Z0-9đĐÁáÀàÃãẢảẠạÂâẤấẦầẪẫẨẩẬậĂăẮắẰằẴẵẲẳẶặÉéÈèẼẽẺẻẸẹêẾếỀềỄễỂểỆệÍíÌìĨĩỈỉỊịÓóÒòÕõỎỏỌọÔôỐốỒồỖỗỔổỘộƠơỚớỜờỠỡỞởỢợÚúÙùŨũỦủỤụƯưỨứỪừỮữỬửỰựÝýỲỳỸỹỶỷỴỵ]+$/;
    cc.LabelTTF._firsrEnglish = /^[a-zA-Z0-9đĐÁáÀàÃãẢảẠạÂâẤấẦầẪẫẨẩẬậĂăẮắẰằẴẵẲẳẶặÉéÈèẼẽẺẻẸẹêẾếỀềỄễỂểỆệÍíÌìĨĩỈỉỊịÓóÒòÕõỎỏỌọÔôỐốỒồỖỗỔổỘộƠơỚớỜờỠỡỞởỢợÚúÙùŨũỦủỤụƯưỨứỪừỮữỬửỰựÝýỲỳỸỹỶỷỴỵ]/;
};