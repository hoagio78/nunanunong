//
//  iOS_native_linker.h
//  GameBai2
//
//  Created by ext on 9/2/16.
//
//

#ifndef iOS_native_linker_h
#define iOS_native_linker_h

#include <string>
#include <vector>

const char* c_to_objC_getUUID(const char* keyUUID);
const char* c_to_objC_getVersion();
const char* c_to_objC_getBundle();
void c_to_objC_setKeyChainUser(const char* userId);
void c_to_objC_set_iClound_no_backup_folder(const char* folderPath);
void c_to_objC_showImagePicker(int maxWidth, int maxHeight);
void objC_to_c_onTakeImageData(const char* data);


#endif /* iOS_native_linker_h */
