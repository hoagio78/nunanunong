//
//  UIImagePicker.m
//  GameSlot
//
//  Created by Balua on 8/29/17.
//
//

#import "iOSImagePicker.h"
#import "iOS_native_linker.h"

void c_to_objC_showImagePicker(int maxWidth, int maxHeight){
    [[iOSImagePicker getInstance] showImagePicker:maxWidth maxHeight:maxHeight];
}

@implementation iOSImagePicker

static iOSImagePicker* s_iOSImagePicker = 0;
+ (iOSImagePicker*) getInstance{
    if (!s_iOSImagePicker) {
        s_iOSImagePicker = [[iOSImagePicker alloc] init];
        [s_iOSImagePicker retain];
    }
    
    return s_iOSImagePicker;
}

- (void) initWithView:(UIViewController*) _rootViewController{
    rootViewController = _rootViewController;
}

- (void) showImagePicker: (int) _maxWidth maxHeight:(int) _maxHeight{
    maxWidth = _maxWidth;
    maxHeight = _maxHeight;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = true;
        imagePickerController.delegate = self;
        [rootViewController presentViewController:imagePickerController animated:NO completion:nil];
    });
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    // UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
	
    float ratioW = maxWidth / chosenImage.size.width;
    float ratioH = maxHeight / chosenImage.size.height;
    float ratio = ratioW < ratioH ? ratioW : ratioH;
    if(ratio == 0.0f){
        ratio = 1.0f;
    }
    
    UIImage* returnImg = nil;
    if(ratio < 1.0f){
        float newWidth = chosenImage.size.width * ratio;
        float newHeight = chosenImage.size.height * ratio;
        returnImg = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(newWidth, newHeight)];
    }
    else{
        returnImg = chosenImage;
    }
  
    NSData *data = UIImageJPEGRepresentation(returnImg, 100);
    NSString* base64 = [data base64Encoding];
    objC_to_c_onTakeImageData([base64 UTF8String]);

    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
