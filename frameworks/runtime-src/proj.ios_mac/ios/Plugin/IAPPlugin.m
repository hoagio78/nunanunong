//
//  InAppPurchare.m
//  GameBaiVip
//
//  Created by Mac on 3/1/16.
//
//

#import "IAPPlugin.h"
#import "NativeSystemPlugin.h"

@interface IAPPlugin()< SKProductsRequestDelegate,SKPaymentTransactionObserver>


@end

@implementation IAPPlugin
static IAPPlugin *shareInApp =  nil;

+ (IAPPlugin *)getInApp
{
    if (shareInApp == nil) {
        shareInApp = [[IAPPlugin alloc] init];
        [shareInApp retain];
    }
 
    return shareInApp;
}

- (id) init{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    return self;
}

- (void) fetchAvailableProducts:( NSArray *)proDuct{
    NSSet *productIdentifiers = [NSSet setWithArray:proDuct];
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (SKProduct *) getItem:(NSString*) itemId
{
    
    for (int i = 0; i< arrSkpayment.count ;i ++) {
        SKProduct *validProduct = [arrSkpayment objectAtIndex:i];
        if ([validProduct.productIdentifier isEqualToString:itemId]) {
            return validProduct;
        }
    }
    return NULL;
}

- (void) buyItem:(SKProduct *)productSK{
    SKPayment *payment = [SKPayment paymentWithProduct:productSK];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        arrSkpayment = [response.products copy];
        NSLog(@"Products Available!");
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Transaction state -> Purchasing");
               // [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
            {
                //this is called when the user has successfully purchased the package (Cha-Ching!)
               //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                NSLog(@"Transaction state -> Purchased");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];

                [self buyItemFinishedWithCode:0 transactionReceipt:[transaction.transactionReceipt base64EncodedStringWithOptions:0]];
          
            }
                break;
            case SKPaymentTransactionStateRestored:
            {
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                NSString* newStr = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self buyItemFinishedWithCode:0 transactionReceipt:newStr];
            }
               
                break;
            case SKPaymentTransactionStateFailed:
            {
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self buyItemFinishedWithCode:1 transactionReceipt:@""];
            }
            case SKPaymentTransactionStateDeferred:{
                NSLog(@"Transaction state -> Deferred");
                break;
            }
                
            default:
                break;
        }
    }
}

-(void) buyItemFinishedWithCode:(int)returnCode transactionReceipt:(NSString*)transactionReceipt{
    NSString* ret = [NSString stringWithFormat:@"NativeSystemPluginBrigde.onBuyIAPFinished(%d, \"%@\");", returnCode, transactionReceipt];
    [[NativeSystemPlugin getInstance] JsEvalString:ret];
}

+ (void) nativeInitStore:(NSString*) itemsList{
    NSArray* itemArr = [[NSMutableArray alloc] init];
    itemArr = [NSJSONSerialization JSONObjectWithData: [itemsList dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
    [[IAPPlugin getInApp] fetchAvailableProducts:itemArr];
}

+ (void) nativeBuyItem:(NSString*) item{
    SKProduct* product = [[IAPPlugin getInApp] getItem:item];
    if(product){
        [[IAPPlugin getInApp] buyItem:product];
    }
}


@end
