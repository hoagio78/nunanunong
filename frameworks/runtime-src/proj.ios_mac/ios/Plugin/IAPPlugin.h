//
//  InAppPurchare.h
//  GameBaiVip
//
//  Created by Mac on 3/1/16.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>


@interface IAPPlugin : NSObject
{
    NSArray *arrSkpayment;   
}
//@property (nonatomic,readonly) bool isCanPurchar;

+ (IAPPlugin *)getInApp;

- (id) init;

- (void) fetchAvailableProducts:( NSArray*)proDuct;
- (SKProduct *) getItem:(NSString*) itemId;
- (void) buyItem:(SKProduct *)productSK;

+ (void) nativeInitStore:(NSString*) itemsList;
+ (void) nativeBuyItem:(NSString*) item;

@end
