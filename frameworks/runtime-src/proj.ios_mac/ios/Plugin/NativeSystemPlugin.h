//
//  SystemPlugin.h
//  GameBai2-mobile
//
//  Created by Balua on 11/27/17.
//

#import <Foundation/Foundation.h>

@interface NativeSystemPlugin : NSObject{
    UIViewController* rootViewController;
}

+ (NativeSystemPlugin*) getInstance;
    
- (void) initWithRootView:(UIViewController*) controller;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error;
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
- (void)applicationWillResignActive:(UIApplication *)application;
- (void)applicationDidBecomeActive:(UIApplication *)application;
- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;
- (void)applicationWillTerminate:(UIApplication *)application ;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

+ (NSString*) getPackageName;
+ (NSString*) getVersionName;
+ (BOOL) showCallPhone:(NSString*) phoneNumber;
+ (NSString*) identifierForAdvertising;
+ (BOOL) textToClipboard:(NSString*)text;

- (void) JsEvalString:(NSString*) evalString;

@end
