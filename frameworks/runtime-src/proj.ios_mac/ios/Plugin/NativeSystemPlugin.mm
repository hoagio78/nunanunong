//
//  SystemPlugin.m
//  GameBai2-mobile
//
//  Created by Balua on 11/27/17.
//

#import "NativeSystemPlugin.h"
#import <AdSupport/ASIdentifierManager.h>
#import "SMSPlugin.h"
#import "iOSImagePicker.h"
#import "NativeFacebookPlugin.h"
#import "UICKeyChainStore.h"
#import "iOS_native_linker.h"
#import "IAPPlugin.h"

#include "cocos2d.h"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"

const char* c_to_objC_getUUID(const char* keyUUID){
    
    NSString *nsUseId = [NSString stringWithCString: keyUUID encoding:NSUTF8StringEncoding];
    NSString *userUUID = [UICKeyChainStore stringForKey:nsUseId];
    if(userUUID == nil)
    {
        return 0;
    }
    return  [userUUID UTF8String]; //  makeStringCopy([userUUID UTF8String]);
}

void c_to_objC_setKeyChainUser(const char* userId)
{
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *nsUseId = [NSString stringWithCString: userId encoding:NSUTF8StringEncoding];
    NSString *nsUUID = [NSString stringWithCString: [uniqueIdentifier UTF8String] encoding:NSUTF8StringEncoding];
    [UICKeyChainStore setString:nsUUID forKey:nsUseId];
}

void c_to_objC_set_iClound_no_backup_folder(const char* folderPath){
    NSString* folder = [NSString stringWithUTF8String:folderPath];
    NSURL* URL= [NSURL fileURLWithPath: folder];
    if([[NSFileManager defaultManager] fileExistsAtPath:[URL path]] == NO){
        NSLog(@"Folder %@ not found", [URL path]);
        return;
    }
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(success){
        NSLog(@"Excluding %@ from backup", [URL lastPathComponent]);
    }
    else{
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
}

const char* c_to_objC_getVersion(){
    return [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] UTF8String];
}

const char* c_to_objC_getBundle(){
    return [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"] UTF8String];
}


@implementation NativeSystemPlugin

static NativeSystemPlugin* s_NativeSystemPlugin = 0;
+ (NativeSystemPlugin*) getInstance{
    if (!s_NativeSystemPlugin) {
        s_NativeSystemPlugin = [[NativeSystemPlugin alloc] init];
        [s_NativeSystemPlugin retain];
    }
    
    return s_NativeSystemPlugin;
}

- (void) initWithRootView:(UIViewController*) controller{
    rootViewController = controller;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    //sms
    [[SMSPlugin getInstance] initWithView:rootViewController];
    [[NativeFacebookPlugin getInstance]initWithView:rootViewController];
    [[iOSImagePicker getInstance] initWithView:rootViewController];
    [IAPPlugin getInApp];
    return [[NativeFacebookPlugin getInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
}

- (void)applicationWillResignActive:(UIApplication *)application{
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [[NativeFacebookPlugin getInstance] applicationDidBecomeActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [[NativeFacebookPlugin getInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void) JsEvalString:(NSString*) evalString{
    ScriptingCore::getInstance()->evalString([evalString UTF8String]);
}

+ (NSString*) getPackageName{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
}

+ (NSString*) getVersionName{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (BOOL) showCallPhone:(NSString*) phoneNumber{
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@", phoneNumber]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        return true;
    } else{
        NSString* message = [NSString stringWithFormat:@"Thiết bị không hỗ trợ gọi điện, vui lòng gọi %@ để được hỗ trợ", phoneNumber];
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Hỗ trợ" message:message delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
    return false;
}

+ (NSString*) identifierForAdvertising{
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]){
        NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        return [IDFA UUIDString];
    }
    
    return @"";
}

+ (BOOL) textToClipboard:(NSString*)text{
    if (![text  isEqual: @""]) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = text;
        return YES;
    }
    return NO;
}

@end
