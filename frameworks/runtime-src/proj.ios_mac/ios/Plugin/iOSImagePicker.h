//
//  UIImagePicker.h
//  GameSlot
//
//  Created by Balua on 8/29/17.
//
//

#import <Foundation/Foundation.h>

@interface iOSImagePicker : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    UIViewController* rootViewController;
    float maxWidth;
    float maxHeight;
}

+ (iOSImagePicker*) getInstance;
- (void) initWithView:(UIViewController*) _rootViewController;
- (void) showImagePicker: (int) maxWidth maxHeight:(int) maxHeight;
@end
