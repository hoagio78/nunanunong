//
//  FacebookPlugin_iOS.h
//  GameBaiVip
//
//  Created by ext on 3/4/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface NativeFacebookPlugin : NSObject{
    UIViewController* viewController;
    FBSDKLoginManager *loginManager;
    NSString* loginID;
}

+ (NativeFacebookPlugin*) getInstance;

- (id) initWithView:(UIViewController*) view;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;
- (void)applicationDidBecomeActive:(UIApplication *)application;

- (void) showLogin;
- (void) logout;

+ (void) nativeShowLogin;
+ (void) nativeShowLogout;
+ (NSString*) nativeGetAppId;

@end
