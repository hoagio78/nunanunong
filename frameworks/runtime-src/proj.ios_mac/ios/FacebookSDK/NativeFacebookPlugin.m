//
//  FacebookPlugin_iOS.m
//  GameBaiVip
//
//  Created by ext on 3/4/16.
//
//

#import "NativeFacebookPlugin.h"
#import "NativeSystemPlugin.h"

@implementation NativeFacebookPlugin

static NativeFacebookPlugin* s_NativeFacebookPlugin = 0;
+ (NativeFacebookPlugin*) getInstance{
    if(!s_NativeFacebookPlugin){
        s_NativeFacebookPlugin = [[NativeFacebookPlugin alloc] init];
        [s_NativeFacebookPlugin retain];
    }
    return s_NativeFacebookPlugin;
}

- (id) initWithView:(UIViewController*) view{
    viewController = view;
    loginManager = [[FBSDKLoginManager alloc] init];
    loginID =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppIDLogin"];
    
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [FBSDKAppEvents activateApp];
}

- (void) showLogin{
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    if(accessToken){
        NSString* accessTokenStr = [accessToken tokenString];
        NSString* userIdStr = [accessToken userID];
        [self onLoginFinishedWithCode:0 userId:userIdStr accessToken:accessTokenStr];
        [self requestGetProfile];
    }
    else{
        if(loginID){
            [FBSDKSettings setAppID:loginID];
        }
        
        [loginManager logInWithReadPermissions:@[@"public_profile"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
            if (error) {
                //NSLog(@"Process error");
                [self onLoginFinishedWithCode:1 userId:@"" accessToken:@""];
            } else if (result.isCancelled) {
                //NSLog(@"Cancelled");
                [self onLoginFinishedWithCode:-1 userId:@"" accessToken:@""];
            } else {
               // NSLog(@"Logged in");
                NSString* accessTokenStr = [[result token] tokenString];
                NSString* userIdStr = [[result token] userID];
                [self onLoginFinishedWithCode:0 userId:userIdStr accessToken:accessTokenStr];
                [self requestGetProfile];
            }
        }];
    }
}

- (void) onLoginFinishedWithCode:(int) returnCode userId:(NSString*)userId accessToken:(NSString*) accessToken{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* ret = [NSString stringWithFormat:@"FacebookNativeBrigde.onLoginFinished(%d, \"%@\", \"%@\");", returnCode, userId, accessToken];
        [[NativeSystemPlugin getInstance] JsEvalString:ret];
    });
}

- (void) logout{
    [loginManager logOut];
}

- (void) requestGetProfile{
    FBSDKGraphRequest* request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
                                  
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@", result);
         }
     }];
}

+ (void) nativeShowLogin{
    [[NativeFacebookPlugin getInstance] showLogin];
}

+ (void) nativeShowLogout{
    [[NativeFacebookPlugin getInstance] logout];
}

+ (NSString*) nativeGetAppId{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    return [dict objectForKey:@"FacebookAppID"];
}

@end
