/*
 * NetworkCore.h
 *
 *  Created on: May 31, 2016
 *      Author: ext
 */

#ifndef LOBBY_CLIENT_SOCKET_NETWORKCORE_H_
#define LOBBY_CLIENT_SOCKET_NETWORKCORE_H_
#include "NetworkDefine.h"
#include "Objects/Value.h"

namespace ext{
namespace net{

enum SocketStatusType{
	NotConnection = 0,
	Connecting, //1
	Connected, //2
	ConnectFailure, //3
	LostConnection, //4
	Closed //5
};

const char* SocketStatusName(int status);

struct SocketStatusData{
	ext::net::SocketStatusType preStatus;
	ext::net::SocketStatusType status;
};

class SocketClientStatus{
	ext::net::SocketStatusType clientStatus;
	std::mutex statusMutex;
	std::vector<ext::net::SocketStatusData> statusEvent;
public:
	SocketClientStatus();
	~SocketClientStatus();

	void set(ext::net::SocketStatusType status, bool isEvent);
	ext::net::SocketStatusType get();

	void popAllStatus(std::vector<ext::net::SocketStatusData> &buffer);
	void clear();
};

typedef ext::data::Value SocketData;

}
}
#endif /* LOBBY_CLIENT_SOCKET_NETWORKCORE_H_ */
