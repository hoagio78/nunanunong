/*
 * SocketAdapter.h
 *
 *  Created on: Dec 1, 2015
 *      Author: ext
 */

#ifndef LOBBY_SOCKET_SOCKETADAPTER_H_
#define LOBBY_SOCKET_SOCKETADAPTER_H_

#include "NetworkCore.h"
#include <queue>
#include <condition_variable>
#include <ctime>
#include "Objects/ValueReader.h"

#define USE_MESSAGE_HEADER

namespace ext{
namespace net{

typedef std::function<void(ext::net::SocketData*)> ReceiverCallback;
typedef std::function<void(const ext::net::SocketStatusData& data)> SocketStatusCallback;

class SocketPool{
protected:
	std::queue<ext::net::SocketData*>* mData;
	std::mutex poolMutex;
	std::condition_variable poolCond;
public:
	SocketPool();
	virtual ~SocketPool();

	virtual void push(ext::net::SocketData* data);
	virtual ext::net::SocketData* take();
	virtual ext::net::SocketData* pop();
	virtual void clear();
};

//class SocketPoolSender : public ext::net::SocketPool{
//	std::condition_variable poolCond;
//	char headerBuffer[4];
//public:
//	SocketPoolSender();
//	virtual ~SocketPoolSender();
//
//	void push(ext::net::SocketData* data);
//	ext::net::SocketData* take();
//	void clear();
//};
//
//class SocketPoolReceiver : public ext::net::SocketPool{
//public:
//	SocketPoolReceiver();
//	virtual ~SocketPoolReceiver();
//
//	void push(ext::net::SocketData* data);
//};

class SocketAdapter : public ext::data::LobbyRef{
protected:
	bool running;
	std::mutex mMutex;

	ext::net::SocketPool* mData;
	virtual void update();
public:
	SocketAdapter();
	virtual ~SocketAdapter();

	virtual void updateThread();

	virtual bool isRunning();
	virtual void setRunning(bool running);

	virtual void start();
	virtual void stop();

	virtual void pushMessage(ext::net::SocketData* data);
	virtual ext::net::SocketData* popMessage();
	//	virtual void popAllMessage(std::vector<SocketData*> &arr);
};

class SocketSender : public ext::net::SocketAdapter{
protected:
	//std::vector<char> sendBuffer;
	ext::data::ValueWriter* writer;
	void toBufferData(ext::net::SocketData* data);
public:
	SocketSender();
	virtual ~SocketSender();
};

class SocketReceiver : public ext::net::SocketAdapter, public ext::data::ValueReaderDelegate{
protected:
#ifdef USE_MESSAGE_HEADER
	std::vector<char> byteBuffer;
	int dataSize;
	bool recvHeader;

	virtual void onRecvData();
	virtual void onUpdateDataHeader();
	virtual void onUpdateData();
#endif

	ext::data::ValueReader* reader;
	virtual void recvData(const char* data, int size);
	virtual void onRecvMessage(ext::data::Value* value);
public:
	SocketReceiver();
	virtual ~SocketReceiver();

	virtual void updateThread();
};

/**/
class SocketClient : public ext::data::LobbyRef{
protected:
	ext::data::ReleasePool* releasePool;

	long long connectTime;
	std::string host;
	int port;

	std::mutex clientMutex;

	ext::net::SocketClientStatus _clientStatus;
	ext::net::SocketSender* mSender;
	ext::net::SocketReceiver* mReceiver;

	std::vector<ext::net::SocketStatusData> _statusBuffer;

	virtual void processEvent();
	virtual void processRecvMessage();
	virtual void clearAdapter();
	virtual void resetSocket();
	virtual void updateConnection();
	virtual void startAdapter();

	virtual void createAdapter();
	virtual bool connectThread();
	virtual void processSocketError();
public:
	ext::net::ReceiverCallback _recvCallback;
	ext::net::SocketStatusCallback _statusCallback;
public:
	SocketClient();
	virtual ~SocketClient();

	virtual void connectTo(const std::string& host, int port);

	virtual void closeClient();
	virtual void closeSocket();

	virtual ext::net::SocketStatusType getStatus();
	virtual void setStatus(ext::net::SocketStatusType status, bool isEvent = true);

	virtual void sendMessage(ext::net::SocketData* data);

	virtual void processMessage();
};

}
}
#endif /* LOBBY_SOCKET_SOCKETADAPTER_H_ */
