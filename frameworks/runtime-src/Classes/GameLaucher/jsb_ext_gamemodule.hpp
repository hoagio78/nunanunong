#include "base/ccConfig.h"
#ifndef __ext_gamemodule_h__
#define __ext_gamemodule_h__

#include "jsapi.h"
#include "jsfriendapi.h"
#include <string>

void register_all_ext_laucher_GameModule(JSContext* cx, JS::HandleObject obj);

extern JSClass  *jsb_ext_GameModule_class;
extern JSObject *jsb_ext_GameModule_prototype;
bool js_ext_laucher_GameModule_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_ext_laucher_GameModule_finalize(JSContext *cx, JSObject *obj);
void js_register_ext_laucher_GameModule(JSContext *cx, JS::HandleObject global);

extern JSClass  *jsb_ext_ModuleManager_class;
extern JSObject *jsb_ext_ModuleManager_prototype;
bool js_ext_laucher_ModuleManager_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_register_ext_laucher_ModuleManager(JSContext *cx, JS::HandleObject global);

void jsb_ext_laucher_ModuleManager_onUpdateModule(const std::string& moduleName, int current, int target);
void jsb_ext_laucher_ModuleManager_onLoadModule(const std::string& moduleName, int current, int target);
void jsb_ext_laucher_ModuleManager_onLoadModuleStatus(const std::string& moduleName, int status);

void jsb_ext_laucher_ModuleManager_onUnLoadModule(const std::string& moduleName, int current, int target);
void jsb_ext_laucher_ModuleManager_onUnLoadModuleStatus(const std::string& moduleName, int status);

#endif // __ext_gamemodule_h__
