/*
 * GameModule.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#ifndef GAMELAUCHER_GameModule_H_
#define GAMELAUCHER_GameModule_H_

#include "GameResource.h"
#include "ResourceLoader.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/prettywriter.h"

namespace ext{

enum LoadModuleStatus{
	ModuleUpdateFailure = 1,
	ModuleUpdateOk,
	ModuleLoadResourcesFailure,
	ModuleLoadResourcesFinished
};

class GameModule : public Ref{
	enum Status{
		None,
		Init,
		Initiated,
		Loading,
		Loaded,
		Unloading
	} status;

	std::mutex status_mutex;

	void setStatus(Status status) {
		std::unique_lock<std::mutex> lk(status_mutex);
		this->status = status;
	};
	Status getStatus() {
		std::unique_lock<std::mutex> lk(status_mutex);
		return this->status;
	};

	std::vector<GameFile*> _allFiles;
	ResourceLoader _resLoader;

	std::mutex updateCurrent_mutex;

	std::string moduleName;
	std::string rootFile;

	void runOnUI(const std::function<void()>& cb);

	void initFromFile(const std::string& fileName);
	void initFromJSON(const rapidjson::Value& data);
	void initModule(const rapidjson::Value& data);
	void initTextures(const rapidjson::Value& data);
	void initBitmapFonts(const rapidjson::Value& data);
	void initSound(const rapidjson::Value& data);
	void initSpineData(const rapidjson::Value& data);
	void initScript(const rapidjson::Value& data);
	void initRawFile(const rapidjson::Value& data);

	bool addFile(const std::string& fileName);

	void init(std::function<void()> finishdHandler = nullptr);
public:
	GameModule();
	virtual ~GameModule();
	
	void updateModule(std::function<void(bool)> finishedCallback);
	void loadModule(std::function<void(bool)> finishedCallback);
	void unloadModule(std::function<void(bool)> finishedCallback);
	void loadScript();
	int getTotalSize(); 

	std::string getName();
	void setName(const std::string& name);

	void setRootFile(const std::string& rootFile);

	void scriptUpdateModule();
	
	bool isReady();
	bool isLoaded();
};

class ModuleManager{
	std::map<std::string, GameModule*> _allModule;
	std::map<std::string, GameFile*> _allFiles;
	std::map<std::string, std::string> _allModuleFiles;
	//std::string _updateHost;

	std::vector<GameModule*> _updateModule;
	std::mutex _updateModule_mutex;

	void initFiles(const rapidjson::Value& data);
	bool initAllModule(const rapidjson::Value& data);
	void initModule(const std::string& name, const std::string& fileName);

	void runOnUI(const std::function<void()>& cb);
public:
	ModuleManager();
	virtual ~ModuleManager();
	void clearAll();
	bool initFromFile(const std::string& fileName);

	void onUpdateModule(GameModule* module);
	void onLoadModule(GameModule* module, int current, int target);
	void onLoadModuleStatus(GameModule* module, int status);

	void onUnloadModule(GameModule* module, int current, int target);
	void onUnloadModuleStatus(GameModule* module, int status);

	//void setUpdateHost(const std::string& updateHost);
	//std::string getUpdateHost();

	GameModule* getModule(const std::string& moduleName);
	std::string getModuleFile(const std::string& moduleName);
	std::vector<std::string> allModuleName();
	GameFile* getFile(const std::string& fileName);
	const std::map<std::string, GameFile*>& getAllFiles();

	static ModuleManager* getInstance();
};

}

#endif /* GAMELAUCHER_GameModule_H_ */
