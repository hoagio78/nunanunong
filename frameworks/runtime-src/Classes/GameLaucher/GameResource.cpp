/*
 * GameFile.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#include "GameResource.h"
#include "cocos2d.h"
USING_NS_CC;
#include <algorithm>
#include <ostream>
#include <iostream>
#include "network/HttpClient.h"
#include "GameLaucher.h"
#include <stdio.h>
#include "EngineUtilsThreadSafe.h"
#include "GameResourceDownloader.h"
#include "../Plugin/HttpFileDownloader.h"

namespace ext {
/****/

GameFile::GameFile() {
	// TODO Auto-generated constructor stub
	fileName = "";
	filePath = "";
	md5Digest = "";
    currentSize = 0;
	status = GameFile::Status::NONE;
}

GameFile::~GameFile() {
	// TODO Auto-generated destructor stub
}

bool GameFile::checkHashFile(){
	Data d = EngineUtilsThreadSafe::getInstance()->getRawFileData(filePath);
	if (!d.isNull()){
		MD5 md5;
		md5.update(d.getBytes(), d.getSize());
		md5.finalize();

		std::string md5Str = md5.hexdigest();
		std::transform(md5Str.begin(), md5Str.end(), md5Str.begin(), ::tolower);

		if (md5Str == md5Digest){
			return true;
		}
	}
	return false;
}

inline bool string_end_with(const std::string &str, const std::string &strend) {
	if (str.length() >= strend.length()) {
		return (0 == str.compare(str.length() - strend.length(), strend.length(), strend));
	} 
	return false;
}

bool GameFile::test() {
	if (GameLaucher::getInstance()->loadInternal) {
		return true;
	}

	filePath = EngineUtilsThreadSafe::getInstance()->fullPathForFilename(fileName);
	if (filePath != "") {

		bool b = checkHashFile();
		if (b) {
			return true;
		}
		else {
			CCLOG("Test Invalid hash");
		}
	}
	else {
		CCLOG("Test file not found");
	}
	return false;
}

bool GameFile::isReady(){
	std::unique_lock<std::mutex> lk(_status_mutex);
	if (status == Status::NONE) {
		if (this->test()) {
			status = Status::READY;
			this->setCurrentSize(fileSize);
			filePath = EngineUtilsThreadSafe::getInstance()->fullPathForFilename(fileName);
		}
		else {
			status = Status::NO_READY;
			this->setCurrentSize(0);
			filePath = EngineUtilsThreadSafe::getInstance()->getWritablePath() + "Game/" + fileName;
		}
	}
	return (status == Status::READY);
}

void GameFile::setCurrentSize(uint64_t size){
    std::unique_lock<std::mutex> lk(mutex_currentSize);
    this->currentSize = size;
}

uint64_t GameFile::getCurrentSize(){
    std::unique_lock<std::mutex> lk(mutex_currentSize);
    return currentSize;
}

void GameFile::update(const std::string urlHost, std::function<void()> processHandler, UpdateHandler finishedCallback){
	std::unique_lock<std::mutex> lk(_status_mutex);

	if (status == Status::READY) {
		if (processHandler) {
			processHandler();
		}
		if (finishedCallback) {
			finishedCallback(DownloadResult::OK);
		}
		return;
	}
	if (processHandler) {
		_processHandler.push_back(processHandler);
	}

	if (finishedCallback) {
		_finishedCallback.push_back(finishedCallback);
	}
	
	if (status == Status::UPDATING) {
		return;
	}

	status = Status::UPDATING;
	std::string url = urlHost + fileName;
	this->requestUpdate(url);
}

void GameFile::requestUpdate(const std::string url) {
    this->setCurrentSize(0);

	auto request = new GameFileDownloaderRequest(url, filePath);
	request->md5Digest = this->md5Digest;
	
	request->processCallback = [=](size_t size) {
		std::unique_lock<std::mutex> lk(_status_mutex);
		for (int i = 0; i < _processHandler.size(); i++) {
            this->setCurrentSize(size);
			_processHandler[i]();
		}
	};

	request->finishedCallback = [=](int returnCode) {
		std::unique_lock<std::mutex> lk(_status_mutex);

		if (returnCode == DownloadResult::NETWORK_ERROR) {
			//retry
			this->requestUpdate(url);
			return;
		}

		for (int i = 0; i < _finishedCallback.size(); i++) {
			_finishedCallback[i](returnCode);
		}
		_processHandler.clear();
		_finishedCallback.clear();

		status = (returnCode == DownloadResult::OK) ? Status::READY : Status::NO_READY;
	};

	cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([request]() {
		GameFileDownloader::getInstance()->addRequest(request);
	});
}

} /* namespace ext */
