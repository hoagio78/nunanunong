/*
 * SpineCache.h
 *
 *  Created on: Jul 6, 2016
 *      Author: ext
 */

#ifndef NEWGUI_SpineCache_H_
#define NEWGUI_SpineCache_H_

#include "cocos2d.h"
#include <string>
USING_NS_CC;
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/prettywriter.h"
#include "spine/spine-cocos2dx.h"

namespace ext {

class SpineDataItem{
	spAtlas* atlasData;
	spSkeletonData* defaultSkeletonData;
	bool _loaded;
public:
	std::string spineName;
	std::string jsonFile;
	std::string atlasFile;
	std::vector<std::string> textureFiles;
public:
	SpineDataItem(const std::string& name);
	virtual ~SpineDataItem();
	
	void initWithData(const rapidjson::Value& data);

	void load();
	void unload();
	spine::SkeletonAnimation* createNewAnimation(float scale = 1.0);
};

class SpineCache{
	std::map<std::string, SpineDataItem*> _caches;
public:
	SpineCache();
	virtual ~SpineCache();

	void addItem(const std::string& name, SpineDataItem* item);
	SpineDataItem* getItem(const std::string& name);
	SpineDataItem* createNewItem(const std::string& name);

	static SpineCache* getInstance();
};

} /* namespace ext */

#endif /* NEWGUI_SpineCache_H_ */
