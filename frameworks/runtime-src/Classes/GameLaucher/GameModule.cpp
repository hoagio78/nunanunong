/*
 * GameModule.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#include "GameModule.h"
#include "WorkerManager.h"
#include "cocos2d.h"
#include "EngineUtilsThreadSafe.h"
#include "SpineCache.h"
#include "jsb_ext_gamemodule.hpp"
#include "GameLaucher.h"

namespace ext{

GameModule::GameModule(){
	status = Status::None;
	moduleName = "";
}

GameModule::~GameModule(){

}

void GameModule::setRootFile(const std::string& rootFile) {
	this->rootFile = rootFile;
}

void GameModule::scriptUpdateModule() {
	std::unique_lock<std::mutex> lk(updateCurrent_mutex);
	
	uint64_t updateTarget = 0;
	uint64_t updateCurrent = 0;

	if(_allFiles.size() > 1) {
		uint64_t target0 = updateTarget = _allFiles[0]->fileSize;
		uint64_t current0 = updateCurrent = _allFiles[0]->getCurrentSize();

		uint64_t target = 0;
		uint64_t current = 0;
		for (int i = 1; i < _allFiles.size(); i++) {
			current += _allFiles[i]->getCurrentSize();
			target += _allFiles[i]->fileSize;
		}

		updateTarget = (uint64_t)(target0 * 0.1f + target * 0.9f);
		updateCurrent = (uint64_t)(current0 * 0.1f + current * 0.9f);
		if (updateCurrent > updateTarget) {
			updateCurrent = updateTarget;
		}
	}
	else {
		updateTarget = updateTarget = _allFiles[0]->fileSize * 10;
		updateCurrent = updateCurrent = _allFiles[0]->getCurrentSize();
	}
 
	jsb_ext_laucher_ModuleManager_onUpdateModule(moduleName, updateCurrent, updateTarget);
}

void GameModule::init(std::function<void()> finishdHandler){
	if (this->getStatus() == Status::Init) {
		return;
	}

	bool loadRemote = true;
	if (_allFiles.size() == 0) {
		auto configFile = ModuleManager::getInstance()->getFile(rootFile);
		if (configFile) {
			_allFiles.push_back(configFile);
		}
		else {
			loadRemote = false;
		}
	}
	if (loadRemote) {
		auto configFile = _allFiles[0];
		if (!configFile->isReady()) {
			std::string updateHost = GameLaucher::getInstance()->getResourcesHost();
			configFile->update(updateHost,
				[=]() {
				ModuleManager::getInstance()->onUpdateModule(this);
			},
				[=](int returnCode) {
				WorkerManager::getInstance()->pushAction([=]() {
					this->init(finishdHandler);
				});
			});
			return;
		}
	}
	
	Data data = EngineUtilsThreadSafe::getInstance()->getFileData(rootFile);
	std::string json((char*)data.getBytes(), data.getSize());

	rapidjson::Document doc;
	bool error = doc.Parse<0>(json.data()).HasParseError();
	if (!error){
		this->setStatus(Status::Init);
		this->initFromJSON(doc);

		if (_allFiles.size() > 0) {
			auto ticket = WorkerTicket::create(_allFiles.size());
			ticket->finishedCallback = [=](bool) {
				this->setStatus(Status::Initiated);
				if (finishdHandler) {
					finishdHandler();
				}
			};

			for (int i = 0; i < _allFiles.size();i++) {
				auto file = _allFiles[i];
				WorkerManager::getInstance()->pushAction([=]() {
					file->isReady();
					ticket->done(true);
				});
			}
		}
		else {
			this->setStatus(Status::Initiated);
			if (finishdHandler) {
				finishdHandler();
			}
		}
	}
}

void GameModule::initFromFile(const std::string& fileName){
	Data data = EngineUtilsThreadSafe::getInstance()->getFileData(fileName);
	std::string json((char*)data.getBytes(), data.getSize());

	rapidjson::Document doc;
	bool error = doc.Parse<0>(json.data()).HasParseError();
	if (!error){
		this->initFromJSON(doc);
	}
}

void GameModule::initFromJSON(const rapidjson::Value& data){
	if (data.HasMember("module")){
		this->initModule(data["module"]);
	}

	if (data.HasMember("texture")){
		this->initTextures(data["texture"]);
	}

	if (data.HasMember("fonts")){
		this->initBitmapFonts(data["fonts"]);
	}

	if (data.HasMember("sound")){
		this->initSound(data["sound"]);
	}

	if (data.HasMember("script")){
		this->initScript(data["script"]);
	}

	if (data.HasMember("spine")){
		this->initSpineData(data["spine"]);
	}

	if (data.HasMember("raw")){
		this->initRawFile(data["raw"]);
	}
}

void GameModule::initModule(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		std::string moduleName = data[i].GetString();
		std::string moduleFile = ModuleManager::getInstance()->getModuleFile(moduleName);
		this->initFromFile(moduleFile);
	}
}

void GameModule::initTextures(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		const rapidjson::Value& obj = data[i];
		std::string img = obj["img"].GetString();
		std::string plist = "";
		if (obj.HasMember("plist")){
			plist = obj["plist"].GetString();
		}

		if (addFile(img) && addFile(plist)){
			_resLoader.addTexture(img, plist);
		}
	}
}

void GameModule::initBitmapFonts(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		const rapidjson::Value& obj = data[i];
		std::string img = obj["img"].GetString();
		std::string fnt = "";
		if (obj.HasMember("fnt")){
			fnt = obj["fnt"].GetString();
		}

		if (addFile(img) && addFile(fnt)){
			_resLoader.addBMFont(img, fnt);
		}
	}
}

void GameModule::initSound(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		std::string sound = data[i].GetString();
		
		if (addFile(sound)){
			_resLoader.addSound(sound);
		}
	}
}

void GameModule::initSpineData(const rapidjson::Value& data){
	for (auto it = data.MemberBegin(); it != data.MemberEnd(); it++){
		std::string name = it->name.GetString();
		auto spineData = ext::SpineCache::getInstance()->createNewItem(name);
		spineData->initWithData(it->value);

        addFile(spineData->jsonFile);
        addFile(spineData->atlasFile);
        
		for (int i = 0; i < spineData->textureFiles.size(); i++){
			auto img = spineData->textureFiles[i];
            if(addFile(img)){
                _resLoader.addTexture(img);
            }
		}
        _resLoader.addSpine(name);
	}
}

void GameModule::initScript(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		std::string script = data[i].GetString();
		if (addFile(script)){
			_resLoader.addScript(script);
		}
	}
}

void GameModule::initRawFile(const rapidjson::Value& data){
	for (int i = 0; i < data.Size(); i++){
		std::string fileName = data[i].GetString();
        if(this->addFile(fileName)){

        }		
	}
}

bool GameModule::addFile(const std::string& fileName){
	if (GameLaucher::getInstance()->loadInternal){
		return true;
	}

	if (fileName.empty()){
		return false;
	}

	GameFile* file = ModuleManager::getInstance()->getFile(fileName);
	if (file){
        for(int i=0;i<_allFiles.size();i++){
            if(_allFiles[i] == file){
                return false;
            }
        }
		this->_allFiles.push_back(file);
		return true;
	}
	return false;
}

void GameModule::runOnUI(const std::function<void()>& cb){
	cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread(cb);
}

std::string GameModule::getName(){
	return this->moduleName;
}

void GameModule::setName(const std::string& name){
	this->moduleName = name;
}

void GameModule::updateModule(std::function<void(bool)> finishedCallback){
	if (this->isReady()){
		this->runOnUI([=](){
			ModuleManager::getInstance()->onLoadModuleStatus(this, LoadModuleStatus::ModuleUpdateOk);
			if (finishedCallback){
				finishedCallback(true);
			}
		});
			
		return;
	}

	std::vector<GameFile*> _requireUpdate;
	for (int i = 0; i < _allFiles.size(); i++){
		if (!_allFiles[i]->isReady()){
			_requireUpdate.push_back(_allFiles[i]);
		}
	}

	auto ticket = WorkerTicket::create(_requireUpdate.size());
	ticket->finishedCallback = [=](bool success){
		this->runOnUI([=](){
			if (success){
				EngineUtilsThreadSafe::getInstance()->fileUtilsPurgeCachedEntries();
			}
			ModuleManager::getInstance()->onLoadModuleStatus(this, success ? LoadModuleStatus::ModuleUpdateOk : LoadModuleStatus::ModuleUpdateFailure);
			if (finishedCallback){
				finishedCallback(success);
			}	
		});
	};

	std::string updateHost = GameLaucher::getInstance()->getResourcesHost();

	for (int i = 0; i < _requireUpdate.size(); i++){
		GameFile* file = _requireUpdate[i];
		WorkerManager::getInstance()->pushAction([=](){
			file->update(updateHost,
			[=](){
				ModuleManager::getInstance()->onUpdateModule(this);
			},

			[=](int returnCode){
				ticket->done(returnCode == 0);
			});
		});
	}
}

void GameModule::unloadModule(std::function<void(bool)> finishedCallback){
	auto finishedHandler = [=](bool success){
		this->setStatus(Status::Initiated);
		if (finishedCallback){
			finishedCallback(success);
		}
		ModuleManager::getInstance()->onUnloadModuleStatus(this, success ? LoadModuleStatus::ModuleLoadResourcesFinished : LoadModuleStatus::ModuleLoadResourcesFailure);
	};

	if (!this->isLoaded()){
		finishedHandler(true);
		return;
	}

	if (this->getStatus() == Status::Unloading){
		return;
	}

	this->setStatus(Status::Unloading);

	_resLoader.processHandler = [=](int current, int target){
		ModuleManager::getInstance()->onUnloadModule(this, current, target);
	};

	_resLoader.finishedHandler = finishedHandler;

	_resLoader.unload();
}

void GameModule::loadScript(){
	_resLoader.loadScript();
}

void GameModule::loadModule(std::function<void(bool)> finishedCallback){
	if (this->getStatus() == Status::None) {
		this->init([=]() {
			Director::getInstance()->getScheduler()->performFunctionInCocosThread([=]() {
				this->loadModule(finishedCallback);
			});		
		});
		return;
	}

	auto finishedLoadHandler = [=](bool success){
		if (success) {
			this->setStatus(Status::Loaded);
		}
		else {
			this->setStatus(Status::Initiated);
		}
		if (finishedCallback){
			finishedCallback(success);
		}
		ModuleManager::getInstance()->onLoadModuleStatus(this, success ? LoadModuleStatus::ModuleLoadResourcesFinished : LoadModuleStatus::ModuleLoadResourcesFailure);
	};

	if (this->getStatus() == Status::Loaded){
		finishedLoadHandler(true);
		return;
	}

	if (this->getStatus() == Status::Loading){
		return;
	}

	this->setStatus(Status::Loading);

	_resLoader.processHandler = [=](int current, int target){
		ModuleManager::getInstance()->onLoadModule(this, current, target);
	};

	_resLoader.finishedHandler = finishedLoadHandler;

	if (this->isReady()){
		_resLoader.load();
	}
	else{
		this->updateModule([=](bool success){
			if (success){
				_resLoader.load();
			}
			else{
				this->setStatus(Status::Initiated);
				
				if (finishedCallback){
					finishedCallback(false);
				}			
			}
		});
	}
}

int GameModule::getTotalSize(){
	int size = 0;
	for (int i = 0; i < _allFiles.size(); i++){
		size += _allFiles[i]->fileSize;
	}
	return size;
}

bool GameModule::isReady(){
	if (this->getStatus() == Status::None) {
		this->init();
		return false;
	}
	for (int i = 0; i < _allFiles.size(); i++){
		if (!_allFiles[i]->isReady()){
			return false;
		}
	}
	return true;
}

bool GameModule::isLoaded(){
	return (this->getStatus() == Status::Loaded);
}

/****/

ModuleManager::ModuleManager(){
	
}

ModuleManager::~ModuleManager(){
	this->clearAll();
}

void ModuleManager::clearAll(){
	for (auto it = _allModule.begin(); it != _allModule.end(); it++){
		delete it->second;
	}
	_allModule.clear();


	for (auto it = _allFiles.begin(); it != _allFiles.end(); it++){
		delete it->second;
	}
	_allFiles.clear();

	//_allModuleFiles.clear();
}

void ModuleManager::runOnUI(const std::function<void()>& cb){
	Director::getInstance()->getScheduler()->performFunctionInCocosThread(cb);
}

bool ModuleManager::initFromFile(const std::string& fileName){
	Data data = EngineUtilsThreadSafe::getInstance()->getFileData(fileName);
	std::string json((char*)data.getBytes(), data.getSize());

	rapidjson::Document doc;
	bool error = doc.Parse<0>(json.data()).HasParseError();
	if (!error){
		if (doc.HasMember("files")){
			this->initFiles(doc["files"]);
		}

		if (doc.HasMember("native")) {
			auto& nativeModule = doc["native"];
			for (auto it = nativeModule.MemberBegin(); it != nativeModule.MemberEnd(); it++) {
				std::string name = it->name.GetString();
				std::string rawFilename = it->value.GetString();
				_allModuleFiles.insert(std::make_pair(name, rawFilename));
			}
		}

		if (doc.HasMember("module")){
			return this->initAllModule(doc["module"]);
		}
	}
	return false;
}

void ModuleManager::initFiles(const rapidjson::Value& data){
	this->clearAll();

	for (int i = 0; i < data.Size(); i++){
		const rapidjson::Value& obj = data[i];

		GameFile* file = new GameFile();
		file->fileName = obj["file"].GetString();

		std::string md5Str = obj["hash"].GetString();
		std::transform(md5Str.begin(), md5Str.end(), md5Str.begin(), ::tolower);
		file->md5Digest = md5Str;

		file->fileSize = obj["size"].GetUint64();

		_allFiles.insert(std::make_pair(file->fileName, file));
	}
}

bool ModuleManager::initAllModule(const rapidjson::Value& data){
	//std::vector<GameFile*> requireUpdateFiles;

	for (auto it = data.MemberBegin(); it != data.MemberEnd(); it++) {
		std::string name = it->name.GetString();
		std::string rawFilename = it->value.GetString();
		_allModuleFiles.insert(std::make_pair(name, rawFilename));

		//GameFile* gameFile = this->getFile(rawFilename);
		//if (gameFile && !gameFile->isReady()) {
		//	requireUpdateFiles.push_back(gameFile);
		//}
	}

	//if (requireUpdateFiles.size() > 0) {
	//	auto _updateHost = GameLaucher::getInstance()->getResourcesHost();
	//	bool updateSuccess = false;

	//	std::mutex _mutex;
	//	std::unique_lock<std::mutex> lk(_mutex);
	//	std::condition_variable _condition_variable;

	//	auto ticket = WorkerTicket::create(requireUpdateFiles.size());
	//	ticket->finishedCallback = [&](bool success) {
	//		updateSuccess = success;
	//		_condition_variable.notify_all();
	//	};

	//	for(int i=0;i<requireUpdateFiles.size();i++){
	//		requireUpdateFiles[i]->update(_updateHost, nullptr, [&](int returnCode) {
	//			std::unique_lock<std::mutex> lk1(_mutex);
	//			ticket->done(returnCode == 0);
	//		});
	//	}
	//	_condition_variable.wait(lk);
	//	
	//	EngineUtilsThreadSafe::getInstance()->fileUtilsPurgeCachedEntries();

	//	if (!updateSuccess) {
	//		return false;
	//	}
	//}
	
	for (auto it = _allModuleFiles.begin(); it != _allModuleFiles.end(); it++){
		this->initModule(it->first, it->second);
	}
	EngineUtilsThreadSafe::getInstance()->fileUtilsPurgeCachedEntries();
	return true;
}

void ModuleManager::initModule(const std::string& name, const std::string& fileName){
	auto module = new GameModule();
	module->setName(name);
	module->setRootFile(fileName);
	_allModule.insert(std::make_pair(name, module));
}

void ModuleManager::onUpdateModule(GameModule* module){
	std::unique_lock<std::mutex> lk(_updateModule_mutex);
	for (int i = 0; i < _updateModule.size(); i++) {
		if (_updateModule[i] == module) {
			return;
		}
	}

	_updateModule.push_back(module);
	this->runOnUI([=](){
		std::unique_lock<std::mutex> lk(_updateModule_mutex);
		if (_updateModule.empty()) {
			return;
		}

		for (int i = 0; i < _updateModule.size(); i++) {
			_updateModule[i]->scriptUpdateModule();
		}
		_updateModule.clear();
	});	
}

void ModuleManager::onLoadModule(GameModule* module, int current, int target){
	this->runOnUI([=](){
		jsb_ext_laucher_ModuleManager_onLoadModule(module->getName(), current, target);
	});
}

void ModuleManager::onLoadModuleStatus(GameModule* module, int status){
	this->runOnUI([=](){
		jsb_ext_laucher_ModuleManager_onLoadModuleStatus(module->getName(), status);
	});	
}

void ModuleManager::onUnloadModule(GameModule* module, int current, int target){
	this->runOnUI([=](){
		jsb_ext_laucher_ModuleManager_onUnLoadModule(module->getName(), current, target);
	});
}

void ModuleManager::onUnloadModuleStatus(GameModule* module, int status){
	this->runOnUI([=](){
		jsb_ext_laucher_ModuleManager_onUnLoadModuleStatus(module->getName(), status);
	});
}

//void ModuleManager::setUpdateHost(const std::string& updateHost){
//	_updateHost = updateHost;
//}
//
//std::string ModuleManager::getUpdateHost(){
//	return _updateHost;
//}

GameModule* ModuleManager::getModule(const std::string& moduleName){
	auto it = _allModule.find(moduleName);
	if (it != _allModule.end()){
		return it->second;
	}
	return 0;
}

std::string ModuleManager::getModuleFile(const std::string& moduleName){
	auto it = _allModuleFiles.find(moduleName);
	if (it != _allModuleFiles.end()){
		return it->second;
	}
	return "";
}

std::vector<std::string> ModuleManager::allModuleName(){
	std::vector<std::string> names;
	for (auto it = _allModule.begin(); it != _allModule.end(); it++){
		names.push_back(it->second->getName());
	}
	return names;
}

GameFile* ModuleManager::getFile(const std::string& fileName){
	auto it = _allFiles.find(fileName);
	if (it != _allFiles.end()){
		return it->second;
	}
	return 0;
}

const std::map<std::string, GameFile*>& ModuleManager::getAllFiles(){
	return this->_allFiles;
}

static ModuleManager* s_ModuleManager = 0;
ModuleManager* ModuleManager::getInstance(){
	if (!s_ModuleManager){
		s_ModuleManager = new ModuleManager();
	}
	return s_ModuleManager;
}

}
