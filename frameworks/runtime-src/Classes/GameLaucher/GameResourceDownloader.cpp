/*
 * GameFile.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#include "GameResourceDownloader.h"
#include "WorkerManager.h"
#include "../Plugin/MD5.h"
#include "EngineUtilsThreadSafe.h"

USING_NS_CC;
namespace ext {
	static GameFileDownloader* s_GameFileDownloader_instance = 0;
	GameFileDownloader* GameFileDownloader::getInstance() {
		if (!s_GameFileDownloader_instance) {
			s_GameFileDownloader_instance = new GameFileDownloader();
			s_GameFileDownloader_instance->init();
		}
		return s_GameFileDownloader_instance;
	}

	GameFileDownloader::GameFileDownloader() {
		_requestIndex = 0;
		maxThread = 6;
	}

	GameFileDownloader::~GameFileDownloader() {
		for (auto it = _requests.begin(); it != _requests.end(); it++) {
			delete it->second;
		}
		_requests.clear();

		while (!_pendingRequest.empty()) {
			auto r = _pendingRequest.front();
			_pendingRequest.pop();
			delete r;
		}

		if (_downloader) {
			_downloader->onTaskError = (nullptr);
			_downloader->onFileTaskSuccess = (nullptr);
			_downloader->onTaskProgress = (nullptr);
			_downloader = 0;
		}
	}

	void GameFileDownloader::init() {
		network::DownloaderHints hints =
		{
			maxThread,
			15,
			".tmp"
		};
		_downloader = std::shared_ptr<network::Downloader>(new network::Downloader(hints));
		_downloader->onTaskError = [this](const network::DownloadTask& task,
			int errorCode,
			int errorCodeInternal,
			const std::string& errorStr) {

			this->onTaskError(task, errorCode, errorCodeInternal, errorStr);
		};


		_downloader->onTaskProgress = [this](const network::DownloadTask& task,
			int64_t bytesReceived,
			int64_t totalBytesReceived,
			int64_t totalBytesExpected)
		{
			this->onTaskProgress(task, bytesReceived, totalBytesReceived, totalBytesExpected);
		};

		_downloader->onDataTaskSuccess = [this](const network::DownloadTask& task, std::vector<unsigned char>& data)
		{
			this->onTaskSuccess(task, data);
		};
	}

	void GameFileDownloader::addRequest(GameFileDownloaderRequest* request) {
		std::unique_lock<std::mutex> lk(_requests_mutex);
		if (_requests.size() < maxThread) {
			auto identifier = StringUtils::toString(_requestIndex);
			auto url = request->url;
			_requestIndex++;
			if (_requestIndex > 100000000) {
				_requestIndex = 0;
			}

			_requests.insert(std::make_pair(identifier, request));
			_downloader->createDownloadDataTask(request->url, identifier);
		}
		else {
			_pendingRequest.push(request);
		}
	}

	GameFileDownloaderRequest* GameFileDownloader::takeRequest(const cocos2d::network::DownloadTask& task){
		std::unique_lock<std::mutex> lk(_requests_mutex);

		auto it = _requests.find(task.identifier);
		if (it != _requests.end()) {
			auto request = it->second;
			_requests.erase(it);
			return request;
		}
		return nullptr;
	}

	GameFileDownloaderRequest* GameFileDownloader::getRequest(const cocos2d::network::DownloadTask& task) {
		std::unique_lock<std::mutex> lk(_requests_mutex);

		auto it = _requests.find(task.identifier);
		if (it != _requests.end()) {
			return it->second;
		}
		return nullptr;
	}

	void GameFileDownloader::onTaskSuccess(const cocos2d::network::DownloadTask& task, std::vector<unsigned char>& data) {
		auto request = this->takeRequest(task);
		if (request) {
			request->onTaskSuccess(data);
		}
		this->nextRequest();
	}

	void GameFileDownloader::onTaskProgress(const cocos2d::network::DownloadTask& task, int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected) {
		auto request = this->getRequest(task);
		if (request) {
			request->onTaskProgress(bytesReceived, totalBytesReceived, totalBytesExpected);
		}
	}

	void GameFileDownloader::onTaskError(const cocos2d::network::DownloadTask& task, int errorCode, int errorCodeInternal, const std::string& errorStr) {
		auto request = this->takeRequest(task);
		if (request) {
			request->onTaskError(errorCode, errorCodeInternal, errorStr);
		}
		this->nextRequest();
	}

	void GameFileDownloader::nextRequest() {
		_requests_mutex.lock();
		if (!_pendingRequest.empty()) {
			auto request = _pendingRequest.front();
			_pendingRequest.pop();
			_requests_mutex.unlock();
			this->addRequest(request);
			return;
		}
		_requests_mutex.unlock();
	}

	////////////////////////////////////////////////

	GameFileDownloaderRequest::GameFileDownloaderRequest() {
		url = "";
		savePath = "";
		md5Digest = "";
		finishedCallback = 0;
		processCallback = 0;
	}

	GameFileDownloaderRequest::GameFileDownloaderRequest(const std::string& url, const std::string& savePath) : GameFileDownloaderRequest(){
		this->url = url;
		this->savePath = savePath;
	}

	GameFileDownloaderRequest::~GameFileDownloaderRequest() 
	{	
		finishedCallback = 0;
		processCallback = 0;
	}

	void GameFileDownloaderRequest::onTaskSuccess(std::vector<unsigned char>& data) {
		_data.assign(data.begin(), data.end());
		WorkerManager::getInstance()->pushAction([=]() {
			this->onDownloadFinished();
		});
	}

	void GameFileDownloaderRequest::onTaskProgress(int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected) {
		if (processCallback) {
			processCallback(totalBytesReceived);
		}
	}

	void GameFileDownloaderRequest::onTaskError(int errorCode, int errorCodeInternal, const std::string& errorStr) {
		if (finishedCallback) {
			finishedCallback(2);
		}
		delete this;
	}

	void GameFileDownloaderRequest::onDownloadFinished() {
		bool success = false;
		if (md5Digest.empty()) {
			success = true;
		}
		else {
			MD5 md5;
			md5.update(_data.data(), _data.size());
			md5.finalize();
			auto md5Str = md5.hexdigest();
			std::transform(md5Str.begin(), md5Str.end(), md5Str.begin(), ::tolower);
			if (md5Str == md5Digest) {
				success = true;
			}
		}
		if (success) {
			if (!savePath.empty()) {
				auto n = savePath.find_last_of("/");
				if (n != std::string::npos && n != 0) {
					std::string parentFolder = savePath.substr(0, n);
					success = EngineUtilsThreadSafe::getInstance()->createDirectory(parentFolder);
				}
			}
		
			if (success) {
				if (!savePath.empty()) {
					auto fileWrite = fopen(savePath.c_str(), "wb");
					if (fileWrite) {
						fwrite(_data.data(), sizeof(unsigned char), _data.size(), fileWrite);
						fclose(fileWrite);
						fileWrite = 0;
					}
				}

				if (finishedCallback) {
					finishedCallback(0);
				}
			}
			else {
				if (finishedCallback) {
					finishedCallback(2);
				}
			}
		}
		else {
			if (finishedCallback) {
				finishedCallback(2);
			}
		}

		delete this;
	}

} /* namespace ext */
