/*
 * GameLaucher.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#ifndef GAMELAUCHER_GAMELAUCHER_H_
#define GAMELAUCHER_GAMELAUCHER_H_

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <queue>
#include <functional>
#include "cocos2d.h"
USING_NS_CC;
#include "GameResource.h"
#include "ResourceLoader.h"
#include "GameLaucherConfig.h"

namespace ext {
enum GameLaucherStatus{
	GetUpdate = 0,	//0
	TestVersion,	//1
	TestHashFiles,	//2
	Updating,		//3
	UpdateFailure,	//4
	LoadResource,	//5
	LoadScript,		//6
	LoadAndroidExt,	//7
	Finished,		//8
};

class GameLaucher {
	GameFile* _versionFile;

	std::string resourceHost;
	std::string versionHash;
	std::string versionFile;

	int status;
	
	void initLaucher();
	void runOnUI(const std::function<void()>& cb);
	
	void requestGetUpdate();
	void updateByACS(const std::string& url);
	void updateByForceUpdate(const std::string& url);
	void updateLoadInternal();

	void checkVersionFile();
	void checkAllFiles();
	void loadMainModule();

	void finishLaucher();
public:
	bool acsUpdate;
	bool forceUpdate;
	bool loadInternal;
public:
	GameLaucher();
	virtual ~GameLaucher();

	std::string getResourcesHost();
	void onProcessStatus(int status);

	void run();
	static GameLaucher* getInstance();
};

} /* namespace ext */

#endif /* GAMELAUCHER_GAMELAUCHER_H_ */
