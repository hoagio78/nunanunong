/*
 * ResourceLoader.h
 *
 *  Created on: Dec 15, 2015
 *      Author: ext
 */

#ifndef COMMON_RESOURCELOADER_H_
#define COMMON_RESOURCELOADER_H_

#include "cocos2d.h"
#include "WorkerManager.h"
#include "SpineCache.h"
USING_NS_CC;

namespace ext {
struct TextureData{
	std::string texture;
	std::string plist;
};

struct BMFontData{
	std::string texture;
	std::string font;
};

enum ResourceLoaderStep{
	kStepUnLoadImage = 1,
	kStepUnLoadFrames,
	kStepUnLoadFont,
	kStepUnLoadSound,
	kStepUnLoadSpine,

	kStepLoadImage,
	kStepLoadFrames,
	kStepLoadFont,
	kStepLoadSound,
	kStepLoadSpine,
	kStepLoadScript,
	kStepWaiting,

	kStepPreFinish,
	kStepFinish,
};

typedef std::function<void(int, int)> LoaderProcessHandler;

class ResourceLoader : public Ref{
	std::vector<std::string> _texture;
	std::vector<std::string> _frames;
	std::vector<std::string> _bmfonts;
	std::vector<std::string> _sound;
	std::vector<std::string> _scripts;
	std::vector<std::string> _spine;

	int index;
	ResourceLoaderStep step;

	int targetStep;
	int currentStep;

	void onProcessLoader();

	void runOnUI(const std::function<void()>& runable);

	void loadTexture(std::string img, WorkerTicket* ticket);
//public:
	bool running;
	bool isLoad;

	void updateLoadResources();
	void updateUnloadResources();
public:
	std::function<void(int, int)> processHandler;
	std::function<void(bool)> finishedHandler;
public:
	ResourceLoader();
	virtual ~ResourceLoader();

	void addTexture(const std::string &img, const std::string &plist = "");
	void addBMFont(const std::string &texture, const std::string &font);
	void addSound(const std::string& sound);
	void addScript(const std::string& script);
	void addSpine(const std::string& item);

	void update(float dt);

	void load();
	void unload();
	void loadScript();
};

} /* namespace ext */

#endif /* COMMON_RESOURCELOADER_H_ */
