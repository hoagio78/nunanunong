/*
 * WorkerManager.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#include "WorkerManager.h"

#define MAX_WORKER 4

WorkerTicket::WorkerTicket(int count){
	_count = count;
	_success = _count;
	finishedCallback = nullptr;
}

WorkerTicket::~WorkerTicket(){

}

void WorkerTicket::done(bool sucess){
	_count_mutex.lock();
	_count--;
	if (sucess){
		_success--;
	}
	if (_count <= 0){
		_count_mutex.unlock();
		if (finishedCallback){
			finishedCallback(_success == 0);
		}
		delete this;
		return;
	}
	_count_mutex.unlock();
}

WorkerTicket* WorkerTicket::create(int count){
	auto ticker = new WorkerTicket(count);
	return ticker;
}

static WorkerManager* s_WorkerManager = 0;
WorkerManager* WorkerManager::getInstance(){
	if (!s_WorkerManager){
		s_WorkerManager = new WorkerManager();
		s_WorkerManager->start(4);
	}
	return s_WorkerManager;
}

WorkerManager::WorkerManager(){
	//currentThread = 0;
	this->stop();
}

WorkerManager::~WorkerManager(){
	
}

LaucherAction WorkerManager::takeAction(){
	std::unique_lock<std::mutex> lk(_actionsMutex);
	if (!_actions.empty()){
		auto action = _actions.front();
		_actions.pop();
		return action;
	}
	_actions_condition_variable.wait(lk);
	if (!_actions.empty()){
		auto action = _actions.front();
		_actions.pop();
		return action;
	}
	return nullptr;

	//if (!_actions.empty()){
	//	auto action = _actions.front();
	//	_actions.pop();
	//	return action;
	//}
	//return nullptr;
}

void WorkerManager::pushAction(const LaucherAction& action){
	std::unique_lock<std::mutex> lk(_actionsMutex);
	_actions.push(action);
	_actions_condition_variable.notify_one();
}

//#include "cocos2d.h"

void WorkerManager::runActionThread(){
	//cocos2d::log("start thread");

	while (true)
	{
		auto action = this->takeAction();
		if (action){
			action();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		else{
			break;
		}		
	}

	//currentThread_mutex.lock(); 
	//currentThread--;
	//currentThread_mutex.unlock();

	//cocos2d::log("end thread");
}

void WorkerManager::start(int thread){
	for (int i = 0; i < thread; i++){
		std::thread workerThread(&WorkerManager::runActionThread, this);
		workerThread.detach();
	}
}

void WorkerManager::stop(){
	std::unique_lock<std::mutex> lk(_actionsMutex);
	while (!_actions.empty()){
		_actions.pop();
	}
	_actions_condition_variable.notify_all();
}