/*
 * GameResource.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#ifndef GAMELAUCHER_GAMERESOURCE_H_
#define GAMELAUCHER_GAMERESOURCE_H_

#include <string>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <vector>

namespace ext {
typedef std::function<void(int)> UpdateHandler;
    
class GameFile {
	enum Status{
		NONE,
		NO_READY,
		UPDATING,
		READY
	} status;
	std::mutex _status_mutex;

	std::vector<std::function<void()>> _processHandler;
	std::vector<UpdateHandler> _finishedCallback;
    
    uint64_t currentSize;
    std::mutex mutex_currentSize;

	bool checkHashFile();
	void requestUpdate(const std::string url);
	bool test();
public:
	std::string fileName;
	std::string filePath;
	std::string md5Digest;
    
	uint64_t fileSize;
public:
	GameFile();
	virtual ~GameFile();

	bool isReady();
    void update(const std::string url, std::function<void()> processHandler, UpdateHandler finishedCallback);
    
    void setCurrentSize(uint64_t size);
    uint64_t getCurrentSize();
};

} /* namespace ext */

#endif /* GAMELAUCHER_GAMERESOURCE_H_ */
