/*
 * GameResource.h
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#ifndef GAMELAUCHER_GAMERESOURCEDOWNLOADER_H_
#define GAMELAUCHER_GAMERESOURCEDOWNLOADER_H_

#include "cocos2d.h"
#include "network/CCDownloader.h"

namespace ext {
	class GameFileDownloaderRequest {
	public:
		GameFileDownloaderRequest();
		GameFileDownloaderRequest(const std::string& url, const std::string& savePath);
		virtual ~GameFileDownloaderRequest();

		void onTaskSuccess(std::vector<unsigned char>& data);
		void onTaskProgress(int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected);
		void onTaskError(int errorCode, int errorCodeInternal, const std::string& errorStr);

		const std::vector<unsigned char>& getData() { return _data; };
	public:
		std::string url;
		std::string savePath;
		std::string md5Digest;
		
		std::function<void(int)> finishedCallback;
		std::function<void(size_t size)> processCallback;
	private:
		void onDownloadFinished();
		std::vector<unsigned char> _data;
	};

	class GameFileDownloader {
	public:
		static GameFileDownloader* getInstance();

		GameFileDownloader();
		virtual ~GameFileDownloader();
		void init();

		void addRequest(GameFileDownloaderRequest* request);
		GameFileDownloaderRequest* takeRequest(const cocos2d::network::DownloadTask& task);
		GameFileDownloaderRequest* getRequest(const cocos2d::network::DownloadTask& task);

		void onTaskSuccess(const cocos2d::network::DownloadTask& task, std::vector<unsigned char>& data);
		void onTaskProgress(const cocos2d::network::DownloadTask& task, int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected);
		void onTaskError(const cocos2d::network::DownloadTask& task, int errorCode, int errorCodeInternal, const std::string& errorStr);

		void nextRequest();

		std::shared_ptr<cocos2d::network::Downloader> _downloader;

		std::mutex _requests_mutex;
		int _requestIndex;
		std::map<std::string, GameFileDownloaderRequest*> _requests;

		std::queue<GameFileDownloaderRequest*> _pendingRequest;
		uint32_t maxThread;
	};
}


#endif /* GAMELAUCHER_GAMERESOURCEDOWNLOADER_H_ */
