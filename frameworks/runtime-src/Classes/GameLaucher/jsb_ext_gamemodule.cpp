#include "jsb_ext_gamemodule.hpp"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include "GameModule.h"
#include "SpineCache.h"

template<class T>
static bool dummy_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS_ReportError(cx, "Constructor for the requested class is not available, please refer to the API reference.");
	return false;
}

static bool empty_constructor(JSContext *cx, uint32_t argc, jsval *vp) {
	return false;
}

static bool js_is_native_obj(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	args.rval().setBoolean(true);
	return true;
}

JSClass  *jsb_ext_laucher_GameModule_class;
JSObject *jsb_ext_laucher_GameModule_prototype;

bool js_ext_laucher_GameModule_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	ext::GameModule* cobj = new (std::nothrow) ext::GameModule();

	js_type_class_t *typeClass = js_get_type_from_native<ext::GameModule>(cobj);

	// link the native object with the javascript object
	JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "ext::GameModule"));
	args.rval().set(OBJECT_TO_JSVAL(jsobj));
	if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
		ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
	return true;
}

static bool js_ext_laucher_GameModule_ctor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	ext::GameModule *nobj = new (std::nothrow) ext::GameModule();
	auto newproxy = jsb_new_proxy(nobj, obj);
	jsb_ref_init(cx, &newproxy->obj, nobj, "ext::GameModule");
	bool isFound = false;
	if (JS_HasProperty(cx, obj, "_ctor", &isFound) && isFound)
		ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", args);
	args.rval().setUndefined();
	return true;
}


//bool js_ext_laucher_GameModule_test(JSContext *cx, uint32_t argc, jsval *vp){
//	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
//	bool ok = true;
//	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
//	js_proxy_t *proxy = jsb_get_js_proxy(obj);
//	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
//	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_test : Invalid Native Object");
//	if (argc == 1) {
//		std::function<void(bool)> arg0;
//		do {
//			if (JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
//			{
//				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
//				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
//				auto lambda = [=](bool larg0) -> void {
//					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
//
//					jsval largv[1];
//					largv[0] = BOOLEAN_TO_JSVAL(larg0);
//					JS::RootedValue rval(cx);
//					bool succeed = func->invoke(1, &largv[0], &rval);
//					if (!succeed && JS_IsExceptionPending(cx)) {
//						JS_ReportPendingException(cx);
//					}	
//				};
//				arg0 = lambda;
//			}
//			else
//			{
//				arg0 = nullptr;
//			}
//		} while (0);
//
//		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_GameModule_test : Error processing arguments");
//		cobj->test(arg0);
//		args.rval().setUndefined();
//		return true;
//	}
//
//	JS_ReportError(cx, "js_ext_laucher_GameModule_test : wrong number of arguments: %d, was expecting %d", argc, 1);
//	return false;
//}

bool js_ext_laucher_GameModule_updateModule(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_updateModule : Invalid Native Object");
	if (argc == 1) {
		std::function<void(bool)> arg0;
		do {
			if (JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
			{
				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
				auto lambda = [=](bool larg0) -> void {
					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET

						jsval largv[1];
					largv[0] = BOOLEAN_TO_JSVAL(larg0);
					JS::RootedValue rval(cx);
					bool succeed = func->invoke(1, &largv[0], &rval);
					if (!succeed && JS_IsExceptionPending(cx)) {
						JS_ReportPendingException(cx);
					}
				};
				arg0 = lambda;
			}
			else
			{
				arg0 = nullptr;
			}
		} while (0);

		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_GameModule_updateModule : Error processing arguments");
		cobj->updateModule(arg0);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 0){
		cobj->updateModule(nullptr);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_updateModule : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_loadModule(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_loadModule : Invalid Native Object");
	if (argc == 1) {
		std::function<void(bool)> arg0;
		do {
			if (JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
			{
				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
				auto lambda = [=](bool larg0) -> void {
					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET

						jsval largv[1];
					largv[0] = BOOLEAN_TO_JSVAL(larg0);
					JS::RootedValue rval(cx);
					bool succeed = func->invoke(1, &largv[0], &rval);
					if (!succeed && JS_IsExceptionPending(cx)) {
						JS_ReportPendingException(cx);
					}
				};
				arg0 = lambda;
			}
			else
			{
				arg0 = nullptr;
			}
		} while (0);

		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_GameModule_loadModule : Error processing arguments");
		cobj->loadModule(arg0);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 0){
		cobj->loadModule(nullptr);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_loadModule : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_loadScript(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_loadScript : Invalid Native Object");
	if (argc == 0){
		cobj->loadScript();
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_loadScript : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_unloadModule(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_loadModule : Invalid Native Object");
	if (argc == 1) {
		std::function<void(bool)> arg0;
		do {
			if (JS_TypeOfValue(cx, args.get(0)) == JSTYPE_FUNCTION)
			{
				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(0), args.thisv()));
				auto lambda = [=](bool larg0) -> void {
					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET

						jsval largv[1];
					largv[0] = BOOLEAN_TO_JSVAL(larg0);
					JS::RootedValue rval(cx);
					bool succeed = func->invoke(1, &largv[0], &rval);
					if (!succeed && JS_IsExceptionPending(cx)) {
						JS_ReportPendingException(cx);
					}
				};
				arg0 = lambda;
			}
			else
			{
				arg0 = nullptr;
			}
		} while (0);

		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_GameModule_unloadModule : Error processing arguments");
		cobj->unloadModule(arg0);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 0){
		cobj->unloadModule(nullptr);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_unloadModule : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_getTotalSize(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_getTotalSize : Invalid Native Object");
	if (argc == 0) {
		int size = cobj->getTotalSize();
		args.rval().setInt32(size);
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_getTotalSize : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_isLoaded(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_isLoaded : Invalid Native Object");
	if (argc == 0) {
		bool b = cobj->isLoaded();
		args.rval().setBoolean(b);
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_isLoaded : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_GameModule_isReady(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::GameModule* cobj = (ext::GameModule*)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "js_ext_laucher_GameModule_isReady : Invalid Native Object");
	if (argc == 0) {
		bool b = cobj->isReady();
		args.rval().setBoolean(b);
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_GameModule_isReady : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

void js_register_ext_laucher_GameModule(JSContext *cx, JS::HandleObject global) {
	jsb_ext_laucher_GameModule_class = (JSClass *)calloc(1, sizeof(JSClass));
	jsb_ext_laucher_GameModule_class->name = "GameModule";
	jsb_ext_laucher_GameModule_class->addProperty = JS_PropertyStub;
	jsb_ext_laucher_GameModule_class->delProperty = JS_DeletePropertyStub;
	jsb_ext_laucher_GameModule_class->getProperty = JS_PropertyStub;
	jsb_ext_laucher_GameModule_class->setProperty = JS_StrictPropertyStub;
	jsb_ext_laucher_GameModule_class->enumerate = JS_EnumerateStub;
	jsb_ext_laucher_GameModule_class->resolve = JS_ResolveStub;
	jsb_ext_laucher_GameModule_class->convert = JS_ConvertStub;
	jsb_ext_laucher_GameModule_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

	static JSPropertySpec properties[] = {
		JS_PS_END
	};

	static JSFunctionSpec funcs[] = {
		JS_FN("ctor", js_ext_laucher_GameModule_ctor, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("isLoaded", js_ext_laucher_GameModule_isLoaded, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("isReady", js_ext_laucher_GameModule_isReady, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),		
		JS_FN("loadModule", js_ext_laucher_GameModule_loadModule, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("unloadModule", js_ext_laucher_GameModule_unloadModule, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),	
		JS_FN("updateModule", js_ext_laucher_GameModule_updateModule, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getTotalSize", js_ext_laucher_GameModule_getTotalSize, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("loadScript", js_ext_laucher_GameModule_loadScript, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		
	//	JS_FN("test", js_ext_laucher_GameModule_test, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FS_END
	};

	JSFunctionSpec *st_funcs = NULL;

	jsb_ext_laucher_GameModule_prototype = JS_InitClass(
		cx, global,
		JS::NullPtr(),
		jsb_ext_laucher_GameModule_class,
		js_ext_laucher_GameModule_constructor, 0, // constructor
		properties,
		funcs,
		NULL, // no static properties
		st_funcs);

	JS::RootedObject proto(cx, jsb_ext_laucher_GameModule_prototype);
	JS::RootedValue className(cx, std_string_to_jsval(cx, "GameModule"));
	JS_SetProperty(cx, proto, "_className", className);
	JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
	JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
	// add the proto and JSClass to the type->js info hash table
	jsb_register_class<ext::GameModule>(cx, jsb_ext_laucher_GameModule_class, proto, JS::NullPtr());
	anonEvaluate(cx, global, "(function () { laucher.GameModule.extend = cc.Class.extend; })()");
}

/****/

JSClass  *jsb_ext_laucher_ModuleManager_class;
JSObject *jsb_ext_laucher_ModuleManager_prototype;
JSObject *jsb_ext_laucher_ModuleManager_ns_object;
JSObject *jsb_ext_laucher_ModuleManager_target;

bool js_ext_laucher_ModuleManager_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	JS::RootedObject parent(cx, jsb_ext_laucher_ModuleManager_ns_object);
	JS::RootedObject proto(cx, jsb_ext_laucher_ModuleManager_prototype);
	JS::RootedObject jsObj(cx, JS_NewObject(cx, jsb_ext_laucher_ModuleManager_class, proto, parent));
	args.rval().set(OBJECT_TO_JSVAL(jsObj));
	return true;
}

bool js_ext_laucher_ModuleManager_setJSTarget(JSContext *cx, uint32_t argc, jsval *vp){
	if (argc == 1){
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		jsb_ext_laucher_ModuleManager_target = args.get(0).toObjectOrNull();
		return true;
	}
	return false;
}

bool js_ext_laucher_ModuleManager_getModule(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);

	bool ok = false;
	if (argc == 1) {
		std::string moduleName = "";
		ok = jsval_to_std_string(cx, args[0], &moduleName);
		if (ok){
			auto module = ext::ModuleManager::getInstance()->getModule(moduleName);
			jsval jsret = JSVAL_NULL;
			if (module) {
				jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<ext::GameModule>(cx, (ext::GameModule*)module));
			}
			else {
				jsret = JSVAL_NULL;
			};
			args.rval().set(jsret);
		}
		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_GameModule_loadModule : Error processing arguments");
			
		return true;		
	}

	JS_ReportError(cx, "js_ext_laucher_ModuleManager_getModule : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_ModuleManager_allModuleName(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);

	bool ok = false;
	if (argc == 0) {
		auto names = ext::ModuleManager::getInstance()->allModuleName();
		jsval jsret = std_vector_string_to_jsval(cx, names);
		args.rval().set(jsret);
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_ModuleManager_allModuleName : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool js_ext_laucher_ModuleManager_createSpineFromCache(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);

	bool ok = true;
	if (argc == 1) {
		std::string arg0 = "";
		ok &= jsval_to_std_string(cx, args.get(0), &arg0);
		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_ModuleManager_createSpineFromCache : Error processing arguments");

		jsval jsret = JSVAL_NULL;
		auto item = ext::SpineCache::getInstance()->getItem(arg0);
		if (item){
			auto spine = item->createNewAnimation();
			if (spine){
				jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<spine::SkeletonAnimation>(cx, spine));
			}
		}
		args.rval().set(jsret);
		return true;
	}
	else if (argc == 2){
		std::string arg0 = "";
		ok &= jsval_to_std_string(cx, args.get(0), &arg0);
		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_ModuleManager_createSpineFromCache : Error processing arguments");

		double arg1 = 0;
		ok &= JS::ToNumber(cx, args.get(1), &arg1) && !std::isnan(arg1);
		JSB_PRECONDITION2(ok, cx, false, "js_ext_laucher_ModuleManager_createSpineFromCache : Error processing arguments");

		jsval jsret = JSVAL_NULL;
		auto item = ext::SpineCache::getInstance()->getItem(arg0);
		if (item){
			auto spine = item->createNewAnimation(arg1);
			if (spine){
				jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<spine::SkeletonAnimation>(cx, spine));
			}
		}
		args.rval().set(jsret);
		return true;
	}

	JS_ReportError(cx, "js_ext_laucher_ModuleManager_createSpineFromCache : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

void jsb_ext_laucher_ModuleManager_onUpdateModule(const std::string& moduleName, int current, int target){
	if (jsb_ext_laucher_ModuleManager_target){
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, jsb_ext_laucher_ModuleManager_target);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onUpdateModule", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[3] = {
				std_string_to_jsval(cx, moduleName),
				INT_TO_JSVAL(current),
				INT_TO_JSVAL(target)
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsb_ext_laucher_ModuleManager_target), "onUpdateModule", 3, dataVal, &retval);
		}
	}
}

void jsb_ext_laucher_ModuleManager_onLoadModule(const std::string& moduleName, int current, int target){
	if (jsb_ext_laucher_ModuleManager_target){
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, jsb_ext_laucher_ModuleManager_target);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onLoadModule", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[3] = {
				std_string_to_jsval(cx, moduleName),
				INT_TO_JSVAL(current),
				INT_TO_JSVAL(target)
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsb_ext_laucher_ModuleManager_target), "onLoadModule", 3, dataVal, &retval);
		}
	}
}

void jsb_ext_laucher_ModuleManager_onLoadModuleStatus(const std::string& moduleName, int status){
	if (jsb_ext_laucher_ModuleManager_target){
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, jsb_ext_laucher_ModuleManager_target);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onLoadModuleStatus", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[3] = {
				std_string_to_jsval(cx, moduleName),
				INT_TO_JSVAL(status),
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsb_ext_laucher_ModuleManager_target), "onLoadModuleStatus", 2, dataVal, &retval);
		}
	}
}

void jsb_ext_laucher_ModuleManager_onUnLoadModule(const std::string& moduleName, int current, int target){
	if (jsb_ext_laucher_ModuleManager_target){
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, jsb_ext_laucher_ModuleManager_target);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onUnLoadModule", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[3] = {
				std_string_to_jsval(cx, moduleName),
				INT_TO_JSVAL(current),
				INT_TO_JSVAL(target)
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsb_ext_laucher_ModuleManager_target), "onUnLoadModule", 3, dataVal, &retval);
		}
	}
}

void jsb_ext_laucher_ModuleManager_onUnLoadModuleStatus(const std::string& moduleName, int status){
	if (jsb_ext_laucher_ModuleManager_target){
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, jsb_ext_laucher_ModuleManager_target);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onUnLoadModuleStatus", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[3] = {
				std_string_to_jsval(cx, moduleName),
				INT_TO_JSVAL(status),
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsb_ext_laucher_ModuleManager_target), "onUnLoadModuleStatus", 2, dataVal, &retval);
		}
	}
}

void js_ext_laucher_ModuleManager_finalize(JSFreeOp *fop, JSObject *obj){

}

void js_register_ext_laucher_ModuleManager(JSContext *cx, JS::HandleObject global) {
	jsb_ext_laucher_ModuleManager_class = (JSClass *)calloc(1, sizeof(JSClass));
	jsb_ext_laucher_ModuleManager_class->name = "ModuleManager";
	jsb_ext_laucher_ModuleManager_class->addProperty = JS_PropertyStub;
	jsb_ext_laucher_ModuleManager_class->delProperty = JS_DeletePropertyStub;
	jsb_ext_laucher_ModuleManager_class->getProperty = JS_PropertyStub;
	jsb_ext_laucher_ModuleManager_class->setProperty = JS_StrictPropertyStub;
	jsb_ext_laucher_ModuleManager_class->enumerate = JS_EnumerateStub;
	jsb_ext_laucher_ModuleManager_class->resolve = JS_ResolveStub;
	jsb_ext_laucher_ModuleManager_class->convert = JS_ConvertStub;
	jsb_ext_laucher_ModuleManager_class->finalize = js_ext_laucher_ModuleManager_finalize;
	jsb_ext_laucher_ModuleManager_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

	static JSPropertySpec properties[] = {
		JS_PS_END
	};

	static JSFunctionSpec funcs[] = {
		JS_FN("setTarget", js_ext_laucher_ModuleManager_setJSTarget, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("createSpineFromCache", js_ext_laucher_ModuleManager_createSpineFromCache, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getModule", js_ext_laucher_ModuleManager_getModule, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("allModuleName", js_ext_laucher_ModuleManager_allModuleName, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),	
		JS_FS_END
	};

	JSFunctionSpec *st_funcs = NULL;
	jsb_ext_laucher_ModuleManager_prototype = JS_InitClass(
		cx, global,
		JS::NullPtr(),
		jsb_ext_laucher_ModuleManager_class,
		js_ext_laucher_ModuleManager_constructor, 0, // constructor
		properties,
		funcs,
		NULL, // no static properties
		st_funcs);
}

/****/

void register_all_ext_laucher_GameModule(JSContext* cx, JS::HandleObject obj){
	// Get the ns
	JS::RootedObject ns(cx);
	get_or_create_js_obj(cx, obj, "laucher", &ns);

	jsb_ext_laucher_ModuleManager_ns_object = ns;
	js_register_ext_laucher_GameModule(cx, ns);
	js_register_ext_laucher_ModuleManager(cx, ns);
}