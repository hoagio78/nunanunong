/*
 * GameLaucher.cpp
 *
 *  Created on: Jun 27, 2016
 *      Author: ext
 */

#include "GameLaucher.h"
#include <stdint.h> // for ssize_t on android
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/prettywriter.h"
#include "../Plugin/SystemPlugin.h"
#include "network/HttpClient.h"
#include "EngineUtilsThreadSafe.h"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include "WorkerManager.h"
//#include "../Plugin/HttpFileDownloader.h"
#include "GameModule.h"
#include "platform/decryptor/Decryptor.h"
#include "Plugin/ResourcesDownloader.h"
//

static std::string s_acsUrl = "";

static unsigned char aes_key[16] = { 0x33, 0x5a, 0x35, 0x16, 0x96, 0xff, 0xe8, 0x20, 0xa1, 0x62, 0x16, 0xbe, 0x77, 0x6a, 0x4e, 0xea };
std::string _createJsonConfig(const std::map<std::string, std::string>& params){
	rapidjson::Document document;
	document.SetObject();

	for (auto it = params.begin(); it != params.end(); it++){
		rapidjson::Value value;
		value.SetString(it->second.data(), it->second.size(), document.GetAllocator());

		rapidjson::Value key;
		key.SetString(it->first.data(), it->first.size(), document.GetAllocator());
		document.AddMember(key, value, document.GetAllocator());
	}

	rapidjson::StringBuffer buffer;
	buffer.Clear();
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	document.Accept(writer);
	return std::string(buffer.GetString());
}

static const char HEX_CHAR[17] = "0123456789ABCDEF";
std::string _URLEncode(const std::string& data){
	std::string pRet = "";
	for (int i = 0; i<data.size(); i++){
		if (('0' <= data[i] && data[i] <= '9') ||
			('a' <= data[i] && data[i] <= 'z') ||
			('A' <= data[i] && data[i] <= 'Z') ||
			(data[i] == '-' || data[i] == '_' || data[i] == '.' || data[i] == '~')){
			pRet.append(&data[i], 1);
		}
		else{
			//to hext
			pRet.append("%");
			char dig1 = (data[i] & 0xF0) >> 4;
			char dig2 = (data[i] & 0x0F);
			pRet.append(&HEX_CHAR[dig1], 1);
			pRet.append(&HEX_CHAR[dig2], 1);
		}
	}
	return pRet;
}

namespace ext {

GameLaucher::GameLaucher() {
	// TODO Auto-generated constructor stub
	_versionFile = new GameFile();
	versionFile = "version.json";
	versionHash = "";
	resourceHost = "";
	acsUpdate = false;
	forceUpdate = false;
}

GameLaucher::~GameLaucher() {
	// TODO Auto-generated destructor stub
	if (_versionFile){
		delete _versionFile;
		_versionFile = 0;
	}
}

std::string GameLaucher::getResourcesHost(){
	return resourceHost;
}

void GameLaucher::runOnUI(const std::function<void()>& cb){
	Director::getInstance()->getScheduler()->performFunctionInCocosThread(cb);
}

void GameLaucher::initLaucher(){
	
}

void GameLaucher::requestGetUpdate(){
	EngineUtilsThreadSafe::getInstance()->fileUtilsPurgeCachedEntries();

	std::string externalPath = FileUtils::getInstance()->getWritablePath() + "Game/";
	FileUtils::getInstance()->setSearchPaths({ externalPath, "src/" });
	//read config

	Data data = EngineUtilsThreadSafe::getInstance()->getFileData("project.json");
	std::string json((char*)data.getBytes(), data.getSize());

	rapidjson::Document doc;
	bool error = doc.Parse<0>(json.data()).HasParseError();
	if (!error){
		if (doc.HasMember("acsUpdate")){
			s_acsUrl = doc["acsUpdate"].GetString();
			if (!s_acsUrl.empty()){
				this->updateByACS(s_acsUrl);
				return;
			}
		}

		if (doc.HasMember("forceUpdate")){
			std::string url = doc["forceUpdate"].GetString();
			if (!url.empty()){
				this->updateByForceUpdate(url);
				return;
			}
		}	
	}
	this->updateLoadInternal();
}

/*acs config*/
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define ACS_PLATFORM_NAME "Android"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define ACS_PLATFORM_NAME "iOS"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#define ACS_PLATFORM_NAME "WinPhone"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define ACS_PLATFORM_NAME "iOS"
#else
#define ACS_PLATFORM_NAME "iOS"
#endif

#define ACS_CONFIG_FILE "acs";
//#define BUILD_FOR_APPSTORE 1

void GameLaucher::updateByACS(const std::string& _url){
	acsUpdate = true;
	forceUpdate = false;
	loadInternal = false;

	std::string url = _url;
	if (url[url.size() - 1] != '/'){
		url = url + "/";
	}

	std::string httpString = url + ACS_CONFIG_FILE;
	CCLOG("httpString: %s", httpString.c_str());

	ResourcesDownloader::getInstance()->loadResources(httpString, ext::ResourcesType::kResourcesTypeBinary, [=](ext::Resources* res) {
		if (res->isSuccess()){
			auto mData = res->getData();
			std::string json = "";
			bool isEncrypt = decryptor::Decryptor::getInstance()->isDataEncrypted((const char*)mData.data(), mData.size());
			if (isEncrypt){
				std::vector<char> decryptBuffer;
				decryptor::Decryptor::getInstance()->decyrpt(decryptBuffer, (const char*)mData.data(), mData.size());
				json = std::string(decryptBuffer.data(), decryptBuffer.size());
			}
			else{
				json = std::string((const char*)mData.data(), mData.size());
			}

			CCLOG("json: %s", json.c_str());

			rapidjson::Document doc;
			bool error = doc.Parse<0>(json.data()).HasParseError();
			if (!error){
				bool isHasData = false;
				rapidjson::Value data;
#ifdef BUILD_FOR_APPSTORE
				std::string versionName = ext::SystemPlugin::getInstance()->getVersionName();
				std::string bundleName = ext::SystemPlugin::getInstance()->getPackageName();
				std::string appstoreKey = bundleName + versionName + ACS_PLATFORM_NAME;

				if (!isHasData && doc.HasMember(appstoreKey.c_str())) {
					data = doc[appstoreKey.c_str()];
					isHasData = true;
				}
#endif
				if (!isHasData && doc.HasMember("data")) {
					data = doc["data"];
					isHasData = true;
				}

				if (isHasData) {
					bool isInReview = false;
					if (data.HasMember("demo")) {
						isInReview = data["demo"].GetBool();
					}

					if (isInReview) { //review						
						if (FileUtils::getInstance()->isFileExist("version.json")) { //ignore if version.json exist
							this->updateLoadInternal();
							return;
						}
						else {
							CCLOG("is demo not exist version.json -> load resource from host");
						}
					}

					std::string updateHost = data["host"].GetString();
					std::string versionHash = data["versionHash"].GetString();

					CCLOG("updateHost: %s", updateHost.c_str());
					CCLOG("hashVersionFile: %s", versionHash.c_str());

					this->resourceHost = updateHost;
					this->versionHash = versionHash;

					std::string externalPath = FileUtils::getInstance()->getWritablePath() + "Game/";
					FileUtils::getInstance()->setSearchPaths({ externalPath, "res/Game/" });
					FileUtils::getInstance()->addSearchPath("src/", false);
					FileUtils::getInstance()->purgeCachedEntries();

					this->checkVersionFile();
					return;
				}
			}
			else{
				CCLOG("ACS error getConfig");
			}

		}
		else{
			CCLOG("ACS error network");
		}

		this->onProcessStatus(GameLaucherStatus::UpdateFailure);

		//retry
		Director::getInstance()->getScheduler()->schedule([=](float) {
			Director::getInstance()->getScheduler()->unschedule("acs_update", this);
			this->updateByACS(s_acsUrl);
		}, this, 1.0, false, "acs_update");
	}, false);
}

void GameLaucher::updateByForceUpdate(const std::string& url){
	acsUpdate = false;
	forceUpdate = true;
	loadInternal = false;

	std::string externalPath = FileUtils::getInstance()->getWritablePath() + "Game/";
	FileUtils::getInstance()->setSearchPaths({ externalPath, "res/Game/" });
	FileUtils::getInstance()->addSearchPath("src/", false);
	FileUtils::getInstance()->purgeCachedEntries();

	resourceHost = url;
	this->versionHash = "";
	this->checkVersionFile();
}

void GameLaucher::updateLoadInternal(){
	acsUpdate = false;
	forceUpdate = false;
	loadInternal = true;

	FileUtils::getInstance()->setSearchPaths({ "src/" });
	FileUtils::getInstance()->purgeCachedEntries();
	this->checkAllFiles();
}

void GameLaucher::run(){
	this->onProcessStatus(GameLaucherStatus::GetUpdate);
	this->requestGetUpdate();
}

void GameLaucher::checkVersionFile(){
	this->onProcessStatus(GameLaucherStatus::TestVersion);

	WorkerManager::getInstance()->pushAction([=](){
		if (resourceHost.at(resourceHost.size() - 1) != '/'){
			resourceHost += '/';
		}
		_versionFile->fileName = "version.json";
		_versionFile->md5Digest = versionHash;
		std::transform(_versionFile->md5Digest.begin(), _versionFile->md5Digest.end(), _versionFile->md5Digest.begin(), ::tolower);
		_versionFile->fileSize = 0;

		if (!_versionFile->isReady()){
			_versionFile->update(resourceHost, nullptr, [=](int returnCode){
				if (returnCode == 0){
					std::string filePath = _versionFile->filePath;
					this->runOnUI([=]() {
						this->versionFile = filePath;
						this->checkAllFiles();
					});
				}
				else{
					this->runOnUI([=]() {
						this->onProcessStatus(GameLaucherStatus::UpdateFailure);
					});
					this->checkVersionFile();
				}
			});
		}
		else{
			std::string filePath = _versionFile->filePath;
			this->runOnUI([=](){
				this->versionFile = filePath;
				this->checkAllFiles();
			});
		}
	});
}

void GameLaucher::checkAllFiles(){
	WorkerManager::getInstance()->pushAction([=](){
		if (!ModuleManager::getInstance()->initFromFile(this->versionFile)){
			this->onProcessStatus(GameLaucherStatus::UpdateFailure);
			return;
		}

		EngineUtilsThreadSafe::getInstance()->fileUtilsPurgeCachedEntries();
		this->loadMainModule();
	});
}

void GameLaucher::loadMainModule(){
	this->runOnUI([=](){
		/*send event to loading scene*/
		auto scene = Director::getInstance()->getRunningScene();
		js_proxy_t * p = jsb_get_native_proxy(scene);
		if (p){
			auto sc = ScriptingCore::getInstance();
			auto cx = sc->getGlobalContext();
			auto global = sc->getGlobalObject();
			JSAutoCompartment ac(cx, global);

			JS::RootedObject jstarget(cx, p->obj);
			JS::RootedValue value(cx);
			bool ok = JS_GetProperty(cx, jstarget, "onStartLoadModule", &value);
			if (ok && !value.isNullOrUndefined()){
				JS::RootedValue retval(cx);
				auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(p->obj), "onStartLoadModule", 0, 0, &retval);
			}
		}
		
		//load main module
		GameModule* mainModule = ModuleManager::getInstance()->getModule("main");
		mainModule->loadModule([=](bool success){
			/*if (success){
				this->finishLaucher();
			}*/
		});
	});
}

void GameLaucher::finishLaucher(){
	this->onProcessStatus(GameLaucherStatus::Finished);
}

void GameLaucher::onProcessStatus(int status){
	this->status = status;
	//CCLOG("onProcessStatus: %d", status);
	//call js event running scene;
	auto scene = Director::getInstance()->getRunningScene();
	js_proxy_t * p = jsb_get_native_proxy(scene);
	if (p){	
		auto sc = ScriptingCore::getInstance();
		auto cx = sc->getGlobalContext();
		auto global = sc->getGlobalObject();
		JSAutoCompartment ac(cx, global);

		JS::RootedObject jstarget(cx, p->obj);
		JS::RootedValue value(cx);
		bool ok = JS_GetProperty(cx, jstarget, "onProcessStatus", &value);
		if (ok && !value.isNullOrUndefined()){
			jsval dataVal[1] = {
				INT_TO_JSVAL(this->status)
			};
			JS::RootedValue retval(cx);
			auto ret = sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(p->obj), "onProcessStatus", 1, dataVal, &retval);
		}
	}
}

static GameLaucher* s_GameLaucher = 0;
GameLaucher* GameLaucher::getInstance(){
	if (!s_GameLaucher){
		s_GameLaucher = new GameLaucher();
		s_GameLaucher->initLaucher();
	}
	return s_GameLaucher;
}


} /* namespace ext */
