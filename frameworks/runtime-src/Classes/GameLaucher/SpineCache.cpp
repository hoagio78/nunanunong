/*
 * SpineCache.cpp
 *
 *  Created on: Jul 6, 2016
 *      Author: ext
 */

#include "SpineCache.h"
#include "spine/spine-cocos2dx.h"
#include "spine/extension.h"

namespace ext {

SpineDataItem::SpineDataItem(const std::string& name){
	atlasData = 0;
	defaultSkeletonData = 0;
	this->spineName = name;
	_loaded = false;
}

SpineDataItem::~SpineDataItem(){
	this->unload();
}

void SpineDataItem::initWithData(const rapidjson::Value& data){
	if (!this->jsonFile.empty()){
		return;
	}

	this->jsonFile = data["json"].GetString();
	this->atlasFile = data["atlas"].GetString();
	const rapidjson::Value& texData = data["texture"];
	for (int i = 0; i < texData.Size(); i++){
		this->textureFiles.push_back(texData[i].GetString());
	}
}

void SpineDataItem::load(){
	if (_loaded){
		return;
	}
	_loaded = true;
	CCLOG("load spine: %s", this->spineName.c_str());

	atlasData = spAtlas_createFromFile(atlasFile.c_str(), 0);

	auto _attachmentLoader = SUPER(Cocos2dAttachmentLoader_create(atlasData));
	spSkeletonJson* json = spSkeletonJson_createWithLoader(_attachmentLoader);
	json->scale = 1.0;
	spSkeletonData* skeletonData = spSkeletonJson_readSkeletonDataFile(json, jsonFile.c_str());
	CCASSERT(skeletonData, json->error ? json->error : "Error reading skeleton data.");
	spSkeletonJson_dispose(json);

	defaultSkeletonData = skeletonData;
}

void SpineDataItem::unload(){
	if (!_loaded){
		return;
	}

	CCLOG("unload spine: %s", this->spineName.c_str());

	if (atlasData){
		spAtlas_dispose(atlasData);
		atlasData = 0;
	}

	if (defaultSkeletonData){
		spSkeletonData_dispose(defaultSkeletonData);
		defaultSkeletonData = 0;
	}

	_loaded = false;
}

spine::SkeletonAnimation* SpineDataItem::createNewAnimation(float scale){
	if (!_loaded){
		return spine::SkeletonAnimation::createWithJsonFile(this->jsonFile, this->atlasFile, scale);
	}
	if (scale == 1.0){
		return spine::SkeletonAnimation::createWithData(defaultSkeletonData, false);
	}
	else{
		auto _attachmentLoader = SUPER(Cocos2dAttachmentLoader_create(atlasData));
		spSkeletonJson* json = spSkeletonJson_createWithLoader(_attachmentLoader);
		json->scale = scale;
		spSkeletonData* skeletonData = spSkeletonJson_readSkeletonDataFile(json, jsonFile.c_str());
		CCASSERT(skeletonData, json->error ? json->error : "Error reading skeleton data.");
		spSkeletonJson_dispose(json);

		return spine::SkeletonAnimation::createWithData(skeletonData, true);
	}
}


/****/

SpineCache::SpineCache(){

}

SpineCache::~SpineCache(){
	for (auto it = _caches.begin(); it != _caches.end(); it++){
		delete it->second;
	}
	_caches.clear();
}

void SpineCache::addItem(const std::string& name, SpineDataItem* item){
	_caches.insert(std::make_pair(name, item));
}

SpineDataItem* SpineCache::getItem(const std::string& name){
	auto it = _caches.find(name);
	if (it != _caches.end()){
		return it->second;
	}
	return 0;
}

SpineDataItem* SpineCache::createNewItem(const std::string& name){
	auto item = this->getItem(name);
	if (item){
		return item;
	}
	
	item = new SpineDataItem(name);
	SpineCache::getInstance()->addItem(name, item);
	return item;
}

static SpineCache* s_SpineCache = 0;
SpineCache* SpineCache::getInstance(){
	if (!s_SpineCache){
		s_SpineCache = new SpineCache();
	}
	return s_SpineCache;
}

} /* namespace ext */
