/*
 * ResourceLoader.cpp
 *
 *  Created on: Dec 15, 2015
 *      Author: ext
 */

#include "ResourceLoader.h"
#include "2d/CCFontAtlasCache.h"
#include "audio/include/AudioEngine.h"
#include "GameLaucher.h"
#include "EngineUtilsThreadSafe.h"
#include "WorkerManager.h"

#include "jsapi.h"
#include "jsfriendapi.h"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"

namespace ext {

ResourceLoader::ResourceLoader() {
	// TODO Auto-generated constructor stub
	running = false;

	processHandler = nullptr;
	finishedHandler = nullptr;
}

ResourceLoader::~ResourceLoader() {
	// TODO Auto-generated destructor stub
	Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
	CCLOG("ResourceLoader::~ResourceLoader");
}

void ResourceLoader::addTexture(const std::string &img, const std::string &plist){
	_texture.push_back(img);
	if (!plist.empty()){
		_frames.push_back(plist);
	}
}

void ResourceLoader::addBMFont(const std::string &texture, const std::string &font){
	_texture.push_back(texture);
	_bmfonts.push_back(font);
}

void ResourceLoader::addSound(const std::string& sound){
	_sound.push_back(sound);
}

void ResourceLoader::addScript(const std::string& script){
	_scripts.push_back(script);
}

void ResourceLoader::addSpine(const std::string& item){
    for(int i=0;i<_spine.size();i++){
        if(_spine[i] == item){
            return;
        }
    }
	_spine.push_back(item);
}

void ResourceLoader::runOnUI(const std::function<void()>& runable){
	cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread(runable);
}

void ResourceLoader::loadTexture(std::string img, WorkerTicket* ticket){
	std::string fullpath = EngineUtilsThreadSafe::getInstance()->fullPathForFilename(img);
	auto tex = EngineUtilsThreadSafe::getInstance()->getTextureForKey(fullpath);
	if (tex){
		EngineUtilsThreadSafe::getInstance()->setFileLoaded(img);
		currentStep++;
		ticket->done(true);
	}
	else{
		WorkerManager::getInstance()->pushAction([=](){
			Data data = EngineUtilsThreadSafe::getInstance()->getFileData(fullpath);
			if (!data.isNull()){
				Image* imageData = new Image();
				if (imageData->initWithImageData(data.getBytes(), data.getSize())){
					this->runOnUI([=](){
						Texture2D* texture = TextureCache::getInstance()->addImage(imageData, fullpath);
						EngineUtilsThreadSafe::getInstance()->setFileLoaded(img);
						imageData->release();
						currentStep++;
						ticket->done(true);
					});
					return;
				}
			}

			CCLOG("loadTexture error: ", img.c_str());
			this->runOnUI([=](){
				ticket->done(false);
			});		
		});
	}
}

void ResourceLoader::update(float dt){
	if (running){
		if (isLoad){
			this->updateLoadResources(); 
		}
		else{
			this->updateUnloadResources();
		}
	}
}

void ResourceLoader::updateLoadResources(){
	switch (step)
	{
		case kStepLoadImage:
		{
			if (_texture.size() <= 0){
				step = kStepLoadFrames;
			}
			else{
				step = kStepWaiting;
				auto ticket = WorkerTicket::create(_texture.size());
				ticket->finishedCallback = [=](bool success){
					if (success){
						index = 0;
						step = kStepLoadFrames;
					}
					else{
						CCLOG("LOAD TEXTURE ERROR");
						step = kStepFinish;
					}
				};

				for (int i = 0; i < _texture.size(); i++){
					auto textureImg = _texture[i];
					EngineUtilsThreadSafe::getInstance()->fullPathForFilename(textureImg);
					CCLOG("loading texture: %s", textureImg.c_str());
					this->loadTexture(textureImg, ticket);
				}
			}

			break;
		}

		case kStepLoadFrames:
		{
			if (index < _frames.size()){
				auto plist = _frames[index];
				EngineUtilsThreadSafe::getInstance()->fullPathForFilename(plist);

				if (int a = EngineUtilsThreadSafe::getInstance()->getFileLoadedCount(plist) > 0){
					CCLOG("loaded frame: %s - %d", plist.c_str(), a);
				}
				else{
					CCLOG("loading frame: %s", plist.c_str());
					SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plist);
				}
				EngineUtilsThreadSafe::getInstance()->setFileLoaded(plist);

				currentStep++;
				onProcessLoader();

				index++;
			}
			else{
				index = 0;
				step = kStepLoadFont;
			}
			break;
		}

		case kStepLoadFont:
		{
			if (index < _bmfonts.size()){
				auto fnt = _bmfonts[index];
				EngineUtilsThreadSafe::getInstance()->fullPathForFilename(fnt);

				if (int a = EngineUtilsThreadSafe::getInstance()->getFileLoadedCount(fnt) > 0){
					CCLOG("loaded font: %s - %d", fnt.c_str(), a);
				}
				else{			
					if (!FontAtlasCache::getFontAtlasFNT(fnt)){
						CCLOG("loading font error: %s", fnt.c_str());
						step = kStepFinish;
						return;
					}
					else{
						CCLOG("loading font: %s", fnt.c_str());
					}
				}
				EngineUtilsThreadSafe::getInstance()->setFileLoaded(fnt);

				currentStep++;
				onProcessLoader();

				index++;
			}
			else{
				index = 0;
				step = kStepLoadSpine;
			}

			break;
		}

		case kStepLoadSpine:
		{
			if (index < _spine.size()){
				auto n = _spine.size() - index;
				if (n > 1){
					n = 1;
				}
				for (auto i = 0; i < n; i++){
					CCLOG("load spine: %s", _spine[index + i].c_str());
					auto spine = SpineCache::getInstance()->getItem(_spine[index + i]);
					spine->load();
				}

				index += n;
				currentStep += n;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepLoadSound;
			}
			break;
		}

		case kStepWaiting:
		{
			break;
		}

		case kStepLoadSound:
		{
			if (index < _sound.size()){
				auto n = _sound.size() - index;
				if (n > 10){
					n = 10;
				}
				for (auto i = 0; i < n; i++){
					auto sound = _sound[index + i];
					if (int a = EngineUtilsThreadSafe::getInstance()->getFileLoadedCount(sound) > 0){
						CCLOG("sound loaded: %s - %d", sound.c_str(), a);
					}
					else{
						CCLOG("load sound: %s", sound.c_str());
						cocos2d::experimental::AudioEngine::preload(sound);
					}
					EngineUtilsThreadSafe::getInstance()->setFileLoaded(sound);
				}

				index += n;
				currentStep += n;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepLoadScript;
			}
			break;
		}

		case kStepLoadScript:
		{
			if (index < _scripts.size()){
				auto n = _scripts.size() - index;
				if (n > 10){
					n = 10;
				}
				for (auto i = 0; i < n; i++){
					auto script = _scripts[index + i];

					if (EngineUtilsThreadSafe::getInstance()->getFileLoadedCount(script) > 0){
						CCLOG("loaded script: %s", script.c_str());
					}
					else{
						CCLOG("load script: %s", script.c_str());
						ScriptingCore::getInstance()->runScript(script);
						EngineUtilsThreadSafe::getInstance()->setFileLoaded(script);
					}
				}

				index += n;
				currentStep += n;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepPreFinish;
			}
			break;
		}

		case kStepPreFinish:
		{
			currentStep++;
			onProcessLoader();
			step = kStepFinish;
			break;
		}

		case kStepFinish:
		{
			running = false;
			Director::getInstance()->getScheduler()->unscheduleUpdateForTarget(this);
			if (finishedHandler){
				finishedHandler(currentStep == targetStep);
			}
			break;
		}
	}
}

void ResourceLoader::updateUnloadResources(){
	switch (step)
	{
		case kStepUnLoadFrames:
		{
			if (index < _frames.size()){
				auto plist = _frames[index];
				EngineUtilsThreadSafe::getInstance()->fullPathForFilename(plist);
				if (EngineUtilsThreadSafe::getInstance()->removeFileLoaded(plist) <= 0){
					CCLOG("unload frame: %s", plist.c_str());
					SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(plist);
				}
				else{
					CCLOG("no unload frame: %s", plist.c_str());
				}

				currentStep++;
				onProcessLoader();

				index++;
			}
			else{
				index = 0;
				step = kStepUnLoadFont;
			}
			break;
		}

		case kStepUnLoadFont:
		{
			if (index < _bmfonts.size()){
				auto fnt = _bmfonts[index];
				EngineUtilsThreadSafe::getInstance()->fullPathForFilename(fnt);
				if(EngineUtilsThreadSafe::getInstance()->removeFileLoaded(fnt) <= 0){
					CCLOG("unload fnt: %s", fnt.c_str());
				}
				else{
					CCLOG("NO unload fnt: %s", fnt.c_str());
				}
				
				currentStep++;
				onProcessLoader();

				index++;
			}
			else{
				index = 0;
				step = kStepUnLoadSpine;
			}

			break;
		}

		case kStepUnLoadSpine:
		{
			if (index < _spine.size()){
				auto n = _spine.size() - index;
				if (n > 1){
					n = 1;
				}
				for (auto i = 0; i < n; i++){
					CCLOG("unload spine: %s", _spine[index + i].c_str());
					auto spine = SpineCache::getInstance()->getItem(_spine[index + i]);
					spine->unload();
				}

				index += n;
				currentStep += n;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepUnLoadImage;
			}
			break;
		}

		case kStepUnLoadImage:
		{
			if (index < _texture.size()){
				auto texName = _texture[index];
				auto fullTexPath = EngineUtilsThreadSafe::getInstance()->fullPathForFilename(texName);
				if (EngineUtilsThreadSafe::getInstance()->removeFileLoaded(texName) <= 0){
					CCLOG("unload texture: %s", texName.c_str());
					TextureCache::getInstance()->removeTextureForKey(fullTexPath);
				}
				else{
					CCLOG("NO unload texture: %s", texName.c_str());
				}

				index ++;
				currentStep ++;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepUnLoadSound;
			}

			break;
		}

		case kStepUnLoadSound:
		{
			if (index < _sound.size()){
				auto n = _sound.size() - index;
				if (n > 10){
					n = 10;
				}
				for (auto i = 0; i < n; i++){
					auto sound = _sound[index + i];
					if (EngineUtilsThreadSafe::getInstance()->removeFileLoaded(sound) <= 0){
						CCLOG("unload sound: %s", sound.c_str());
						cocos2d::experimental::AudioEngine::uncache(sound);
					}
					else{
						CCLOG("NO unload sound: %s", sound.c_str());
					}
				}

				index += n;
				currentStep += n;
				onProcessLoader();
			}
			else{
				index = 0;
				step = kStepPreFinish;
			}
			break;
		}

		case kStepPreFinish:
		{
			currentStep++;
			onProcessLoader();
			step = kStepFinish;
			break;
		}

		case kStepFinish:
		{
			running = false;
			Director::getInstance()->getScheduler()->unscheduleUpdateForTarget(this);
			if (finishedHandler){
				finishedHandler(currentStep == targetStep);
			}
			break;
		}
	}
}

void ResourceLoader::load(){
	isLoad = true;
	running = true;
	index = 0;

	currentStep = 0;
	step = kStepLoadImage;

	targetStep = 1;
	targetStep += _texture.size();
	targetStep += _frames.size();
	targetStep += _bmfonts.size();
	targetStep += _sound.size();
	targetStep += _scripts.size();
	targetStep += _spine.size();
	Director::getInstance()->getScheduler()->scheduleUpdateForTarget(this, 0, false);
}

void ResourceLoader::unload(){
	isLoad = false;
	running = true;
	index = 0;

	currentStep = 0;
	step = kStepUnLoadFrames;

	targetStep = 1;
	targetStep += _texture.size();
	targetStep += _frames.size();
	targetStep += _bmfonts.size();
	targetStep += _sound.size();
	targetStep += _spine.size();
	Director::getInstance()->getScheduler()->scheduleUpdateForTarget(this, 0, false);
}

void ResourceLoader::loadScript(){
	for (int i = 0; i < _scripts.size(); i++){
		auto script = _scripts[i];

		if (EngineUtilsThreadSafe::getInstance()->getFileLoadedCount(script) > 0){
			CCLOG("loaded script: %s", script.c_str());
		}
		else{
			CCLOG("load script: %s", script.c_str());
			ScriptingCore::getInstance()->runScript(script);
			EngineUtilsThreadSafe::getInstance()->setFileLoaded(script);
		}
	}
}

void ResourceLoader::onProcessLoader(){
	if (processHandler){
		processHandler(currentStep, targetStep);
	}
}

} /* namespace ext */
