#include "base/ccConfig.h"
#ifndef __ext_action_h__
#define __ext_action_h__

#include "jsapi.h"
#include "jsfriendapi.h"
extern JSClass  *jsb_ext_CustomAction_class;
extern JSObject *jsb_ext_CustomAction_prototype;

bool js_ext_action_CustomAction_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_ext_action_CustomAction_finalize(JSContext *cx, JSObject *obj);
void js_register_ext_action_CustomAction(JSContext *cx, JS::HandleObject global);
bool js_ext_action_CustomAction_initCustomAction(JSContext *cx, uint32_t argc, jsval *vp);
void register_all_ext_action(JSContext* cx, JS::HandleObject obj);
#endif // __ext_action_h__
