/*
 * LobbyClient.h
 *
 *  Created on: Jun 23, 2016
 *      Author: ext
 */

#ifndef SOCKET_LOBBYCLIENT_H_
#define SOCKET_LOBBYCLIENT_H_

#include "LobbyServer.h"
#include <vector>

namespace ext {
enum LobbyClientType{
	UDT = 0,
	TCP = 1
};

class LobbyClient : public ext::data::LobbyRef{
	ext::net::SocketClient* mClient;

	void onRecvMessage(ext::net::SocketData* data);
	void onRecvStatus(const ext::net::SocketStatusData& data);
	void sendMessage(ext::data::Value* message);
	void sendJSMessage(const std::string& messageName, const std::string& value);
public:
	LobbyClient();
	virtual ~LobbyClient();

	void initClient();
	void update(float dt);

	void connect(const std::string& host, int port);
	void close();
	void closeLostPing();
	void send(const std::string& json);
	
	int getStatus();
};

} /* namespace ext */

#endif /* SOCKET_LOBBYCLIENT_H_ */
