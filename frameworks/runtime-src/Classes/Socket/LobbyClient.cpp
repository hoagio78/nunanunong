/*
 * LobbyClient.cpp
 *
 *  Created on: Jun 23, 2016
 *      Author: ext
 */

#include "LobbyClient.h"
#include "cocos2d.h"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "scripting/js-bindings/manual/ScriptingCore.h"
USING_NS_CC;

namespace ext {

LobbyClient::LobbyClient() {
	// TODO Auto-generated constructor stub
	mClient = 0;
}

LobbyClient::~LobbyClient() {
	// TODO Auto-generated destructor stub
	Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
	if (mClient){
		mClient->release();
		mClient = 0;
	}
}

void LobbyClient::sendJSMessage(const std::string& messageName, const std::string& value){
	js_proxy_t* p = jsb_get_native_proxy(this);
	if (!p){
		//error
		return;
	}
	ScriptingCore* sc = ScriptingCore::getInstance();
	if (sc){
		jsval dataVal[] = {
			dataVal[0] = std_string_to_jsval(sc->getGlobalContext(), messageName),
			dataVal[1] = std_string_to_jsval(sc->getGlobalContext(), value)
		};
		sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(p->obj), "nativeOnEvent", 2, dataVal);
	}
}

void LobbyClient::initClient(){
	mClient = new ext::net::TCPClient();
	mClient->_recvCallback = CC_CALLBACK_1(LobbyClient::onRecvMessage, this);
	mClient->_statusCallback = CC_CALLBACK_1(LobbyClient::onRecvStatus, this);
	Director::getInstance()->getScheduler()->scheduleUpdate(this, INT_MAX, false);
}

void LobbyClient::update(float dt){
	if (mClient){
		mClient->processMessage();
	}
}

void LobbyClient::sendMessage(ext::data::Value* message){
	if (mClient){
		mClient->sendMessage(message);
	}
}

void LobbyClient::connect(const std::string& host, int port){
	if (mClient){	
		mClient->connectTo(host, port);
	}
}

void LobbyClient::send(const std::string& json){
	auto message = ext::data::ValueJson::create(json);
	this->sendMessage(message);
}

void LobbyClient::closeLostPing(){
	CCLOG("lost ping");
	mClient->closeSocket();
}

void LobbyClient::close(){
	if (mClient){
		mClient->closeClient();
	}
}

void LobbyClient::onRecvMessage(ext::net::SocketData* data){
	ext::data::ValueJson* json = (ext::data::ValueJson*)data;
	this->sendJSMessage("message", json->getJSON());
}

void LobbyClient::onRecvStatus(const ext::net::SocketStatusData& data){
	this->sendJSMessage("socketStatus", ext::net::SocketStatusName(data.status));
}

int LobbyClient::getStatus(){
	if (mClient){
		return mClient->getStatus();
	}
	return -1;
}


} /* namespace ext */
