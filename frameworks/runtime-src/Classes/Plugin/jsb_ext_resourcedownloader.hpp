#include "base/ccConfig.h"
#ifndef __ext_resourcesloader_h__
#define __ext_resourcesloader_h__

#include "jsapi.h"
#include "jsfriendapi.h"
#include <string>

void register_all_ext_resourcesloader(JSContext* cx, JS::HandleObject obj);

#endif // __ext_resourcesloader_h__
