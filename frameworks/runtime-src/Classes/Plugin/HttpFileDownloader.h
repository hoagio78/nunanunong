/*
 * HttpFileDownloader.h
 *
 *  Created on: Mar 23, 2016
 *      Author: ext
 */

#ifndef PLUGIN_HttpFileDownloader_H_
#define PLUGIN_HttpFileDownloader_H_

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <queue>
#include <thread>
#include <functional>
#include <condition_variable>
#include "MD5.h"
namespace ext{

enum DownloadResult{
	OK = 0,
	NETWORK_ERROR = 2,
	FILE_ERROR = 3,
	CURL_ERROR = 4
};

class DownloadRequest{
protected:
	FILE* fileWrite;
	void onFinished(int returnCode);
public:
	std::string _url;
	std::string _fileName;
	std::function<void(int)> finishedCallback;
	std::function<void(unsigned char* data, size_t size)> processCallback;
public:
	DownloadRequest();
	DownloadRequest(const std::string& url, const std::string& fileName = "");
	virtual ~DownloadRequest();

	virtual size_t writeDataHandler(void *ptr, size_t size, size_t nmemb);
	virtual size_t writeHeaderHandler(void *ptr, size_t size, size_t nmemb);

	virtual void finishedTask(bool success);
	virtual bool initDownloadHandler(void* handler);
};

class HttpFileDownloader{
	std::queue<DownloadRequest*> _request;
	std::condition_variable _request_condition_variable;

	std::mutex _request_mutex;
	DownloadRequest* takeRequest();

	void updateThread();
	void downloadThread();

	void init();
public:
	HttpFileDownloader();
	virtual ~HttpFileDownloader();

	static size_t writeDataHandler(void *ptr, size_t size, size_t nmemb, DownloadRequest* writer);
	static size_t writeHeaderHandler(void *ptr, size_t size, size_t nmemb, DownloadRequest* writer);

	void addRequest(DownloadRequest* request);

	static HttpFileDownloader* getInstance();
};

}
#endif /* PLUGIN_HttpFileDownloader_H_ */
