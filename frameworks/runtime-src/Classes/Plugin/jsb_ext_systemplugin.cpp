#include "jsb_ext_systemplugin.hpp"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include "SystemPlugin.h"
#include "../GameLaucher/GameLaucher.h"

JSClass  *jsb_ext_systemplugin_class;
JSObject *jsb_ext_systemplugin_prototype;
JSObject *jsb_ext_systemplugin_ns_object;

bool jsb_ext_systemplugin_textureFromBase64(JSContext *cx, uint32_t argc, jsval *vp){
	if (argc == 1){
		std::string base64Data;
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
 
		bool ok = jsval_to_std_string(cx, args[0], &base64Data);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_systemplugin_textureFromBase64 : Error processing arguments");

		Texture2D* img = ext::SystemPlugin::getInstance()->textureFromBase64(base64Data);
		jsval jsret;
		if (img){
			jsret = OBJECT_TO_JSVAL(js_get_or_create_jsobject<cocos2d::Image>(cx, (cocos2d::Image*)img));
		}
		else{
			jsret = JSVAL_NULL;
		};
		args.rval().set(jsret);
		return true;
	}
	JS_ReportError(cx, "jsb_ext_systemplugin_textureFromBase64 : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_systemplugin_enableMipmapTexture(JSContext *cx, uint32_t argc, jsval *vp){
	if (argc == 1){
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		std::string textureName;
		bool ok = true;
		ok = jsval_to_std_string(cx, args.get(0), &textureName);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_systemplugin_enableMipmapTexture : Error processing arguments");
		if (ok){
			ext::SystemPlugin::getInstance()->enableMipmapTexture(textureName);
		}
		args.rval().setUndefined();
		return true;
	}
	return false;
}

bool jsb_ext_systemplugin_exitApp(JSContext *cx, uint32_t argc, jsval *vp){
	if (argc == 0){
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
        exit(0);
#endif
		return true;
	}
	return false;
}

bool jsb_ext_systemplugin_startLaucher(JSContext *cx, uint32_t argc, jsval *vp){
	if (argc == 0){
        ext::GameLaucher::getInstance()->run();
		return true;
	}
	return false;
}

bool jsb_ext_systemplugin_getDeviceUUID(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	std::string deviceUUID = "";
	if (argc == 0){
		deviceUUID = ext::SystemPlugin::getInstance()->getDeviceUUID();
	}
	else if (argc == 1){
		std::string key;
		jsval_to_std_string(cx, args.get(0), &key);
		deviceUUID = ext::SystemPlugin::getInstance()->getDeviceUUID(key);
	}
	args.rval().set(std_string_to_jsval(cx, deviceUUID));
	return true;
}

bool jsb_ext_systemplugin_showImagePicker(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);

	if (argc == 2){
		bool ok = true;
		int maxWidth = 0;
		int maxHeight = 0;

		ok = jsval_to_int(cx, args.get(0), &maxWidth);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_systemplugin_showImagePicker : Error processing arguments");

		ok = jsval_to_int(cx, args.get(1), &maxHeight);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_systemplugin_showImagePicker : Error processing arguments");

		ext::SystemPlugin::getInstance()->showImagePicker(maxWidth, maxHeight);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 0){
		ext::SystemPlugin::getInstance()->showImagePicker(0, 0);
		args.rval().setUndefined();
		return true;
	}

	return false;
}

bool js_ext_systemplugin_constructor(JSContext *cx, uint32_t argc, jsval *vp){
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	JS::RootedObject parent(cx, jsb_ext_systemplugin_ns_object);
	JS::RootedObject proto(cx, jsb_ext_systemplugin_prototype);
	JS::RootedObject jsObj(cx, JS_NewObject(cx, jsb_ext_systemplugin_class, proto, parent));
	args.rval().set(OBJECT_TO_JSVAL(jsObj));
	return true;
}

void js_ext_systemplugin_finalize(JSFreeOp *fop, JSObject *obj){

}

void js_register_ext_systemplugin(JSContext *cx, JS::HandleObject global) {
	jsb_ext_systemplugin_class = (JSClass *)calloc(1, sizeof(JSClass));
	jsb_ext_systemplugin_class->name = "SystemPlugin";
	jsb_ext_systemplugin_class->addProperty = JS_PropertyStub;
	jsb_ext_systemplugin_class->delProperty = JS_DeletePropertyStub;
	jsb_ext_systemplugin_class->getProperty = JS_PropertyStub;
    jsb_ext_systemplugin_class->setProperty = JS_StrictPropertyStub;
    jsb_ext_systemplugin_class->enumerate = JS_EnumerateStub;
    jsb_ext_systemplugin_class->resolve = JS_ResolveStub;
    jsb_ext_systemplugin_class->convert = JS_ConvertStub;
    jsb_ext_systemplugin_class->finalize = js_ext_systemplugin_finalize;
    jsb_ext_systemplugin_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
		JS_FN("enableMipmapTexture", jsb_ext_systemplugin_enableMipmapTexture, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getDeviceUUID", jsb_ext_systemplugin_getDeviceUUID, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getDeviceUUIDWithKey", jsb_ext_systemplugin_getDeviceUUID, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("exitApp", jsb_ext_systemplugin_exitApp, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("startLaucher", jsb_ext_systemplugin_startLaucher, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("textureFromBase64", jsb_ext_systemplugin_textureFromBase64, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("showImagePicker", jsb_ext_systemplugin_showImagePicker, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
			
        JS_FS_END
    };
	
    JSFunctionSpec *st_funcs = NULL;
    jsb_ext_systemplugin_prototype = JS_InitClass(
        cx, global,
        JS::NullPtr(),
        jsb_ext_systemplugin_class,
		js_ext_systemplugin_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);
}

void register_all_ext_systemplugin(JSContext* cx, JS::HandleObject obj) {
    // Get the ns
    JS::RootedObject ns(cx);
    get_or_create_js_obj(cx, obj, "ext", &ns);
	jsb_ext_systemplugin_ns_object = ns;
	js_register_ext_systemplugin(cx, ns);
}

/****/

void jsb_ext_onTakeImageData(ext::ImageBitmap* image){
    ScriptingCore* sc = ScriptingCore::getInstance();
    if (sc){
        JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
        auto cx = sc->getGlobalContext();
        
        JS::RootedObject NativeSystemPluginBrigde(cx);
        get_or_create_js_obj("NativeSystemPluginBrigde", &NativeSystemPluginBrigde);
        
        
        jsval dataVal[1];
        if (image){
            dataVal[0] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<ext::ImageBitmap>(cx, image));
        }
        else{
            dataVal[0] = JSVAL_NULL;
        }
        
        sc->executeFunctionWithOwner(OBJECT_TO_JSVAL(NativeSystemPluginBrigde), "onTakeImageData", 1, dataVal);
    }
}

