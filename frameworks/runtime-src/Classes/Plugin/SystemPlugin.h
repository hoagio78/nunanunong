/*
 * SystemPlugin.h
 *
 *  Created on: Feb 3, 2016
 *      Author: ext
 */

#ifndef PLUGIN_SYSTEMPLUGIN_H_
#define PLUGIN_SYSTEMPLUGIN_H_
#include <string>
#include <vector>
#include "cocos2d.h"
USING_NS_CC;

namespace ext {
class SystemPlugin;
class SoftKeyboardDelegate{
	//friend SystemPlugin;
public:
	SoftKeyboardDelegate();
	virtual ~SoftKeyboardDelegate();
	virtual void onSoftKeyboardShow(float keyboardHeight);
	virtual void onSoftKeyboardHide();
};

class SystemPlugin {
	friend SoftKeyboardDelegate;
private:
	std::vector<SoftKeyboardDelegate*> _keyboardDelegate;
	void addSoftKeyboardDelegate(SoftKeyboardDelegate* delegate);
	void removeSoftKeyboardDelegate(SoftKeyboardDelegate* delegate);
public:
	SystemPlugin();
	virtual ~SystemPlugin();

    std::string getDeviceUUID(std::string nameKeyChain = "" );
	void enableMipmapTexture(const std::string& textureName);

	std::string getVersionName();
	std::string getPackageName();

	/* ecnrypt*/
	std::vector<char> dataEncrypt(const char* key, const char* data, int dataSize);
	std::vector<char> dataDecrypt(const char* key, const char* data, int dataSize);
	std::string dataEncryptBase64(const char* key, const std::string& plainText);
	std::string dataDecryptBase64(const char* key, const std::string& encryptText);
	std::string URLEncode(const std::string& data);
	std::string URLDecode(const std::string& data);

	void showImagePicker(int maxWidth, int maxHeight);
	void onTakeImageData(const std::string& base64Data);

	Texture2D* textureFromBase64(const std::string& base64Data);

	static SystemPlugin* getInstance();
};

} /* namespace ext */

#endif /* PLUGIN_SYSTEMPLUGIN_H_ */
