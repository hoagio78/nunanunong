/*
 * ImageBitmap.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: ext
 */

#include "ImageBitmap.h"
#include "base64.h"
#include <stdio.h>
#include <stdint.h>
#include <array>
#include <vector>

namespace ext {
#define CLAMP(v, min, max) if (v < min) { v = min; } else if (v > max) { v = max; } 

float CubicHermite(float A, float B, float C, float D, float t){
	float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
	float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
	float c = -A / 2.0f + C / 2.0f;
	float d = B;
	return a*t*t*t + b*t*t + c*t + d;
}

float Lerp(float A, float B, float t){
	return A * (1.0f - t) + B * t;
}

ImageBitmap::ImageBitmap(){
	dataLen = 0;
	data = 0;
	jpegDataReady = false;
}

ImageBitmap::~ImageBitmap(){
	if (data){
		delete[] data;
		data = 0;
	}
}

bool ImageBitmap::initWithImageData(const unsigned char* data, int len){
	auto ccImg = new cocos2d::Image();
	if (ccImg->initWithImageData(data, len)){
		int pixelSize = ccImg->getDataLen() / (ccImg->getWidth() * ccImg->getHeight());
		if (pixelSize == 3 || pixelSize == 4){
			this->dataLen = ccImg->getDataLen();
			this->data = new unsigned char[dataLen];
			memcpy(this->data, ccImg->getData(), sizeof(unsigned char) * this->dataLen);
			this->hasAlpha = ccImg->hasAlpha();
			this->width = ccImg->getWidth();
			this->height = ccImg->getHeight();
			this->pixelSize = this->hasAlpha ? 4 : 3;
			
			this->updateStringBase64JPEG();

			return true;
		}
	}
	ccImg->release();

	CCLOG("ImageBitmap format not support");
	return false;
}

bool ImageBitmap::initWithImageData(const std::string& base64Data){
	auto data = base64_decode(base64Data);
	return this->initWithImageData((const unsigned char*)data.data(), data.size());
}

void ImageBitmap::crop(const cocos2d::Rect& rect, const std::function<void(ImageBitmap*)>& callback){
	this->retain();
	if (callback){
		std::thread mThread(&ImageBitmap::_cropByThread, this, rect, callback);
		mThread.detach();
	}
	else{
		this->_cropByThread(rect, callback);
	}
}

void ImageBitmap::resizeTo(const cocos2d::Size& newSize, const std::function<void(ImageBitmap*)>& callback){
	this->retain();
	if (callback){
		std::thread mThread(&ImageBitmap::_resizeByThread, this, newSize, callback);
		mThread.detach();
	}
	else{
		this->_resizeByThread(newSize, callback);
	}
}

void ImageBitmap::_cropByThread(cocos2d::Rect rect, std::function<void(ImageBitmap*)> callback){
	if (!data){
		CCLOG("ImageBitmap no data");
		if (callback){
			cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
				callback(this);
				this->release();
			});
		}
		return;
	}

	jpegDataReady = false;

	auto newImgRect = rect;
	//converto to opengl
	newImgRect.origin.y = this->height - newImgRect.origin.y - newImgRect.size.height;

	if (newImgRect.origin.x < 0){
		newImgRect.origin.x = 0;
	}

	if (newImgRect.origin.y < 0){
		newImgRect.origin.y = 0;
	}

	if (newImgRect.getMaxX() > this->width){
		newImgRect.size.width -= (newImgRect.getMaxX() - this->width);
	}

	if (newImgRect.getMaxY() > this->height){
		newImgRect.size.height -= (newImgRect.getMaxY() - this->height);
	}

	auto newSize = newImgRect.size;
	int pixelSize = (hasAlpha ? 4 : 3);
	unsigned char* newData = new unsigned char[(int)newSize.width * (int)newSize.height * pixelSize];
	unsigned char* old_row_pointers = 0;
	unsigned char* new_row_pointers = 0;

	for (int i = 0; i < (int)(newSize.height); ++i){
		old_row_pointers = data + (i + (int)newImgRect.origin.y) * (this->width * pixelSize) + (int)(newImgRect.origin.x) * pixelSize;
		new_row_pointers = newData + i * ((int)newSize.width * pixelSize);
		memcpy(new_row_pointers, old_row_pointers, (int)newSize.width * pixelSize);
	}

	this->width = newSize.width;
	this->height = newSize.height;
	this->dataLen = (int)newSize.width * (int)newSize.height * (hasAlpha ? 4 : 3);
	delete[] data;
	data = newData;

	this->updateStringBase64JPEG();

	if (callback){
		cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
			callback(this);
			this->release();
		});
	}
}

unsigned char* ImageBitmap::getPixelClamped(int x, int y){
	CLAMP(x, 0, this->width - 1);
	CLAMP(y, 0, this->height - 1);
	return &data[(y * pixelSize * this->width) + x* pixelSize];
}

void ImageBitmap::getSampleNearest(std::vector<unsigned char>& bitmap, float u, float v){
	// calculate coordinates
	int xint = int(u * this->width);
	int yint = int(v * this->height);

	// return pixel
	auto pixel = this->getPixelClamped(xint, yint);
	for (int i = 0; i < pixelSize; ++i){
		bitmap.push_back(pixel[i]);
	}
}

void ImageBitmap::getSampleLinear(std::vector<unsigned char>& bitmap, float u, float v){
	if (pixelSize == 4){
		return this->getSampleLinearWithAlpha(bitmap, u, v);
	}

	// calculate coordinates -> also need to offset by half a pixel to keep image from shifting down and left half a pixel
	float x = (u * this->width) - 0.5f;
	int xint = int(x);
	float xfract = x - floor(x);

	float y = (v * this->height) - 0.5f;
	int yint = int(y);
	float yfract = y - floor(y);

	// get pixels
	auto p00 = this->getPixelClamped(xint + 0, yint + 0);
	auto p10 = this->getPixelClamped(xint + 1, yint + 0);
	auto p01 = this->getPixelClamped(xint + 0, yint + 1);
	auto p11 = this->getPixelClamped(xint + 1, yint + 1);

	// interpolate bi-linearly!
	for (int i = 0; i < 3; ++i){
		float col0 = Lerp(p00[i], p10[i], xfract);
		float col1 = Lerp(p01[i], p11[i], xfract);
		
		float value = Lerp(col0, col1, yfract);
		CLAMP(value, 0.0f, 255.0f);
		bitmap.push_back((unsigned char)value);
	}
}

void ImageBitmap::getSampleLinearWithAlpha(std::vector<unsigned char>& bitmap, float u, float v){
	float x = (u * this->width) - 0.5f;
	int xint = int(x);
	float xfract = x - floor(x);

	float y = (v * this->height) - 0.5f;
	int yint = int(y);
	float yfract = y - floor(y);

	// get pixels
	auto p00 = this->getPixelClamped(xint + 0, yint + 0);
	auto p10 = this->getPixelClamped(xint + 1, yint + 0);
	auto p01 = this->getPixelClamped(xint + 0, yint + 1);
	auto p11 = this->getPixelClamped(xint + 1, yint + 1);

	//cal alpha
	float col0 = Lerp(p00[3], p10[3], xfract);
	float col1 = Lerp(p01[3], p11[3], xfract);

	float alphaValue = Lerp(col0, col1, yfract);
	CLAMP(alphaValue, 0.0f, 255.0f);
	if (alphaValue == 0.0f){
		bitmap.assign({ 0, 0, 0, 0 });
	}

	float a00 = float(p00[3]) / 255.0f;
	float a10 = float(p10[3]) / 255.0f;
	float a01 = float(p01[3]) / 255.0f;
	float a11 = float(p11[3]) / 255.0f;

	// interpolate bi-linearly!
	float a = alphaValue / 255.0f;
	for (int i = 0; i < 3; ++i){
		float col0 = Lerp(float(p00[i]) * a00, float(p10[i]) * a10, xfract);
		float col1 = Lerp(float(p01[i]) * a01, float(p11[i]) * a11, xfract);
		float value = Lerp(col0, col1, yfract) / a;
		CLAMP(value, 0.0f, 255.0f);
		bitmap.push_back((unsigned char)(value));
	}
	bitmap.push_back(alphaValue);
}

void ImageBitmap::getSampleBicubic(std::vector<unsigned char>& bitmap, float u, float v){
	if (pixelSize == 4){
		return this->getSampleBicubicWithAlpha(bitmap, u, v);
	}

	// calculate coordinates -> also need to offset by half a pixel to keep image from shifting down and left half a pixel
	float x = (u * this->width) - 0.5;
	int xint = int(x);
	float xfract = x - floor(x);

	float y = (v * this->height) - 0.5;
	int yint = int(y);
	float yfract = y - floor(y);

	// 1st row
	auto p00 = this->getPixelClamped(xint - 1, yint - 1);
	auto p10 = this->getPixelClamped(xint + 0, yint - 1);
	auto p20 = this->getPixelClamped(xint + 1, yint - 1);
	auto p30 = this->getPixelClamped(xint + 2, yint - 1);

	// 2nd row
	auto p01 = this->getPixelClamped(xint - 1, yint + 0);
	auto p11 = this->getPixelClamped(xint + 0, yint + 0);
	auto p21 = this->getPixelClamped(xint + 1, yint + 0);
	auto p31 = this->getPixelClamped(xint + 2, yint + 0);

	// 3rd row
	auto p02 = this->getPixelClamped(xint - 1, yint + 1);
	auto p12 = this->getPixelClamped(xint + 0, yint + 1);
	auto p22 = this->getPixelClamped(xint + 1, yint + 1);
	auto p32 = this->getPixelClamped(xint + 2, yint + 1);

	// 4th row
	auto p03 = this->getPixelClamped(xint - 1, yint + 2);
	auto p13 = this->getPixelClamped(xint + 0, yint + 2);
	auto p23 = this->getPixelClamped(xint + 1, yint + 2);
	auto p33 = this->getPixelClamped(xint + 2, yint + 2);

	// interpolate bi-cubically!
	// Clamp the values since the curve can put the value below 0 or above 255
	for (int i = 0; i < pixelSize; ++i)
	{
		float col0 = CubicHermite(p00[i], p10[i], p20[i], p30[i], xfract);
		float col1 = CubicHermite(p01[i], p11[i], p21[i], p31[i], xfract);
		float col2 = CubicHermite(p02[i], p12[i], p22[i], p32[i], xfract);
		float col3 = CubicHermite(p03[i], p13[i], p23[i], p33[i], xfract);
		float value = CubicHermite(col0, col1, col2, col3, yfract);
		CLAMP(value, 0.0f, 255.0f);
		bitmap.push_back((unsigned char)value);
	}
}

void ImageBitmap::getSampleBicubicWithAlpha(std::vector<unsigned char>& bitmap, float u, float v){
	// calculate coordinates -> also need to offset by half a pixel to keep image from shifting down and left half a pixel
	float x = (u * this->width) - 0.5;
	int xint = int(x);
	float xfract = x - floor(x);

	float y = (v * this->height) - 0.5;
	int yint = int(y);
	float yfract = y - floor(y);

	// 1st row
	auto p00 = this->getPixelClamped(xint - 1, yint - 1);
	auto p10 = this->getPixelClamped(xint + 0, yint - 1);
	auto p20 = this->getPixelClamped(xint + 1, yint - 1);
	auto p30 = this->getPixelClamped(xint + 2, yint - 1);

	// 2nd row
	auto p01 = this->getPixelClamped(xint - 1, yint + 0);
	auto p11 = this->getPixelClamped(xint + 0, yint + 0);
	auto p21 = this->getPixelClamped(xint + 1, yint + 0);
	auto p31 = this->getPixelClamped(xint + 2, yint + 0);

	// 3rd row
	auto p02 = this->getPixelClamped(xint - 1, yint + 1);
	auto p12 = this->getPixelClamped(xint + 0, yint + 1);
	auto p22 = this->getPixelClamped(xint + 1, yint + 1);
	auto p32 = this->getPixelClamped(xint + 2, yint + 1);

	// 4th row
	auto p03 = this->getPixelClamped(xint - 1, yint + 2);
	auto p13 = this->getPixelClamped(xint + 0, yint + 2);
	auto p23 = this->getPixelClamped(xint + 1, yint + 2);
	auto p33 = this->getPixelClamped(xint + 2, yint + 2);

	float col0 = CubicHermite(p00[3], p10[3], p20[3], p30[3], xfract);
	float col1 = CubicHermite(p01[3], p11[3], p21[3], p31[3], xfract);
	float col2 = CubicHermite(p02[3], p12[3], p22[3], p32[3], xfract);
	float col3 = CubicHermite(p03[3], p13[3], p23[3], p33[3], xfract);
	float aValue = CubicHermite(col0, col1, col2, col3, yfract);
	CLAMP(aValue, 0.0f, 255.0f);
	if (aValue == 0.0f){
		bitmap.assign({ 0, 0, 0, 0 });
	}

	float a00 = float(p00[3]) / 255.0f;
	float a10 = float(p10[3]) / 255.0f;
	float a20 = float(p20[3]) / 255.0f;
	float a30 = float(p30[3]) / 255.0f;

	float a01 = float(p01[3]) / 255.0f;
	float a11 = float(p11[3]) / 255.0f;
	float a21 = float(p21[3]) / 255.0f;
	float a31 = float(p31[3]) / 255.0f;

	float a02 = float(p02[3]) / 255.0f;
	float a12 = float(p12[3]) / 255.0f;
	float a22 = float(p22[3]) / 255.0f;
	float a32 = float(p32[3]) / 255.0f;

	float a03 = float(p03[3]) / 255.0f;
	float a13 = float(p13[3]) / 255.0f;
	float a23 = float(p23[3]) / 255.0f;
	float a33 = float(p33[3]) / 255.0f;

	float a = aValue / 255.0f;
	// interpolate bi-cubically!
	// Clamp the values since the curve can put the value below 0 or above 255
	for (int i = 0; i < 3; ++i)
	{
		float col0 = CubicHermite(p00[i] * a00, p10[i] * a10, p20[i] * a20, p30[i] * a30, xfract);
		float col1 = CubicHermite(p01[i] * a01, p11[i] * a11, p21[i] * a21, p31[i] * a31, xfract);
		float col2 = CubicHermite(p02[i] * a02, p12[i] * a12, p22[i] * a22, p32[i] * a32, xfract);
		float col3 = CubicHermite(p03[i] * a03, p13[i] * a13, p23[i] * a23, p33[i] * a33, xfract);
		float value = CubicHermite(col0, col1, col2, col3, yfract) / a;
		CLAMP(value, 0.0f, 255.0f);
		bitmap.push_back((unsigned char)value);
	}
	bitmap.push_back(aValue);
}

void ImageBitmap::_resizeByThread(cocos2d::Size newSize, std::function<void(ImageBitmap*)> callback){
	if (!data){
		CCLOG("ImageBitmap no data");
		if (callback){
			cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
				callback(this);
				this->release();
			});
		}
		return;
	}

	jpegDataReady = false;

	int newWidth = (int)newSize.width;
	int newHeight = (int)newSize.height;
	int newDataLen = newWidth * newHeight * pixelSize;
	unsigned char* newData = new unsigned char[newDataLen];

	unsigned char *row = newData;
	std::vector<unsigned char> sample(4);
	for (int y = 0; y < newHeight; ++y)
	{	
		unsigned char *destPixel = row;
		float v = float(y) / float(newHeight - 1);
		for (int x = 0; x < newWidth; ++x)
		{	
			float u = float(x) / float(newWidth - 1);
			sample.clear();
			//auto sample = this->getSampleNearest(u, v);
			this->getSampleLinear(sample, u, v);
			//this->getSampleBicubic(sample, u, v);
			for (int i = 0; i < pixelSize; ++i){
				destPixel[i] = sample[i];
			}
			destPixel += pixelSize;
		}
		row += (pixelSize * newWidth);
	}

	if (data){
		delete[] data;
		data = 0;
	}
	data = newData;
	this->dataLen = newDataLen;
	this->width = newWidth;
	this->height = newHeight;

	this->updateStringBase64JPEG();

	if (callback){
		cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
			callback(this);
			this->release();
		});
	}
}

int ImageBitmap::getWidth(){
	return width;
}

int ImageBitmap::getHeight(){
	return height;
}

void ImageBitmap::updateStringBase64JPEG(){
	if (!jpegDataReady){
		auto buffer = cocos2d::Image::saveBitmapToJPEG(data, width, height, hasAlpha);
		jpegData = base64_encode(buffer.data(), buffer.size());
		jpegDataReady = true;

		//cocos2d::Data newData;
		//newData.copy(buffer.data(), buffer.size());
		//cocos2d::FileUtils::getInstance()->writeDataToFile(newData, "C:/Users/Test/Desktop/test.jpg");
	}
}

std::string ImageBitmap::saveToJPEG(){
	if (!data){
		CCLOG("ImageBitmap no data");
		return "";
	}

	this->updateStringBase64JPEG();
	return jpegData;
}

cocos2d::Texture2D* ImageBitmap::createTexture(){
	if (!data){
		CCLOG("ImageBitmap no data");
		return 0;
	}

	auto tex = new cocos2d::Texture2D();

	tex->initWithData(this->data,
		this->dataLen,
		hasAlpha ? cocos2d::Texture2D::PixelFormat::RGBA8888 : cocos2d::Texture2D::PixelFormat::RGB888,
		this->width,
		this->height,
		cocos2d::Size(this->width, this->height));
	tex->autorelease();
	return tex;
}

} /* namespace ext */
