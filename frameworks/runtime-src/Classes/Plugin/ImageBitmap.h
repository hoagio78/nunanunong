/*
 * ImageBitmap.h
 *
 *  Created on: Feb 3, 2016
 *      Author: ext
 */

#ifndef PLUGIN_IMAGEBITMAP_H_
#define PLUGIN_IMAGEBITMAP_H_
#include "cocos2d.h"

namespace ext {

class ImageBitmap : public cocos2d::Ref{
	int width;
	int height;

	unsigned char* data;
	int dataLen;
	int pixelSize;
	bool hasAlpha;

	void _cropByThread(cocos2d::Rect rect, std::function<void(ImageBitmap*)> callback);
	void _resizeByThread(cocos2d::Size newSize, std::function<void(ImageBitmap*)> callback);

	unsigned char* getPixelClamped(int x, int y);
	void getSampleNearest(std::vector<unsigned char>& bitmap, float u, float v);
	void getSampleLinear(std::vector<unsigned char>& bitmap, float u, float v);
	void getSampleLinearWithAlpha(std::vector<unsigned char>& bitmap, float u, float v);

	void getSampleBicubic(std::vector<unsigned char>& bitmap, float u, float v);
	void getSampleBicubicWithAlpha(std::vector<unsigned char>& bitmap, float u, float v);
	
	std::string jpegData;
	bool jpegDataReady;
	void updateStringBase64JPEG();
public:
	ImageBitmap();
	virtual ~ImageBitmap();

	bool initWithImageData(const unsigned char* data, int len);
	bool initWithImageData(const std::string& base64Data);

	void crop(const cocos2d::Rect& rect, const std::function<void(ImageBitmap*)>& callback = nullptr);
	void resizeTo(const cocos2d::Size& newSize, const std::function<void(ImageBitmap*)>& callback = nullptr);
	int getWidth();
	int getHeight();

	std::string saveToJPEG();

	cocos2d::Texture2D* createTexture();
};


} /* namespace ext */

#endif /* PLUGIN_IMAGEBITMAP_H_ */
