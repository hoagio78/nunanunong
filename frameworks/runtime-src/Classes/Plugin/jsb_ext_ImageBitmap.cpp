#include "jsb_ext_ImageBitmap.hpp"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include "ImageBitmap.h"

JSClass  *jsb_ext_ImageBitmap_class;
JSObject *jsb_ext_ImageBitmap_prototype;
JSObject *jsb_ext_ImageBitmap_ns_object;
JSObject *jsb_ext_ImageBitmap_target;

bool jsb_ext_ImageBitmap_initWithImageData(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_initWithImageData : Invalid Native Object");
	if (argc == 1) {
		std::string arg0;
		ok &= jsval_to_std_string(cx, args[0], &arg0);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_ImageBitmap_initWithImageData : Error processing arguments");
		cobj->initWithImageData(arg0);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_initWithImageData : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_crop(JSContext *cx, uint32_t argc, jsval *vp)
{	
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_crop : Invalid Native Object");
	if (argc == 1) {
		cocos2d::Rect arg0;
		ok &= jsval_to_ccrect(cx, args[0], &arg0);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_ImageBitmap_crop : Error processing arguments");
		cobj->crop(arg0);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 2){
		cocos2d::Rect arg0;
		ok &= jsval_to_ccrect(cx, args[0], &arg0);

		std::function<void(ext::ImageBitmap*)> arg1;
		do {
			if (JS_TypeOfValue(cx, args.get(1)) == JSTYPE_FUNCTION)
			{
				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(1), args.thisv()));
				auto lambda = [=](ext::ImageBitmap* larg0) -> void {
					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
						jsval largv[1];

					if (larg0) {
						largv[0] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<ext::ImageBitmap>(cx, larg0));
					}
					else {
						largv[0] = JSVAL_NULL;
					};

					JS::RootedValue rval(cx);
					bool succeed = func->invoke(1, &largv[0], &rval);
					if (!succeed && JS_IsExceptionPending(cx)) {
						JS_ReportPendingException(cx);
					}
				};
				arg1 = lambda;
			}
			else
			{
				arg1 = nullptr;
			}
		} while (0);
			
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_ImageBitmap_crop : Error processing arguments");
		cobj->crop(arg0, arg1);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_crop : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_resizeTo(JSContext *cx, uint32_t argc, jsval *vp)
{	
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_resizeTo : Invalid Native Object");
	if (argc == 1) {
		cocos2d::Size newSize;
		ok &= jsval_to_ccsize(cx, args[0], &newSize);
		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_ImageBitmap_resizeTo : Error processing arguments");

		cobj->resizeTo(newSize);
		args.rval().setUndefined();
		return true;
	}
	else if (argc == 2){
		cocos2d::Size newSize;
		ok &= jsval_to_ccsize(cx, args[0], &newSize);

		std::function<void(ext::ImageBitmap*)> arg1;
		do {
			if (JS_TypeOfValue(cx, args.get(1)) == JSTYPE_FUNCTION)
			{
				JS::RootedObject jstarget(cx, args.thisv().toObjectOrNull());
				std::shared_ptr<JSFunctionWrapper> func(new JSFunctionWrapper(cx, jstarget, args.get(1), args.thisv()));
				auto lambda = [=](ext::ImageBitmap* larg0) -> void {
					JSB_AUTOCOMPARTMENT_WITH_GLOBAL_OBJCET
						jsval largv[1];

					if (larg0) {
						largv[0] = OBJECT_TO_JSVAL(js_get_or_create_jsobject<ext::ImageBitmap>(cx, larg0));
					}
					else {
						largv[0] = JSVAL_NULL;
					};

					JS::RootedValue rval(cx);
					bool succeed = func->invoke(1, &largv[0], &rval);
					if (!succeed && JS_IsExceptionPending(cx)) {
						JS_ReportPendingException(cx);
					}
				};
				arg1 = lambda;
			}
			else
			{
				arg1 = nullptr;
			}
		} while (0);


		JSB_PRECONDITION2(ok, cx, false, "jsb_ext_ImageBitmap_resizeTo : Error processing arguments");
		cobj->resizeTo(newSize, arg1);
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_resizeTo : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_getWidth(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_getWidth : Invalid Native Object");
	if (argc == 0) {
		int32_t nRet = cobj->getWidth();
		args.rval().setInt32(nRet);
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_getWidth : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_getHeight(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_getHeight : Invalid Native Object");
	if (argc == 0) {
		int32_t nRet = cobj->getHeight();
		args.rval().setInt32(nRet);
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_getHeight : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_saveToJPEG(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_saveToJPEG : Invalid Native Object");
	if (argc == 0) {
		std::string nRet = cobj->saveToJPEG();
		args.rval().set(std_string_to_jsval(cx, nRet));
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_saveToJPEG : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_createTexture(JSContext *cx, uint32_t argc, jsval *vp)
{
	
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_createTexture : Invalid Native Object");
	if (argc == 0) {
		auto nativeObj = cobj->createTexture();
		jsval returnVal;
		if (nativeObj) {
			returnVal = OBJECT_TO_JSVAL(js_get_or_create_jsobject<cocos2d::Texture2D>(cx, nativeObj));
		}
		else {
			returnVal = JSVAL_NULL;
		};

		args.rval().set(returnVal);
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_createTexture : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_retain(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_retain : Invalid Native Object");
	if (argc == 0) {
		cobj->retain();
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_retain : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}

bool jsb_ext_ImageBitmap_release(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	js_proxy_t *proxy = jsb_get_js_proxy(obj);
	ext::ImageBitmap* cobj = (ext::ImageBitmap *)(proxy ? proxy->ptr : NULL);
	JSB_PRECONDITION2(cobj, cx, false, "jsb_ext_ImageBitmap_release : Invalid Native Object");
	if (argc == 0) {
		cobj->release();
		args.rval().setUndefined();
		return true;
	}

	JS_ReportError(cx, "jsb_ext_ImageBitmap_release : wrong number of arguments: %d, was expecting %d", argc, 1);
	return false;
}


bool jsb_ext_ImageBitmap_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	bool ok = true;
	ext::ImageBitmap* cobj = new (std::nothrow) ext::ImageBitmap();

	js_type_class_t *typeClass = js_get_type_from_native<ext::ImageBitmap>(cobj);

	// link the native object with the javascript object
	JS::RootedObject jsobj(cx, jsb_ref_create_jsobject(cx, cobj, typeClass, "ext::ImageBitmap"));
	args.rval().set(OBJECT_TO_JSVAL(jsobj));
	if (JS_HasProperty(cx, jsobj, "_ctor", &ok) && ok)
		ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(jsobj), "_ctor", args);
	return true;
}
static bool jsb_ext_ImageBitmap_ctor(JSContext *cx, uint32_t argc, jsval *vp)
{
	JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
	JS::RootedObject obj(cx, args.thisv().toObjectOrNull());
	ext::ImageBitmap *nobj = new (std::nothrow) ext::ImageBitmap();
	auto newproxy = jsb_new_proxy(nobj, obj);
	jsb_ref_init(cx, &newproxy->obj, nobj, "ext::ImageBitmap");
	bool isFound = false;
	if (JS_HasProperty(cx, obj, "_ctor", &isFound) && isFound)
		ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", args);
	args.rval().setUndefined();
	return true;
}

void js_ext_ImageBitmap_finalize(JSFreeOp *fop, JSObject *obj){

}

void js_register_ext_ImageBitmap(JSContext *cx, JS::HandleObject global) {
	jsb_ext_ImageBitmap_class = (JSClass *)calloc(1, sizeof(JSClass));
	jsb_ext_ImageBitmap_class->name = "ImageBitmap";
	jsb_ext_ImageBitmap_class->addProperty = JS_PropertyStub;
	jsb_ext_ImageBitmap_class->delProperty = JS_DeletePropertyStub;
	jsb_ext_ImageBitmap_class->getProperty = JS_PropertyStub;
    jsb_ext_ImageBitmap_class->setProperty = JS_StrictPropertyStub;
    jsb_ext_ImageBitmap_class->enumerate = JS_EnumerateStub;
    jsb_ext_ImageBitmap_class->resolve = JS_ResolveStub;
    jsb_ext_ImageBitmap_class->convert = JS_ConvertStub;
    jsb_ext_ImageBitmap_class->finalize = js_ext_ImageBitmap_finalize;
    jsb_ext_ImageBitmap_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        JS_PS_END
    };

    static JSFunctionSpec funcs[] = {
		JS_FN("initWithImageData", jsb_ext_ImageBitmap_initWithImageData, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("crop", jsb_ext_ImageBitmap_crop, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("resizeTo", jsb_ext_ImageBitmap_resizeTo, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getWidth", jsb_ext_ImageBitmap_getWidth, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("getHeight", jsb_ext_ImageBitmap_getHeight, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("saveToJPEG", jsb_ext_ImageBitmap_saveToJPEG, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("createTexture", jsb_ext_ImageBitmap_createTexture, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("retain", jsb_ext_ImageBitmap_retain, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("release", jsb_ext_ImageBitmap_release, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
		JS_FN("ctor", jsb_ext_ImageBitmap_ctor, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };
	
    JSFunctionSpec *st_funcs = NULL;
	jsb_ext_ImageBitmap_prototype = JS_InitClass(
		cx, global,
		JS::NullPtr(),
		jsb_ext_ImageBitmap_class,
		jsb_ext_ImageBitmap_constructor, 0, // constructor
		properties,
		funcs,
		NULL, // no static properties
		st_funcs);

	JS::RootedObject proto(cx, jsb_ext_ImageBitmap_prototype);
    JS::RootedValue className(cx, std_string_to_jsval(cx, "ImageBitmap"));
    JS_SetProperty(cx, proto, "_className", className);
    JS_SetProperty(cx, proto, "__nativeObj", JS::TrueHandleValue);
    JS_SetProperty(cx, proto, "__is_ref", JS::TrueHandleValue);
    // add the proto and JSClass to the type->js info hash table
	jsb_register_class<ext::ImageBitmap>(cx, jsb_ext_ImageBitmap_class, proto, JS::NullPtr());
    anonEvaluate(cx, global, "(function () { cc.ImageBitmap.extend = cc.Class.extend; })()");
}

void register_all_ext_ImageBitmap(JSContext* cx, JS::HandleObject obj) {
    // Get the ns
    JS::RootedObject ns(cx);
    get_or_create_js_obj(cx, obj, "cc", &ns);
	jsb_ext_ImageBitmap_ns_object = ns;
	js_register_ext_ImageBitmap(cx, ns);
}

