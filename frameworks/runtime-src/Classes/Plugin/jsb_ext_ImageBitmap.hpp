#include "base/ccConfig.h"
#ifndef __ext_ImageBitmap_h__
#define __ext_ImageBitmap_h__

#include "jsapi.h"
#include "jsfriendapi.h"
#include <string>
#include "ImageBitmap.h"

void js_register_ext_ImageBitmap(JSContext *cx, JS::HandleObject global);
void register_all_ext_ImageBitmap(JSContext* cx, JS::HandleObject obj);

#endif // __ext_ImageBitmap_h__
