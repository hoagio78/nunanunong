/*
 * HttpFileDownloader.cpp
 *
 *  Created on: Mar 23, 2016
 *      Author: ext
 */

#include "HttpFileDownloader.h"
#include "cocos2d.h"
USING_NS_CC;

#include <curl/curl.h>
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#include <direct.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>
#include "../GameLaucher/EngineUtilsThreadSafe.h"

namespace ext{

DownloadRequest::DownloadRequest(){
	_url = "";
	_fileName = "";
	finishedCallback = nullptr;
	processCallback = nullptr;
	fileWrite = 0;
}

DownloadRequest::DownloadRequest(const std::string& url, const std::string& fileName) : DownloadRequest(){
	_url = url;
	_fileName = fileName;
}

DownloadRequest::~DownloadRequest(){
	if (fileWrite) {
		fclose(fileWrite);
		fileWrite = 0;
	}
}

size_t DownloadRequest::writeDataHandler(void *ptr, size_t size, size_t nmemb){
	size_t ret = size * nmemb;
	if (fileWrite){
		ret = fwrite(ptr, size, nmemb, fileWrite);
	}
	if (processCallback){
		processCallback((unsigned char*)ptr, size * nmemb);
	}

	return ret;
}

size_t DownloadRequest::writeHeaderHandler(void *ptr, size_t size, size_t nmemb) {
	size_t ret = size * nmemb;

	return ret;
}

void DownloadRequest::onFinished(int returnCode){
	if (finishedCallback){
		finishedCallback(returnCode);
	}
}

void DownloadRequest::finishedTask(bool success) {
	if (fileWrite) {
		fclose(fileWrite);
		fileWrite = 0;
	}

	if (success) {
		CCLOG("download file OK : %s", _url.c_str());
		this->onFinished(DownloadResult::OK);
	}
	else {
		if (!_fileName.empty()) {
			remove(_fileName.c_str());
		}
		this->onFinished(DownloadResult::NETWORK_ERROR);
	}
}

bool DownloadRequest::initDownloadHandler(void* handler) {
	CURL *curl = (CURL*) handler;
	curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
	curl_easy_setopt(curl, CURLOPT_AUTOREFERER, true);
	curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 10);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 120);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 120);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_easy_setopt(curl, CURLOPT_FRESH_CONNECT, true);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpFileDownloader::writeDataHandler);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, true);
	curl_easy_setopt(curl, CURLOPT_FAILONERROR, true);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);

	if (!_fileName.empty() && !fileWrite) {
		auto n = _fileName.find_last_of("/");
		if (n != std::string::npos && n != 0) {
			std::string parentFolder = _fileName.substr(0, n);
			if (!EngineUtilsThreadSafe::getInstance()->createDirectory(parentFolder)) {
				return false;
			}
		}

		fileWrite = fopen(_fileName.c_str(), "wb");
		if (!fileWrite) {
			CCLOG("download file cannot create file:  %s", _url.c_str());
			this->onFinished(DownloadResult::FILE_ERROR);
			return false;
		}
	}
	return true;
}

/****/

HttpFileDownloader::HttpFileDownloader(){
	
}

HttpFileDownloader::~HttpFileDownloader(){
	std::unique_lock<std::mutex> lk(_request_mutex);
	if (!_request.empty()) {
		auto request = _request.front();
		_request.pop();
		delete request;
	}
	_request_condition_variable.notify_all();
}

size_t HttpFileDownloader::writeDataHandler(void *ptr, size_t size, size_t nmemb, DownloadRequest* writer) {
	size_t written = writer->writeDataHandler(ptr, size, nmemb);
	return written;
}

size_t HttpFileDownloader::writeHeaderHandler(void *ptr, size_t size, size_t nmemb, DownloadRequest* writer) {
	size_t written = writer->writeHeaderHandler(ptr, size, nmemb);
	return written;
}

DownloadRequest* HttpFileDownloader::takeRequest(){
	std::unique_lock<std::mutex> lk(_request_mutex);
	if (!_request.empty()){
		auto request = _request.front();
		_request.pop();
		return request;
	}
	return 0;
}

void HttpFileDownloader::downloadThread() {
	CCLOG("HttpFileDownloader:: create download thread");

	uint32_t maxTask = 6;

	CURLM* curlmHandle = curl_multi_init();
	std::map<CURL*, DownloadRequest*> coTaskMap;
	int runningHandles = 0;
	CURLMcode mcode = CURLM_OK;
	int rc = 0;
	int msgq = 0;

	do {
		if (runningHandles) {
			long timeoutMS = -1;
			curl_multi_timeout(curlmHandle, &timeoutMS);
			if (timeoutMS < 0) {
				timeoutMS = 1000;
			}

			fd_set fdread;
			fd_set fdwrite;
			fd_set fdexcep;
			int maxfd = -1;

			FD_ZERO(&fdread);
			FD_ZERO(&fdwrite);
			FD_ZERO(&fdexcep);

			mcode = curl_multi_fdset(curlmHandle, &fdread, &fdwrite, &fdexcep, &maxfd);
			if (mcode != CURLM_OK) {
				break;
			}

			if (maxfd == -1) {
				if (timeoutMS > 100) {
					timeoutMS = 100;
				}
				std::this_thread::sleep_for(std::chrono::milliseconds(timeoutMS));
				rc = 0;
			}
			else {
				struct timeval timeout;

				timeout.tv_sec = 1;
				timeout.tv_usec = 0;

				if (timeoutMS >= 0) {
					timeout.tv_sec = timeoutMS / 1000;
					if (timeout.tv_sec > 1) {
						timeout.tv_sec = 1;
					}
					else {
						timeout.tv_usec = (timeoutMS % 1000) * 1000;
					}
				}
				rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);
			}

			if (rc < 0) {
				CCLOG("select return unexpect code: %d", rc);
			}
		}

		if (coTaskMap.size()) {
			mcode = CURLM_CALL_MULTI_PERFORM;
			while (mcode == CURLM_CALL_MULTI_PERFORM) {
				mcode = curl_multi_perform(curlmHandle, &runningHandles);
			}
			if (mcode != CURLM_OK) {
				CCLOG("curl_multi_perform error");
				break;
			}

			struct CURLMsg *m;
			do {
				m = curl_multi_info_read(curlmHandle, &msgq);
				if (m && (m->msg == CURLMSG_DONE))
				{
					CURL *curlHandle = m->easy_handle;
					CURLcode errCode = m->data.result;

					// remove from multi-handle
					curl_multi_remove_handle(curlmHandle, curlHandle);
					auto it = coTaskMap.find(curlHandle);
					auto request = it->second;
					
					if (errCode == CURLM_OK) {
						long httpResponseCode = 0;
						rc = curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpResponseCode);
						if (CURLE_OK != rc) {
							CCLOG("add handler error");
						}
						request->finishedTask(httpResponseCode == 200);
					}
					else {
						request->finishedTask(false);
					}
					curl_easy_cleanup(curlHandle);
					
					delete request;
					coTaskMap.erase(it);
				}
			} while (m);
		}

		// add new handler
		while (coTaskMap.size() < maxTask) {
			auto request = this->takeRequest();
			if (!request) {
				break;
			}
			CURL* newHandler = curl_easy_init();
			if (!request->initDownloadHandler((void*)newHandler)) {
				curl_easy_cleanup(newHandler);
				delete request;
				continue;
			}
			coTaskMap.insert(std::make_pair(newHandler, request));
			mcode = curl_multi_add_handle(curlmHandle, newHandler);
			if (mcode != CURLM_OK) {
				CCLOG("add handler error");
			}
		}
	} while (coTaskMap.size());

	for (auto it = coTaskMap.begin(); it != coTaskMap.end(); it++) {
		curl_easy_cleanup(it->first);

		it->second->finishedTask(false);
		delete it->second;
	}
	coTaskMap.clear();

	curl_multi_cleanup(curlmHandle);

	CCLOG("HttpFileDownloader:: end download thread");
}

void HttpFileDownloader::updateThread(){
	std::unique_lock<std::mutex> lk(_request_mutex);
	lk.unlock();

	while (true) {
		lk.lock();
		if (_request.empty()) {
			_request_condition_variable.wait(lk);
			if (_request.empty()) {
				break;
			}
		}
		lk.unlock();
		this->downloadThread();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void HttpFileDownloader::addRequest(DownloadRequest* request){
	std::unique_lock<std::mutex> lk(_request_mutex);
	_request.push(request);
	_request_condition_variable.notify_one();
}

void HttpFileDownloader::init() {
	std::thread workerThread(&HttpFileDownloader::updateThread, this);
	workerThread.detach();
}

static HttpFileDownloader* s_HttpFileDownloader = 0;
HttpFileDownloader* HttpFileDownloader::getInstance(){
	if (!s_HttpFileDownloader){
		s_HttpFileDownloader = new HttpFileDownloader();
		s_HttpFileDownloader->init();
	}
	return s_HttpFileDownloader;
}

}
