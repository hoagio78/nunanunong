#include "base/ccConfig.h"
#ifndef __ext_systemplugin_h__
#define __ext_systemplugin_h__

#include "jsapi.h"
#include "jsfriendapi.h"
#include <string>
#include "ImageBitmap.h"

void js_register_ext_systemplugin(JSContext *cx, JS::HandleObject global);
void register_all_ext_systemplugin(JSContext* cx, JS::HandleObject obj);
void jsb_ext_onTakeImageData(ext::ImageBitmap* image);

#endif // __ext_systemplugin_h__
