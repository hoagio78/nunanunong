/*
 * SystemPlugin.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: ext
 */

#include "SystemPlugin.h"
#include "cocos2d.h"
#include <json/rapidjson.h>
#include <json/document.h>
#include <iostream>
#include <fstream>
#include "jsb_ext_systemplugin.hpp"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "scripting/js-bindings/manual/cocos2d_specifics.hpp"
#include <locale>
#include "crypt_aes.h"
#include "base64.h"
#include "base/ccUTF8.h"
#include "network/HttpClient.h"
#include <curl/curl.h>
#include "../GameLaucher/GameLaucher.h"
#include "ImageBitmap.h"

#ifdef WINRT
#include <codecvt>
#endif

USING_NS_CC;

static const char HEX_CHAR[17] = "0123456789ABCDEF";
inline bool _isxdigit(char c){
	if ('0' <= c && c <= '9'){
		return true;
	}
	if ('a' <= c && c <= 'f'){
		return true;
	}
	if ('A' <= c && c <= 'F'){
		return true;
	}

	return false;
}

char _charxtoint(char c){
	if ('0' <= c && c <= '9'){
		return (c - '0');
	}
	if ('a' <= c && c <= 'f'){
		return (c - 'a' + 10);
	}

	return (c - 'A' + 10);
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "UUIDEncrypt.h"
#include "jni/JniHelper.h"
extern "C"{
	JNIEXPORT void JNICALL Java_org_cocos2dx_lib_system_ImagePicker_nativeOnTakeImageData(JNIEnv*  env, jobject thiz, jstring base64Data){
		std::string _base64Data = JniHelper::jstring2string(base64Data);
		ext::SystemPlugin::getInstance()->onTakeImageData(_base64Data);
	}

	void jniShowImagePicker(int maxWidth, int maxHeight){
		JniMethodInfo method;
		bool bRet = JniHelper::getStaticMethodInfo(method, "org/cocos2dx/lib/system/ImagePicker", "jniShowImagePicker", "(II)V");
		if (bRet){
			method.env->CallStaticVoidMethod(method.classID, method.methodID, maxWidth, maxHeight);
			method.env->DeleteLocalRef(method.classID);
		}
	}

	std::string jniGetVersionName(){
		JniMethodInfo sMethod;
		bool bRet = JniHelper::getStaticMethodInfo(sMethod, "org/cocos2dx/lib/system/SystemPlugin", "jniGetVersionName", "()Ljava/lang/String;");
		if(bRet){
			jstring jstr = (jstring) sMethod.env->CallStaticObjectMethod(sMethod.classID, sMethod.methodID);
			std::string pStr = JniHelper::jstring2string(jstr);
			sMethod.env->DeleteLocalRef(sMethod.classID);
			sMethod.env->DeleteLocalRef(jstr);
			return pStr;
		}
		return "";
	}

	std::string jniSystemPluginGetAndroidPackage(){
		JniMethodInfo sMethod;
		bool bRet = JniHelper::getStaticMethodInfo(sMethod,"org/cocos2dx/lib/system/SystemPlugin","jniGetAndroidPackage","()Ljava/lang/String;");
		if (bRet){
			jstring jstr = (jstring)sMethod.env->CallStaticObjectMethod(sMethod.classID, sMethod.methodID);
			std::string pStr = JniHelper::jstring2string(jstr);
			sMethod.env->DeleteLocalRef(sMethod.classID);
			sMethod.env->DeleteLocalRef(jstr);
			return pStr;
		}
		return "";
	}
}

#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "iOS_native_linker.h"
//void objC_to_c_buyInAppSuccess(int returnCode, const char*  token){
//    CCLOG("ios inapp token %s",token);
//    ext::SystemPlugin::getInstance()->onBuyItemFinished(returnCode, std::string(token));
//}
//
//void objC_to_c_registedNotificationSuccess(const char* uid, const char* token){
//    ext::SystemPlugin::getInstance()->onRegisterNotificationSuccess(uid, token);
//}

void objC_to_c_onTakeImageData(const char* data){
    ext::SystemPlugin::getInstance()->onTakeImageData(std::string(data));
}
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
#include "MacOS_native_linker.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
std::vector<unsigned char> _winrt_getData(::Windows::Storage::Streams::IBuffer^ buf)
{
	auto reader = ::Windows::Storage::Streams::DataReader::FromBuffer(buf);
	std::vector<unsigned char> data(reader->UnconsumedBufferLength);
	if (!data.empty())
		reader->ReadBytes(::Platform::ArrayReference<unsigned char>(&data[0], data.size()));
	return data;
}

std::string _winrt_ByteArrayToHex(const std::vector<unsigned char>& buffer){
	std::string imei = "";

	char stringBuffer[10];
	for (int i = 0; i < buffer.size(); i++){
		sprintf(stringBuffer, "%02X", buffer[i]);

		imei += std::string(stringBuffer);
	}
	return imei;
}

std::string _winrt_getVersionName(){
	//::Windows.ApplicationModel.Package.Current
	::Windows::ApplicationModel::Package^ currentPackage = ::Windows::ApplicationModel::Package::Current;
	::Windows::ApplicationModel::PackageVersion version = currentPackage->Id->Version;

	char stringBuffer[20];
	sprintf(stringBuffer, "%hu.%hu.%hu", version.Major, version.Minor, version.Build);
	return std::string(stringBuffer);
}

inline std::string _wstring_to_string(const std::wstring& wstring){
	auto wideData = wstring.c_str();
	int bufferSize = WideCharToMultiByte(CP_UTF8, 0, wideData, -1, nullptr, 0, NULL, NULL);
	auto utf8 = std::make_unique<char[]>(bufferSize);
	if (0 == WideCharToMultiByte(CP_UTF8, 0, wideData, -1, utf8.get(), bufferSize, NULL, NULL))
		throw std::exception("Can't convert string to UTF8");
	return std::string(utf8.get());
}

std::string _winrt_getPackageName(){
	::Windows::ApplicationModel::PackageId^ packageId = ::Windows::ApplicationModel::Package::Current->Id;
	std::wstring wstr(packageId->Name->Data());
	return _wstring_to_string(wstr);
}

#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include <windows.h>
#include <shobjidl.h> 

inline std::string _wstring_to_string(const std::wstring& wstring){
	auto wideData = wstring.c_str();
	int bufferSize = WideCharToMultiByte(CP_UTF8, 0, wideData, -1, nullptr, 0, NULL, NULL);
	auto utf8 = std::make_unique<char[]>(bufferSize);
	if (0 == WideCharToMultiByte(CP_UTF8, 0, wideData, -1, utf8.get(), bufferSize, NULL, NULL))
		throw std::exception("Can't convert string to UTF8");
	return std::string(utf8.get());
}

void _win32_ImagePickerOpenFile(const std::string& filePath){
	cocos2d::log("filePath: %s", filePath.c_str());
	auto data = cocos2d::FileUtils::getInstance()->getDataFromFile(filePath);
	auto base64Data = base64_encode(data.getBytes(), data.getSize());
	ext::SystemPlugin::getInstance()->onTakeImageData(base64Data);

	//if (!data.isNull()){
	//	auto img = new ext::ImageBitmap();
	//	img->autorelease();
	//	img->initWithImageData(data.getBytes(), data.getSize());
	//	img->crop(cocos2d::Rect(0, 0, img->getWidth() / 2, img->getHeight() / 2)); 
	//	img->saveToJPEG();
	//}
}

void _win32_showImagePicker(){
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		IFileOpenDialog *pFileOpen;

		// Create the FileOpenDialog object.
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));

		if (SUCCEEDED(hr))
		{
			// Show the Open dialog box.
			COMDLG_FILTERSPEC rgSpec[] =
			{
				{ L"Image", L"*.jpg;*.jpeg;*.bmp;*.png" }
			};
			pFileOpen->SetFileTypes(1, rgSpec);
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr))
			{
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr))
				{
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					// Display the file name to the user.
					if (SUCCEEDED(hr))
					{	
						std::wstringstream ss;
						ss << pszFilePath;
						CoTaskMemFree(pszFilePath);

						_win32_ImagePickerOpenFile(_wstring_to_string(ss.str()));			
					}
					pItem->Release();
				}
			}
			pFileOpen->Release();
		}
		CoUninitialize();
	}
}

#endif

bool __stringify_callback(const jschar *buf, uint32_t len, void *data){
	auto buffer = (std::vector<jschar>*)data;
	buffer->insert(buffer->end(), buf, buf + len);
	return true;
}

namespace ext {

static SystemPlugin* s_SystemPlugin = 0;

SystemPlugin* SystemPlugin::getInstance(){
	if(!s_SystemPlugin){
		s_SystemPlugin = new SystemPlugin();
	}
	return s_SystemPlugin;
}

SystemPlugin::SystemPlugin() {
	// TODO Auto-generated constructor stub
}

SystemPlugin::~SystemPlugin() {
	// TODO Auto-generated destructor stub
}

void SystemPlugin::enableMipmapTexture(const std::string& textureName){
	Texture2D* texture = Director::getInstance()->getTextureCache()->getTextureForKey(textureName);
	if (texture){
		texture->generateMipmap();
		texture->setAntiAliasTexParameters();
		texture->setTexParameters({ GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE });
	}
}
    
std::string SystemPlugin::getDeviceUUID(std::string nameKeyChain){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	return UUIDEncrypt::getInstance()->getUUID();

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
   
    const char* uuidMe= c_to_objC_getUUID(nameKeyChain.c_str());
    if(uuidMe==0)
    {
        c_to_objC_setKeyChainUser(nameKeyChain.c_str());
        uuidMe= c_to_objC_getUUID(nameKeyChain.c_str());
    }
	return uuidMe;

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	auto hardwareToken = Windows::System::Profile::HardwareIdentification::GetPackageSpecificToken(nullptr);
	std::vector<unsigned char> buffer = _winrt_getData(hardwareToken->Id);
	return _winrt_ByteArrayToHex(buffer);

//	return "imei";
#else
	return "imei";
#endif
}

std::string SystemPlugin::getVersionName(){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	return jniGetVersionName();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	return c_to_objC_getVersion();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
	return c_to_objC_getVersion();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	return _winrt_getVersionName();
#else
	return "1.0.0";
#endif
}

std::string SystemPlugin::getPackageName(){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	return jniSystemPluginGetAndroidPackage();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	return c_to_objC_getBundle();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
	return c_to_objC_getBundle();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	return _winrt_getPackageName();
#else
	return "club.zik.slot";
#endif
}

/***/

#define AES_BLOCK_SIZE_BIT 128
#define AES_BLOCK_SIZE_BYTE 16

std::vector<char> SystemPlugin::dataEncrypt(const char* key, const char* data, int dataSize){
	std::vector<char> retData;

	//create init vector
	uint8_t ivBuffer[AES_BLOCK_SIZE_BYTE];
	for (int i = 0; i<AES_BLOCK_SIZE_BYTE; i++){
		ivBuffer[i] = (uint8_t)rand();
		retData.push_back((char)ivBuffer[i]);
	}

	//add padding [PKCS#5]
	std::vector<char> dataBuffer(data, data + dataSize);
	int padding = AES_BLOCK_SIZE_BYTE - (dataSize%AES_BLOCK_SIZE_BYTE);
	for (int i = 0; i<padding; i++){
		dataBuffer.push_back(padding);
	}

	//encrypt
	aes_ks_t secretKey;
	aes_setks_encrypt((const uint8_t*)key, AES_BLOCK_SIZE_BIT, &secretKey);
	int outputSize = dataBuffer.size();
	int blockSize = outputSize / AES_BLOCK_SIZE_BYTE;
	uint8_t* outputBuffer = new uint8_t[outputSize];
	aes_cbc_encrypt((const uint8_t*)dataBuffer.data(), outputBuffer, ivBuffer, blockSize, &secretKey);
	retData.insert(retData.end(), outputBuffer, outputBuffer + outputSize);

	delete[] outputBuffer;
	return retData;
}

std::vector<char> SystemPlugin::dataDecrypt(const char* key, const char* data, int dataSize){
	std::vector<char> retData;
	if (dataSize == 0 || dataSize % AES_BLOCK_SIZE_BYTE){
		return retData;
	}

	//read iv
	uint8_t ivBuffer[AES_BLOCK_SIZE_BYTE];
	memcpy(ivBuffer, data, AES_BLOCK_SIZE_BYTE);

	//decrypt
	int encyrptSize = dataSize - AES_BLOCK_SIZE_BYTE;
	int blockSize = encyrptSize / AES_BLOCK_SIZE_BYTE;
	aes_ks_t secretKey;
	aes_setks_decrypt((const uint8_t*)key, AES_BLOCK_SIZE_BIT, &secretKey);
	uint8_t* outputBuffer = new uint8_t[encyrptSize];
	aes_cbc_decrypt((const uint8_t*)(data + AES_BLOCK_SIZE_BYTE), outputBuffer, ivBuffer, blockSize, &secretKey);

	//remove padding [PKCS#5]
	uint8_t lastByte = outputBuffer[encyrptSize - 1];
	if (lastByte > AES_BLOCK_SIZE_BYTE){
		CCLOG("ERROR PADDING");
		return retData;
	}
	encyrptSize -= lastByte;
	
	retData.insert(retData.end(), outputBuffer, outputBuffer + encyrptSize);
	delete[] outputBuffer;
	return retData;
}

std::string SystemPlugin::dataEncryptBase64(const char* key, const std::string& plainText){
	std::vector<char> buffer = this->dataEncrypt(key, plainText.data(), plainText.size());
	return base64_encode((const unsigned char*)buffer.data(), (unsigned int)buffer.size());;
}

std::string SystemPlugin::dataDecryptBase64(const char* key, const std::string& encryptText){
	std::string bytesData = base64_decode(encryptText);
	std::vector<char> buffer = this->dataDecrypt(key, bytesData.data(), bytesData.size());
	return std::string(buffer.data(), buffer.size());
}

std::string SystemPlugin::URLEncode(const std::string& data){
	std::string pRet = "";

	for (int i = 0; i<data.size(); i++){
		if (('0' <= data[i] && data[i] <= '9') ||
			('a' <= data[i] && data[i] <= 'z') ||
			('A' <= data[i] && data[i] <= 'Z') ||
			(data[i] == '-' || data[i] == '_' || data[i] == '.' || data[i] == '~')){
			pRet.append(&data[i], 1);
		}
		else{
			//to hext
			pRet.append("%");
			char dig1 = (data[i] & 0xF0) >> 4;
			char dig2 = (data[i] & 0x0F);
			pRet.append(&HEX_CHAR[dig1], 1);
			pRet.append(&HEX_CHAR[dig2], 1);
		}
	}

	return pRet;
}

std::string SystemPlugin::URLDecode(const std::string& data){
	std::string pRet = "";
	char dig1, dig2;

	for (int i = 0; i<data.size();){
		if (data[i] == '%'){
			dig1 = data[i + 1];
			dig2 = data[i + 2];
			if (_isxdigit(dig1) && _isxdigit(dig2)){
				dig1 = _charxtoint(dig1);
				dig2 = _charxtoint(dig2);
				pRet.append(16 * dig1 + dig2, 1);
			}
		}
		else{
			i++;
		}
	}

	return pRet;
}

void SystemPlugin::addSoftKeyboardDelegate(SoftKeyboardDelegate* delegate){
	_keyboardDelegate.push_back(delegate);
}

void SystemPlugin::removeSoftKeyboardDelegate(SoftKeyboardDelegate* delegate){
	_keyboardDelegate.erase(std::remove(_keyboardDelegate.begin(), _keyboardDelegate.end(), delegate));
}

size_t s_download_save_file_method(void *ptr, size_t size, size_t nmemb, FILE *fp){
	size_t written = fwrite(ptr, size, nmemb, fp);
	return written;
}

void s_download_file_thread(const std::string url, const std::string savePath, std::function<void(int)> finishedCallback){
	CURL *curl;
	curl = curl_easy_init();
	if (curl != NULL) {
		FILE *fp;
		fp = fopen(savePath.c_str(), "wb");
		if (fp != NULL) {
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
			curl_easy_setopt(curl, CURLOPT_AUTOREFERER, true);
			curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 10);
			curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 120);
			curl_easy_setopt(curl, CURLOPT_TIMEOUT, 120);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, true);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, s_download_save_file_method);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
			CURLcode res = curl_easy_perform(curl);
			long responseCode;
			CURLcode code = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &responseCode);
			curl_easy_cleanup(curl);

			fclose(fp);
			if (res == CURLE_OK) {
				if (code == CURLE_OK && responseCode >= 200 && responseCode <= 300){
					CCLOG("download file OK : %s", url.c_str());
					if (finishedCallback){
						Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
							finishedCallback(0);
						});
					}
					return;
				}
				else{
					CCLOG("download file network error[%d]: %s", res, curl_easy_strerror(code));
					if (finishedCallback){
						Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
							finishedCallback(1);
						});
					}
				}
			}
			else{				
				
				CCLOG("download file network error[%d]: %s", res, url.c_str());
				if (finishedCallback){
					Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
						finishedCallback(1);
					});
				}
				return;
			}
		}
		else{
			CCLOG("download file network error: %s [cannot open filePath: %s]", url.c_str(), savePath.c_str());
			if (finishedCallback){
				Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
					finishedCallback(2);
				});
			}
			return;
		}
	}
	CCLOG("download file network error: %s [cannot init curl]", url.c_str());
	if (finishedCallback){
		Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
			finishedCallback(3);
		});
	}
	return;
}

void SystemPlugin::showImagePicker(int maxWidth, int maxHeight){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	jniShowImagePicker(maxWidth, maxHeight);
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	c_to_objC_showImagePicker(maxWidth, maxHeight);
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	_win32_showImagePicker();
#endif
}

void SystemPlugin::onTakeImageData(const std::string& base64Data){
    std::string data = base64Data;
    std::thread newThread([=](){
        auto img = new ext::ImageBitmap();
        
        if (data.size() > 0){
            if (img->initWithImageData(data)){
                cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
                    jsb_ext_onTakeImageData(img);
                    img->autorelease();
                });
                
                return;
            }
        }
        
        cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
            jsb_ext_onTakeImageData(0);
            img->autorelease();
        });
    });
    newThread.detach();
}

Texture2D* SystemPlugin::textureFromBase64(const std::string& base64Data){
	unsigned char* outputBuffer = 0;
	int len = base64Decode((const unsigned char*)base64Data.data(), base64Data.size(), &outputBuffer);
	if (len > 0){
		Image* image = new Image();
		image->autorelease();
		bool ok = image->initWithImageData(outputBuffer, len);
		delete []outputBuffer;
		if (ok){
			Texture2D* tex = new Texture2D();
			tex->autorelease();
			if (tex->initWithImage(image)){
				return tex;
			}
		}
	}
	return NULL;
}

/****/
SoftKeyboardDelegate::SoftKeyboardDelegate(){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	SystemPlugin::getInstance()->addSoftKeyboardDelegate(this);
#endif
}

SoftKeyboardDelegate::~SoftKeyboardDelegate(){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	SystemPlugin::getInstance()->removeSoftKeyboardDelegate(this);
#endif
}

void SoftKeyboardDelegate::onSoftKeyboardShow(float keyboardHeight){

}

void SoftKeyboardDelegate::onSoftKeyboardHide(){

}


} /* namespace ext */
