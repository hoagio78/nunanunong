package org.cocos2dx.lib.system;

import android.app.Activity;
import android.view.View;
import android.view.Window;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

/**
 * Created by ext on 11/27/2017.
 */

public class TextField {
    private KeyboardHeight keyboardHeightSystem;

    private Activity activity;
    private View rootLayout;

    private int keyboardHeight;

    public TextField(Activity activity){
        this.activity = activity;

        keyboardHeight = 0;

        Window windows = activity.getWindow();
        rootLayout = windows.getDecorView().findViewById(android.R.id.content);

        keyboardHeightSystem = new KeyboardHeight(activity);
        keyboardHeightSystem.setOnChangedListener(new KeyboardHeight.OnChangedListener() {
            @Override
            public void onKeyboardHeightChanged(final int height, int orientation) {
                if(keyboardHeight != height){
                    keyboardHeight = height;
                    Cocos2dxGLSurfaceView.getInstance().queueEvent(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            nativeOnKeyboardVisible(height);
                        }
                    });
                }
            }
        });
        rootLayout.post(new Runnable() {
            public void run() {
                keyboardHeightSystem.start();
            }
        });
    }

    private native void nativeOnKeyboardVisible(int height);
}
