package org.cocos2dx.lib.system;

/**
 * Created by ext on 11/27/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.util.Log;
import com.android.billing.util.IabHelper;
import com.android.billing.util.IabResult;
import com.android.billing.util.Inventory;
import com.android.billing.util.Purchase;
import com.android.billing.util.IabHelper.OnConsumeFinishedListener;
import com.android.billing.util.IabHelper.OnConsumeMultiFinishedListener;
import com.android.billing.util.IabHelper.OnIabPurchaseFinishedListener;
import com.android.billing.util.IabHelper.OnIabSetupFinishedListener;
import com.android.billing.util.IabHelper.QueryInventoryFinishedListener;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import java.util.ArrayList;
import java.util.List;

public class AndroidBilling {
    private static AndroidBilling instance = null;
    private Activity mAcvitity = null;
    private GLSurfaceView mGameSurfaceView = null;
    private IabHelper mHelper = null;
    final String TAG = "GoogleINAPP";
    String base64PublicKey;

    private AndroidBilling() {
    }

    public static AndroidBilling getInstance() {
        if(instance == null) {
            instance = new AndroidBilling();
        }

        return instance;
    }

    public void initBilling(Activity acvitity, GLSurfaceView gameSurfaceView, String base64PublicKey) {
        this.mAcvitity = acvitity;
        this.mGameSurfaceView = gameSurfaceView;
        this.base64PublicKey = base64PublicKey;
        this.mHelper = new IabHelper(this.mAcvitity, base64PublicKey);
        this.mHelper.startSetup(new OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
            }
        });
    }

    private void requestConsumeItem(String itemId, final boolean sendEvent) {
        List<String> itemsSKU = new ArrayList();
        itemsSKU.add(itemId);
        this.mHelper.queryInventoryAsync(true, itemsSKU, new QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                List<Purchase> items = inv.getAllPurchases();
                if(sendEvent) {
                    for(int i = 0; i < items.size(); ++i) {
                        Purchase info = (Purchase)items.get(i);
                        AndroidBilling.this.onPurchaseFinished(0, info.getSignature(), info.getOriginalJson());
                    }
                }

                AndroidBilling.this.mHelper.consumeAsync(items, (OnConsumeMultiFinishedListener)null);
            }
        });
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return this.mHelper == null?false:this.mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public void onDestroy() {
        if(this.mHelper != null) {
            this.mHelper.dispose();
            this.mHelper = null;
        }

    }

    public void buyItem(final String itemId, final boolean isConsume) {
        this.mAcvitity.runOnUiThread(new Runnable() {
            public void run() {
            AndroidBilling.this.mHelper.launchPurchaseFlow(AndroidBilling.this.mAcvitity, itemId, 10000, new OnIabPurchaseFinishedListener() {
                public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if(result.isFailure()) {
                    if(result.getResponse() == 7) {
                        Log.i("GoogleINAPP", "BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED");
                        if(isConsume) {
                            AndroidBilling.this.requestConsumeItem(itemId, true);
                        }
                    }

                    AndroidBilling.this.onPurchaseFinished(result.getResponse(), "", "");
                } else if(AndroidBilling.this.mHelper != null) {
                    if(result.getResponse() == 0) {
                        Log.i("GoogleINAPP", "BILLING_RESPONSE_RESULT_OK : " + info.getSignature() + " -- " + info.getOriginalJson());
                        AndroidBilling.this.onPurchaseFinished(0, info.getSignature(), info.getOriginalJson());
                        if(isConsume) {
                            AndroidBilling.this.mHelper.consumeAsync(info, (OnConsumeFinishedListener)null);
                        }
                    } else {
                        Log.i("GoogleINAPP", "ERROR" + result.getResponse());
                        AndroidBilling.this.onPurchaseFinished(result.getResponse(), "", "");
                    }

                }
                }
            });
            }
        });

    }

    private void onPurchaseFinished(final int returnCode, final String purchaseSignature, final String purchaseJson) {
        final String json = purchaseJson.replace("\"", "\\\"").replace("\'", "\\\'");
        this.mGameSurfaceView.queueEvent(new Runnable() {
            public void run() {
                String str = "NativeSystemPluginBrigde.onBuyIAPFinished(" + returnCode + "," +
                        "\"" + purchaseSignature + "\"," +
                        "\"" + json + "\");";
                Cocos2dxJavascriptJavaBridge.evalString(str);
            }
        });
    }

    private static void jniBuyItem(String itemId, boolean isConsume) {
        AndroidBilling.getInstance().buyItem(itemId, isConsume);
    }
}
