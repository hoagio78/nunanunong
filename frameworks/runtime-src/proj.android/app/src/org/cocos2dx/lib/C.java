package org.cocos2dx.lib;

import com.mobile.hutop.R;

/**
 * Created by ext on 11/28/2017.
 */

public class C {
    public static final int facebook_app_id = R.string.facebook_app_id;
    public static final int facebook_app_id_login = R.string.facebook_app_id;

    public static final int ic_launcher = R.mipmap.ic_launcher;

    public static final int default_notification_channel_id = R.string.default_notification_channel_id;
    public static final int default_notification_channel_name = R.string.default_notification_channel_name;

    public static final int ic_stat_ic_notification = R.drawable.ic_stat_ic_notification;

    public static final String IAP_base64PublicKey = "12345";
}
