package org.cocos2dx.lib.system;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import static org.cocos2dx.lib.Cocos2dxActivity.getContext;

public class DeviceId {

    static DeviceId instance = null;
    public static DeviceId getInstance(){
        if(instance == null){
            instance = new DeviceId();
        }
        return instance;
    }

    private Context context = null;
    private DeviceId(){

    }

    public void init(Activity activity){
        this.context = activity.getApplicationContext();
    }

//    private String getAndroidId(){
////        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//    }

    public String getDeviceId(){
        AdvertisingIdClient.Info adInfo = null;
		try {
			adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		if(adInfo != null){
			return adInfo.getId();
		}
		return "";
    }

    public static String jniGetDeviceId(){
        return DeviceId.getInstance().getDeviceId();
    }
}
