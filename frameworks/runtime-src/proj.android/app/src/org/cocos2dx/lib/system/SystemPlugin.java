package org.cocos2dx.lib.system;

import java.util.regex.Pattern;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
//import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;

import org.cocos2dx.lib.C;

public class SystemPlugin {
	static SystemPlugin instance = null;
	private static final String TAG = "SystemPlugin";
	private SystemPlugin(){
		
	}
	
	public static SystemPlugin getInstance(){
		if(instance == null){
			instance = new SystemPlugin();
		}
		return instance;
	}
	
	private Activity activity = null;
	private org.cocos2dx.lib.system.TextField textField = null;
	private UUIDPlugin uuidPlugin;

	public void init(Activity activity){
		this.activity = activity;
		textField = new TextField(activity);
		UUIDPlugin.getInstance().initWithActivity(activity);
		ImagePicker.getInstance().init(activity, Cocos2dxGLSurfaceView.getInstance());
		AndroidBilling.getInstance().initBilling(activity, Cocos2dxGLSurfaceView.getInstance(), C.IAP_base64PublicKey);
		DeviceId.getInstance().init(activity);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data){
		AndroidBilling.getInstance().onActivityResult(requestCode, resultCode, data);
		ImagePicker.getInstance().onActivityResult(requestCode, resultCode, data);
	}

	public void onDestroy() {
		AndroidBilling.getInstance().onDestroy();
	}

	public String getMainAccount(){
		if(activity != null){
			Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
			Account[] accounts = AccountManager.get(activity).getAccounts();
			for (Account account : accounts) {
			    if (emailPattern.matcher(account.name).matches()) {
			        return account.name;
			    }
			}
		}
		
		return "";
	}
	
	public void callSupport(final String numberSup){
		activity.runOnUiThread(new Runnable() {	
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("tel:" + numberSup));
				activity.startActivity(intent);
			}
		});
	
	}
	
	public String getVersionName(){		
		try {
			PackageManager manager = activity.getApplicationContext().getPackageManager();
			PackageInfo info = manager.getPackageInfo(activity.getApplicationContext().getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
		
	public boolean checkNetworkConnection(){
		if(activity != null){
			ConnectivityManager conMgr = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
			if(netInfo != null){
				if(netInfo.isConnected()){
					return true;
				}
			}
		}
		return false;
	}
	public String getAndroidPackage(){
		if(activity != null){
			return activity.getApplicationContext().getPackageName();
		}
		return "";
	}

	public boolean showSMS(final String phoneNumber, final String content){
		if(activity != null){
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNumber));
			intent.putExtra("sms_body", content);
			PackageManager packageManager = activity.getPackageManager();
			if(intent.resolveActivity(packageManager) != null){
				final Intent sendIntent = intent;
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						activity.startActivity(sendIntent);
					}
				});

				return true;
			}
		}
		return false;
	}
	
	/**/
	/** fix getAccoutn android-23 **/
	/**/
	private static final int LOGIN_PERMISSION_REQUEST = 101;
	public boolean checkLoginPermission(boolean isRequest){
		if(activity != null){
			String[] permission = null;
			if(android.os.Build.VERSION.SDK_INT < 16){
				permission = new String[]{android.Manifest.permission.GET_ACCOUNTS,
						android.Manifest.permission.WRITE_EXTERNAL_STORAGE };
			}
			else{
				permission = new String[]{android.Manifest.permission.GET_ACCOUNTS,
						android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
						android.Manifest.permission.READ_EXTERNAL_STORAGE};
			}
			
			if(!checkPermission(permission)){
				if(isRequest){
					this.requestPermission(permission, LOGIN_PERMISSION_REQUEST);
				}				
				return false;
			}
			return true;
		}
		return false;
	}

	public boolean checkPermission(String permission){
		if(activity != null){
			int flag = ContextCompat.checkSelfPermission(activity , permission);
	        return (flag == PackageManager.PERMISSION_GRANTED);
		}
		return false;
	}	
	public boolean checkPermission(String[] permission){
		if(activity != null){
			for(int i=0;i<permission.length;i++){
				if(!checkPermission(permission[i])){
					return false;
				}
			}
		}
		return true;
	}	
	public void requestPermission(String permission, int requestCode){
		requestPermission(new String[]{permission}, requestCode);
	}	
	public void requestPermission(String[] permission, int requestCode){
		if(activity != null){
			ActivityCompat.requestPermissions(activity, permission, requestCode);
		}
	}

	public void requestPermissionThreadSafe(final String[] permission, final int requestCode){
		if(activity != null){
			activity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					for(int i =0;i<permission.length;i++){
						Log.d(TAG, "permission: "+permission[i]);
					}
					ActivityCompat.requestPermissions(activity, permission, requestCode);
				}
			});		
		}
	}

	public String getAdvertisingIdClient() {
//		AdvertisingIdClient.Info adInfo = null;
//		try {
//			adInfo = AdvertisingIdClient.getAdvertisingIdInfo(activity.getApplicationContext());
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "";
//		}
//		if(adInfo != null){
//			return adInfo.getId();
//		}
		return "";
	}

	public boolean textToClipboard(final String textcopy){
		if(activity != null){
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try{
						ClipboardManager clipboard = (ClipboardManager)activity.getSystemService(Context.CLIPBOARD_SERVICE);
						ClipData clip =  ClipData.newPlainText("label",textcopy);
						clipboard.setPrimaryClip(clip);
					}
					catch (final Exception e){
						e.printStackTrace();
					}
				}
			});
			return true;
		}
		return false;
	}
	/****/
	private static String jniGetMainAccount(){
		return SystemPlugin.getInstance().getMainAccount();
	}
	
	private static String jniGetVersionName(){
		return SystemPlugin.getInstance().getVersionName();
	}
	
	private static boolean jniCheckNetworkConnection(){
		return SystemPlugin.getInstance().checkNetworkConnection();
	}

	private static String jniGetAndroidPackage(){
		return SystemPlugin.getInstance().getAndroidPackage();
	}
	private static void jniPhoneSupport(String phoneNumber){
		SystemPlugin.getInstance().callSupport(phoneNumber);
	}
	private static boolean jniCheckPermission(String permission){
		return SystemPlugin.getInstance().checkPermission(permission);
	}
	private static void jniRequestPermission(String[] permission, int requestCode){	
		String[] per = new String[]{android.Manifest.permission.SEND_SMS};
		
		SystemPlugin.getInstance().requestPermissionThreadSafe(per , requestCode);
	}

	private static boolean jniShowSMS(String smsNumber, String smsContent){
		return SystemPlugin.getInstance().showSMS(smsNumber, smsContent);
	}

	private static String jniGetAdvertisingIdClient(){
		return SystemPlugin.getInstance().getAdvertisingIdClient();
	}

	private static boolean jniTextToClipboard(String textcopy){
		return SystemPlugin.getInstance().textToClipboard(textcopy);
	}

}
