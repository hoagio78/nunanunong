package org.cocos2dx.lib.system;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by ext on 8/28/2017.
 */

public class ImagePicker {
    private ImagePicker(){

    }
    static ImagePicker instance = null;
    public static ImagePicker getInstance(){
        if(instance == null){
            instance = new ImagePicker();
        }
        return instance;
    }

    private Activity activity = null;
    private GLSurfaceView gameView = null;
    private int maxWidth = 0;
    private int maxHeight = 0;
    public void init(Activity activity, GLSurfaceView gameView){
        this.activity = activity;
        this.gameView = gameView;
    }

    public void showImagePicker(final int maxWidth, final int maxHeight){
        if(this.activity != null){
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;

            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    activity.startActivityForResult(intent , IMAGE_PICKER_REQUEST_CODE);
                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == IMAGE_PICKER_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                if (data != null) {
                    Bitmap img = this.getBitmap(data.getData(), this.maxWidth, this.maxHeight);
                    this.onOpenBitmap(img);
                }
            }
        }
    }

    private Bitmap getBitmap(Uri photoPath, int reqWidth, int reqHeight) {
        try {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            InputStream inputStream = activity.getContentResolver().openInputStream(photoPath);
            BitmapFactory.decodeStream(inputStream, null, bmOptions);
            inputStream.close();

            int width = bmOptions.outWidth;
            int height = bmOptions.outHeight;

            int inSampleSize = 1;
            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = inSampleSize;

            inputStream = activity.getContentResolver().openInputStream(photoPath);
            Bitmap b = BitmapFactory.decodeStream(inputStream, null, bmOptions);
            inputStream.close();

            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void onOpenBitmap(Bitmap img){
        if(img == null){
            nativeOnTakeImageData("");
            return;
        }

        if(img.getWidth() > this.maxWidth || img.getHeight() > this.maxHeight){
            float ratioX = (float)this.maxWidth / (float) img.getWidth();
            float ratioY = (float)this.maxHeight / (float) img.getHeight();
            float ratio = Math.min(ratioX, ratioY);


            int newWidth = (int)(ratio * (float)img.getWidth());
            int newHeight = (int)(ratio * (float)img.getHeight());
            Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

            float middleX = newWidth / 2.0f;
            float middleY = newHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratio, ratio, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(img, middleX - img.getWidth() / 2, middleY - img.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            img = scaledBitmap;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        String base64Data = android.util.Base64.encodeToString(baos.toByteArray(), Base64.NO_WRAP);
        nativeOnTakeImageData(base64Data);
    }

    /**/
    public static void jniShowImagePicker(int maxWidth, int maxHeight){
        ImagePicker.getInstance().showImagePicker(maxWidth, maxHeight);
    }

    public static native void nativeOnTakeImageData(String base64Data);

    private static final int IMAGE_PICKER_REQUEST_CODE = 10001;
}
