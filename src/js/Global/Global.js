/**
 * Created by ext on 7/6/2016.
 */

String.prototype.insertAt = function (index, string) {
    return this.substr(0, index) + string + this.substr(index);
};

var cc = cc || {};
cc.Global = cc.Global || {};
var _map_swap_key_value = function (_map, func) {
    var newMap = {};
    for (var _key in _map) {
        if (!_map.hasOwnProperty(_key)) continue;
        var newKey = _map[_key];
        if (func) {
            newMap[_map[_key]] = func(_key);
        } else {
            newMap[_map[_key]] = _key;
        }
    }
    return newMap;
};

cc.Global.NumberFormat1 = function (number) {
    var newNum = Math.floor(number);
    var pret = Math.abs(newNum).toString();
    if (pret.length > 3) {
        for (var i = pret.length - 3; i > 0; i -= 3) {
            pret = pret.insertAt(i, ".");
        }
    }
    if (newNum < 0) {
        return "-" + pret;
    }
    return pret;
};

cc.Global.Factorial = function (num) {
    if (num <= 0) {
        return 1;
    } else {
        return num * cc.Global.Factorial(num - 1);
    }
};

cc.Global.Combination = function (k, n) {
    return parseInt(cc.Global.Factorial(n) / (cc.Global.Factorial(k) * cc.Global.Factorial(n - k)));
};

cc.Global.convertTimestampToString = function (timestamp) {
    var days = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
    return days[timestamp.getDay()] + ", ngày " + timestamp.getDate() + ", tháng " + (timestamp.getMonth() + 1) + ", năm " + timestamp.getFullYear()
        + " " + cc.Global.checkNumberLower10(timestamp.getHours())
        + ":" + cc.Global.checkNumberLower10(timestamp.getMinutes())
        + ":" + cc.Global.checkNumberLower10(timestamp.getSeconds());
};

cc.Global.convertTimestampToString2 = function (timestamp) {
    var days = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
    return days[timestamp.getDay()] + " " + cc.Global.checkNumberLower10(timestamp.getDate()) + "/" + cc.Global.checkNumberLower10(timestamp.getMonth() + 1) + "/" + timestamp.getFullYear()
        + " " + cc.Global.checkNumberLower10(timestamp.getHours())
        + ":" + cc.Global.checkNumberLower10(timestamp.getMinutes())
        + ":" + cc.Global.checkNumberLower10(timestamp.getSeconds());
};

cc.Global.convertDateToString = function (date) {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
};
cc.Global.checkNumberLower10 = function (number) {
    return parseInt(number) < 10 ? "0" + number : number
}

cc.Global.convertRemainTimeToString = function (timestamp) {
    var hours = parseInt(timestamp / 3600000);
    var minutes = parseInt((timestamp - hours * 3600000) / 60000);
    var seconds = parseInt((timestamp - hours * 3600000 - minutes * 60000) / 1000);
    return cc.Global.checkNumberLower10(hours) + ":" + cc.Global.checkNumberLower10(minutes) + ":" + cc.Global.checkNumberLower10(seconds);
};

var Number_Format_Type = ["", "K", "M", "B"];
cc.Global.NumberFormat2 = function (number) {
    var orginVal = number;
    var prefix = (number % 1000) / 1000;


    var i = 0;
    while (number >= 1000) {
        number = Math.floor(number / 1000);
        i++;
    }

    if (orginVal > 1000)
        return ((number + prefix).toString() + Number_Format_Type[i]);
    else
        return (number.toString() + Number_Format_Type[i]);
};

cc.Global.NumberFormat3 = function (number) {
    var orginVal = number;


    var i = 0;
    do {
        if (number >= 10000 || number == 1000) {
            number = parseInt(number / 1000);
            i++;
        } else if (number > 1000) {
            number = (number / 1000).toFixed(1);
            i++;
        } else {
            number = parseInt(number);
        }
    } while (number >= 1000);
    // if(orginVal > 0)
    // console.error("NumberFormat3", (number).toString(), number.toString(), Number_Format_Type[i]);

    return (number.toString() + Number_Format_Type[i]);
};

cc.Global.NumberFromString = function (str) {
    var numberText = str.replace(/[.,]/g, '');
    if (numberText && cc.Global.IsNumber(numberText)) {
        return parseInt(numberText);
    }
    return null;
};

cc.Global.NumberFormatWithPadding = function (number, size) {
    if (size == undefined) {
        size = 2;
    }
    if (number < 0) {
        return number.toString();
    }
    var str = number.toString();
    while (str.length < size) {
        str = "0" + str;
    }
    return str;
};

cc.Global.DateToString = function (d, space) {
    if (space === undefined) {
        space = "\n";
    }
    var timeString = cc.Global.NumberFormatWithPadding(d.getDate()) + "/" +
        cc.Global.NumberFormatWithPadding(d.getMonth() + 1) + "/" +
        (1900 + d.getYear()).toString() + space +
        cc.Global.NumberFormatWithPadding(d.getHours()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getMinutes()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getSeconds());
    return timeString;
};

cc.Global.DayToString = function (d) {
    var timeString = cc.Global.NumberFormatWithPadding(d.getDate()) + "/" +
        cc.Global.NumberFormatWithPadding(d.getMonth() + 1) + "/" +
        (1900 + d.getYear()).toString();
    return timeString;
};


cc.Global.TimeToString = function (d) {
    var timeString = cc.Global.NumberFormatWithPadding(d.getHours()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getMinutes()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getSeconds());
    return timeString;
};



cc.Global.getWeek = function () {
    var now = new Date();
    var onejan = new Date(now.getFullYear(), 0, 1);
    return Math.ceil((((now - onejan) / 86400000) + onejan.getDay() + 1) / 7);
};

cc.Global.GetSetting = function (setting, defaultValue) {
    var value = cc.sys.localStorage.getItem(setting);
    if (value) {
        try {
            return JSON.parse(value);
        } catch (e) {
            return value;
        }
    //    return value;
    }
    return defaultValue;
};
cc.Global.SetSetting = function (setting, value) {
    cc.sys.localStorage.setItem(setting, value);
};

cc.Global.NodeIsVisible = function (node) {
    while (node) {
        if (!node.visible) {
            return false;
        }
        node = node.getParent();
    }
    return true;
};

cc.Global.getSaveUsername = function () {
    return cc.Global.GetSetting("username", "");
};

cc.Global.setSaveUsername = function (userName) {
    cc.Global.SetSetting("username", userName);
};

cc.Global.getSavePassword = function () {
    return cc.Global.GetSetting("password", "");
};

cc.Global.setSavePassword = function (passwords) {
    cc.Global.SetSetting("password", passwords);
};

cc.Global.IsNumber = function (str) {
    if (typeof str === 'number') {
        return true;
    }
    var numberText = str.replace(/[.,]/g, '');
    var re = new RegExp("^[0-9]+$");
    return re.test(numberText);
};

cc.Global.ParseInt = function (str) {
    if (cc.isString(str)) {
        if (!cc.Global.IsNumber(str)) {
            return 0;
        }
        var numberText = str.replace(/[.,]/g, '');
        return parseInt(numberText);
    }
    return 0;
};

if (cc.sys.isNative) {
    ccui.Slider.prototype._ctor = function (barTextureName, normalBallTextureName, resType) {
        this.init();
        if (barTextureName) {
            this.loadBarTexture(barTextureName, resType);
        }
        if (normalBallTextureName) {
            this.loadSlidBallTextureNormal(normalBallTextureName, resType);
        }
    };
}

cc.Global.openURL = function (url) {
    if (cc.sys.isNative) {
        cc.Application.getInstance().openURL(url);
    } else {
        var win = window.open(url, '_blank');
        win.focus();
    }
};

cc.Global.setPageForToogleGroup = function (currentIndex, toggleGroup, numPage) {
    var numlast = numPage - 1;
    if (currentIndex % numPage === numlast) {
        var fake = currentIndex + 1;

        for (var i = 0; i < toggleGroup.mItem.length; i++) {
            var item = toggleGroup.mItem[i];
            var num = fake - (numlast - i);
            item.lb_page.setString(num);
        }
    }

    if (currentIndex % numPage === 0) {
        var fake = currentIndex + 1
        for (var i = 0; i < toggleGroup.mItem.length; i++) {
            var item = toggleGroup.mItem[i];
            item.lb_page.setString(fake + i);
        }
    }
}

if (cc.sys.isNative) {
    ModuleManager.getInstance().getReadyModule = function () {
        return [
            ModuleManager.getInstance().getModule("GameBaiAll")
        ];
    };
}


////GAME chAN
var StartGameData = StartGameData || {};
StartGameData.canU = false;
StartGameData.cardStilt = -1;
StartGameData.uidChonCai = -1;
StartGameData.turnID = -1;
StartGameData.listCards = [];

cc.Global.sortChanUEndGame = function (cards) {
    //cards da sap xep theo chan ca va cay an luon roi
    var earnUList = [];
    var caList = [];
    var chanList = [];
    var sortedList = [];

    var endIdx = cards.length - 1;
    var i = cards.length - 1;
    if (cards[i] == cards[i - 1] && cards[i] == cards[i - 2] && cards[i] == cards[i - 3]) {
        earnUList.push(cards[i - 3]);
        earnUList.push(cards[i - 2]);
        earnUList.push(cards[i - 1]);
        earnUList.push(cards[i]);
        endIdx -= 4;
    } else {
        //2 cay bai u`
        earnUList.push(cards[i - 1]);
        earnUList.push(cards[i]);
        endIdx -= 2;
    }
    for (var j = 0; j <= endIdx; j += 2) {
        if (cards[j] == cards[j + 1]) {
            chanList.push(cards[j]);
            chanList.push(cards[j + 1]);
        } else {
            caList.push(cards[j]);
            caList.push(cards[j + 1]);
        }
    }

    sortedList.push(chanList);
    sortedList.push(caList);
    sortedList.push(earnUList);
    //cc.log("SORT DNE "+sortedList.toString());
    return sortedList;

};

cc.Global.sortChan2 = function (cards) {
    //cc.log("SORT BE4 "+cards.toString());
    var sortedList = [];
    var singleIndices = [];
    var chanList = [];
    cc.Global.classfifyChanAndSingleIndices(cards, chanList, singleIndices);
    singleIndices = cc.Global.sortByCaAndBaDau(singleIndices);
    var caList = singleIndices[0];
    var singleList = singleIndices[1];
    sortedList.push(chanList);
    sortedList.push(caList);
    sortedList.push(singleList);
    //cc.log("SORT DNE "+sortedList.toString());
    return sortedList;

};
cc.Global.sortChan = function (cards) {
    //cc.log("SORT BE4 "+cards.toString());
    var sortedList = [];
    var singleIndices = [];
    var chanList = [];
    cc.Global.classfifyChanAndSingleIndices(cards, chanList, singleIndices);
    singleIndices = cc.Global.sortByCa(singleIndices);
    sortedList = sortedList.concat(chanList);
    sortedList = sortedList.concat(singleIndices);
    //cc.log("SORT DNE "+sortedList.toString());
    return sortedList;

};

cc.Global.pushSort = function (list, idCard) {
    if (idCard == -1)
        list.unshift(idCard);
    else
        list.push(idCard);
};
cc.Global.classfifyChanAndSingleIndices = function (cards, chanList, singleIndices) {
    //array card tu -1 -> 23
    //var cards =[1,2,3,4,6,7,1,1,4,2,3];
    // function groups => sum so lan xuat hien cua element
    var groups =
        cards.reduce(function (acc, e) {
            if (e === -1) e = 24;// -1 la chichi => 24 cho de~ tinh, se convert lai. sau
            acc[e] = (e in acc ? acc[e] + 1 : 1);
            return acc;
        }, {});
    cc.log("groups " + groups);
    for (var i = 0; i <= 24; i++) {
        var a = groups[i];
        if (a !== undefined) {
            var idCard = (i == 24 ? -1 : i);
            if (a === 4) {
                //add 2 doi chan = thien khai
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(chanList, idCard);
            } else if (a == 2) {
                //add 1 doi chan
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(chanList, idCard);
            } else if (a === 3) {
                //add 1 doi chan va dua 1 card vao` list singleIndices
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(chanList, idCard);
                cc.Global.pushSort(singleIndices, idCard);
            } else if (a === 1) {
                cc.Global.pushSort(singleIndices, idCard);
            } else {
                alert("Global: Bộ bài chia bị sai " + cards.toString());
            }
        }

    }
    //cc.log("SORT chanList  = "+chanList.toString());
    //cc.log("SORT singleIndices  = "+singleIndices.toString());


};
cc.Global.sortByCa = function (cards) {
    //cards la arr cac quan bai` le? id  -1 -> 23
    // 0 -1 -2 la 2 van,2 anv, 2 sach
    //rieng -1 thi la chichi
    //var num = idCard == -1 ? 0 : (Math.floor(idCard/3) +2 );
    //var cards =[1,2,3,4,6,7,1,1,4,2,3];
    // function groups => sum so lan xuat hien cua element
    var sortedList = [];
    var caList = [];
    var badauList = [];
    var singleIndices = [];
    var groups = cards.reduce(function (acc, e) {
        if (e === -1) e = 24;// -1 la chichi => 24 cho de~ tinh, se convert lai. sau
        var point = Math.floor(e / 3) + 2;
        acc[point] = (point in acc ? acc[point] + 1 : 1);
        return acc;
    }, {});
    for (var i = 2; i <= 10; i++) {// 2 = van. -> 9 = cuu? va` 10 la chichi
        var a = groups[i];
        if (a !== undefined) {
            //cc.log(i +"  = "+a);
            if (a === 1) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa singleIndices  = "+arr1.toString());
                //singleIndices = singleIndices.concat(arr1);
                if (i == 10) {
                    //chichi = -1
                    var arr1 = cc.Global.getAllCardByPoint(cards, -1);
                    singleIndices = arr1.concat(singleIndices);
                } else {
                    singleIndices = singleIndices.concat(arr1);
                }
            } else if (a === 2) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa caList  = "+arr1.toString());
                caList = caList.concat(arr1);
            } else if (a === 3) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa badauList  = "+arr1.toString());
                badauList = badauList.concat(arr1);
            }
        }
    }

    sortedList = sortedList.concat(caList);
    sortedList = sortedList.concat(badauList);
    sortedList = sortedList.concat(singleIndices);
    return sortedList;


};
cc.Global.sortByCaAndBaDau = function (cards) {
    //cards la arr cac quan bai` le? id  -1 -> 23
    // 0 -1 -2 la 2 van,2 anv, 2 sach
    //rieng -1 thi la chichi
    //var num = idCard == -1 ? 0 : (Math.floor(idCard/3) +2 );
    //var cards =[1,2,3,4,6,7,1,1,4,2,3];
    // function groups => sum so lan xuat hien cua element
    var sortedList = [];
    var caList = [];
    var badauList = [];
    var singleIndices = [];
    var groups = cards.reduce(function (acc, e) {
        if (e === -1) e = 24;// -1 la chichi => 24 cho de~ tinh, se convert lai. sau
        var point = Math.floor(e / 3) + 2;
        acc[point] = (point in acc ? acc[point] + 1 : 1);
        return acc;
    }, {});
    for (var i = 2; i <= 10; i++) {// 2 = van. -> 9 = cuu? va` 10 la chichi
        var a = groups[i];
        if (a !== undefined) {
            //cc.log(i +"  = "+a);
            if (a === 1) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa singleIndices  = "+arr1.toString());
                //singleIndices = singleIndices.concat(arr1);
                if (i == 10) {
                    //chichi = -1
                    var arr1 = cc.Global.getAllCardByPoint(cards, -1);
                    singleIndices = arr1.concat(singleIndices);
                } else {
                    singleIndices = singleIndices.concat(arr1);
                }
            } else if (a === 2) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa caList  = "+arr1.toString());
                caList = caList.concat(arr1);
            } else if (a === 3) {
                var arr1 = cc.Global.getAllCardByPoint(cards, i);
                //cc.log(a +" SORT sortByCa badauList  = "+arr1.toString());
                badauList = badauList.concat(arr1);
            }
        }
    }
    caList = caList.concat(badauList);
    sortedList.push(caList);
    sortedList.push(singleIndices);
    return sortedList;


};
cc.Global.getAllCardByPoint = function (cards, point) {
    var arr = [];
    for (var i = 0; i < cards.length; i++) {
        var p = Math.floor(cards[i] / 3) + 2;
        if (point == -1) {
            //chi chi
            p = cards[i];
        }
        if (point === p) {
            arr.push(cards[i]);
        }

    }
    return arr;
};


cc.Global.DateToString1 = function (d) {
    var timeString = cc.Global.NumberFormatWithPadding(d.getHours()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getMinutes()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getSeconds()) + " - " +
        cc.Global.NumberFormatWithPadding(d.getDate()) + "/" +
        cc.Global.NumberFormatWithPadding(d.getMonth() + 1) + "/" +
        (1900 + d.getYear()).toString();
    return timeString;
};

cc.Global.DateToString2 = function (d) {
    var timeString = cc.Global.NumberFormatWithPadding(d.getHours()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getMinutes()) + ":" +
        cc.Global.NumberFormatWithPadding(d.getSeconds()) + "\n" +
        cc.Global.NumberFormatWithPadding(d.getDate()) + "/" +
        cc.Global.NumberFormatWithPadding(d.getMonth() + 1) + "/" +
        (1900 + d.getYear()).toString();
    return timeString;
};
