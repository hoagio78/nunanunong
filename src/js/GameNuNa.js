var HelloWorldScene = cc.Scene.extend({
    ctor: function () {
        this._super();
        cc.winSize.screenScale = cc.winSize.width / 1280.0;
        var size = cc.winSize;

        var bg = new cc.Sprite("#bg.png");
        bg.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        bg.setScale(cc.winSize.screenScale, cc.winSize.height / 720.0);
        this.addChild(bg, 0);
        /////////////////////////////
        // 3. add your codes below...
        // add a label shows "Hello World"
        // create and initialize a label
        //
        // cc.log("======" +      cc.winSize.screenScale);
        var nodeBottom = new cc.Node();
        nodeBottom.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);

        nodeBottom.setScale(cc.winSize.screenScale);
        nodeBottom.setContentSize(cc.size(1280, 720));
        nodeBottom.setAnchorPoint(0.5, 0.5);
        this.addChild(nodeBottom);


        var btnPlay = new ccui.Button("btn_start.png", "", "", ccui.Widget.PLIST_TEXTURE);
        btnPlay.setPosition(nodeBottom.width / 2, 174);
        btnPlay.setZoomScale(0.01);
        btnPlay.addClickEventListener(function () {
            cc.director.runScene(new ListPlayerScene());
        });
        nodeBottom.addChild(btnPlay);

        var title_game = new cc.Sprite("#title_game.png");
        title_game.attr({
            x: 660,
            y: 446
        });
        nodeBottom.addChild(title_game, 0);
    },
    onEnter: function () {

        this._super();

    }
});


var ListPlayerScene = cc.Scene.extend({
    onEnter: function () {

        this._super();
        cc.winSize.screenScale = cc.winSize.width / 1280.0;
        var size = cc.winSize;

        var bg = new cc.Sprite("#bg_game.png");
        bg.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        bg.setScale(cc.winSize.screenScale, cc.winSize.height / 720.0);
        this.addChild(bg, 0);
        /////////////////////////////
        // 3. add your codes below...
        // add a label shows "Hello World"
        // create and initialize a label
        //
        // cc.log("======" +      cc.winSize.screenScale);
        var nodeBottom = new cc.Node();
        nodeBottom.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        nodeBottom.setScale(cc.winSize.screenScale);
        nodeBottom.setContentSize(cc.size(1280, 720));
        nodeBottom.setAnchorPoint(0.5, 0.5);
        this.nodeBottom = nodeBottom;
        this.addChild(nodeBottom);

        var title_game = new cc.Sprite("#lobby_title.png");
        title_game.attr({
            x: 640,
            y: 630
        });
        nodeBottom.addChild(title_game, 0);
        this.arrUsers = [];
        for (var i = 0; i < 5; i++) {
            var bgtext = new cc.Sprite("#lobby_bg_text.png");
            bgtext.setPosition(cc.p(439 + (i % 2) * 339, 513 - Math.floor(i / 2) * 100));
            nodeBottom.addChild(bgtext)

            // var lbl = new cc.LabelTTF( "Người chơi " + (i+1).toString(), "",30);
            // lbl.setPosition(bgtext.getPosition());
            // lbl.setColor(cc.color(255,222,0,255));
            // nodeBottom.addChild(lbl);

            var lbl2 = new cc.LabelTTF((i + 1).toString() + ".", "", 30);
            lbl2.setPosition(cc.p(bgtext.x - 164, bgtext.y));
            lbl2.setColor(cc.color(255, 0, 0, 255));
            nodeBottom.addChild(lbl2);


            var userText = new ccui.TextField();
            userText.setContentSize(cc.size(bgtext.width - 10, bgtext.height));
            userText.setPosition(bgtext.getPosition());
            userText.setFontName("Arial");
            userText.setMaxLength(10);
            userText.setFontSize(30);
            userText.setPlaceHolder("Nhập tên người chơi");
            userText.setPlaceHolderColor(cc.color("#ffffff"));
            nodeBottom.addChild(userText, 1);
            this.arrUsers.push(userText);
        }

        var btnPlay = new ccui.Button("btn_start.png", "", "", ccui.Widget.PLIST_TEXTURE);
        btnPlay.setPosition(640, 100);
        btnPlay.setZoomScale(0.01);
        var thiz = this;
        btnPlay.addClickEventListener(function () {
            var liss = [];
            for (var i = 0; i < thiz.arrUsers.length; i++) {
                var userText = thiz.arrUsers[i];

                if (userText.getString().length !== 0) {
                    liss.push(userText.getString());
                }
            }
            if (liss.length > 1) {

                cc.director.runScene(new GamePlayScene(thiz.shuffle(liss)));
            }


        });
        nodeBottom.addChild(btnPlay);
    },

    shuffle: function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

});


var GamePlayScene = cc.Scene.extend({
    ctor: function (listPlay) {
        this._super();
        this.nameList = listPlay;
        this._audioSource = new SoundPlayer.AudioSource();
        cc.winSize.screenScale = cc.winSize.width / 1280.0;
        var size = cc.winSize;

        var bg = new cc.Sprite("#bg_game.png");
        bg.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        bg.setScale(cc.winSize.screenScale, cc.winSize.height / 720.0);
        this.addChild(bg, 0);
        /////////////////////////////
        // 3. add your codes below...
        // add a label shows "Hello World"
        // create and initialize a label
        //
        // cc.log("======" +      cc.winSize.screenScale);
        var nodeBottom = new cc.Node();
        nodeBottom.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);

        nodeBottom.setScale(cc.winSize.screenScale);
        nodeBottom.setContentSize(cc.size(1280, 720));
        nodeBottom.setAnchorPoint(0.5, 0.5);
        this.nodeBottom = nodeBottom;
        this.addChild(nodeBottom);


        var nodeChan = new cc.Node();
        nodeChan.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);

        nodeChan.setScale(cc.winSize.screenScale);
        nodeChan.setContentSize(cc.size(1280, 720));
        nodeChan.setAnchorPoint(0.5, 0.5);
        this.nodeChan = nodeChan;
        this.addChild(nodeChan);


        var connt = new cc.Sprite("#connt.png");
        connt.setPosition(640, 534);

        nodeChan.addChild(connt);


        var btnBack = new ccui.Button("btn_back.png", "", "", ccui.Widget.PLIST_TEXTURE);
        btnBack.setPosition(83, 654);

        btnBack.setZoomScale(0.01);

        nodeBottom.addChild(btnBack);

        btnBack.addClickEventListener(function () {
            cc.director.runScene(new ListPlayerScene());
        });

        var btnreset = new ccui.Button("btn_reset.png", "", "", ccui.Widget.PLIST_TEXTURE);
        btnreset.setPosition(1197, 654);

        btnreset.setZoomScale(0.01);

        nodeBottom.addChild(btnreset);

        btnreset.addClickEventListener(function () {
            thiz.resetGame();
        });

        this.song = ["nu", "na", "nu", "nong",
            "danh", "trong", "phat", "co",
            "mo", "cuoc", "thi", "dua", "chan", "ai", "sach", "se", "got", "do", "hong", "hao", "khong", "ban", "ty", "nao", "duoc",
            "vao", "danh", "trong"
        ]
        this.words = this.song.slice();
        this.listPlayers = [];
        var thiz = this;
        for (var i = 0; i < listPlay.length; i++) {
            (function () {
                var inew = i;
                var tile = (4 / 5);

                var idx = Math.round(Math.random() * 5);
                var btnchan = new ccui.Button("n" + (idx + 1) + "_chan.png", "", "", ccui.Widget.PLIST_TEXTURE);
                var xpos = 1280 / 2 - (btnchan.getNormalTextureSize().width * 2 * tile * listPlay.length) / 2 + 120;
                btnchan.setPosition((xpos + inew * 300) * tile, 0);
                btnchan.setAnchorPoint(cc.p(0.5, 0));
                btnchan.setZoomScale(0.01);
                btnchan.setScale(tile);
                btnchan.indexColor = idx;
                btnchan.typePlayer = 0;
                btnchan.indexPlayer = inew;
                btnchan.isStraight = true;
                btnchan.isTouched = false;
                btnchan.addClickEventListener(function () {
                    thiz.touchChan(btnchan);
                });

                var btnle = new ccui.Button("n" + (idx + 1) + "_chan.png", "", "", ccui.Widget.PLIST_TEXTURE);
                btnle.setPosition(157 * tile + btnchan.x, 0);
                btnle.setAnchorPoint(cc.p(0.5, 0));
                btnle.setZoomScale(0.01);
                btnle.indexColor = idx;
                btnle.isStraight = true;
                btnle.typePlayer = 1;
                btnle.indexPlayer = inew;
                btnle.isTouched = false;
                btnle.setScale(btnchan.getScale());
                btnle.setScaleX(-btnchan.getScaleX());
                btnle.addClickEventListener(function () {
                    thiz.touchChan(btnle);
                });

                nodeChan.addChild(btnle);
                nodeChan.addChild(btnchan);


                var lobby_bg_text = new cc.Sprite("#bg_text2.png");
                lobby_bg_text.setPosition(79 * tile + btnchan.x, 0);
                lobby_bg_text.setAnchorPoint(cc.p(0.5, 0));
                lobby_bg_text.setScale(btnchan.getScale());
                nodeChan.addChild(lobby_bg_text);

                var lbl = new cc.LabelTTF(listPlay[inew], "", 30);
                lbl.setPosition(cc.p(lobby_bg_text.width / 2, lobby_bg_text.height / 2));
                lbl.setColor(cc.color(255, 255, 255, 255));
                lobby_bg_text.addChild(lbl);


                var obTemp = {"l": btnchan, "r": btnle};
                thiz.listPlayers.push(obTemp);
            })();


        }


    },

    resetGame: function () {
        for (var i = 0; i < this.listPlayers.length; i++) { // kiem tra chan truoc do
            var btnchan = this.listPlayers[i]['l'];
            btnchan.isStraight = true;
            btnchan.isTouched = false;
            btnchan.setTouchEnabled(true);

            var idx = Math.round(Math.random() * 5);
            cc.log(idx);
            btnchan.indexColor = idx;
            btnchan.loadTextureNormal("n" + (idx + 1) + "_chan.png", ccui.Widget.PLIST_TEXTURE);
            var btnphai = this.listPlayers[i]['r'];
            btnphai.isStraight = true;
            btnphai.isTouched = false;
            btnphai.indexColor = idx;
            btnphai.setTouchEnabled(true);
            btnphai.loadTextureNormal("n" + (idx + 1) + "_chan.png", ccui.Widget.PLIST_TEXTURE);

            if (btnchan.lbl != undefined && btnchan.lbl != null) {
                btnchan.lbl.removeFromParent(true);
            }

            if (btnphai.lbl != undefined && btnphai.lbl != null) {
                btnphai.lbl.removeFromParent(true);
            }

        }
        this.words = this.song.slice();
    },

    fillOrderInTurn: function () {
        for (var i = 0; i < this.listPlayers.length; i++) {

        }
    },

    checkFinishTurn: function () {

    },

    touchChan: function (chan) {

        if (chan.isStraight === true) {

            var isPermit = true;
            for (var i = 0; i < chan.indexPlayer; i++) { // kiem tra chan truoc do

                if ((this.listPlayers[i]["l"].isTouched === false || this.listPlayers[i]["r"].isTouched === false)) {
                    isPermit = false;
                    break;
                }
            }
            if (isPermit === true && chan.typePlayer === 1) { // if  chan phai kiem tra chan trai
                if (this.listPlayers[chan.indexPlayer]["l"].isTouched === false && this.listPlayers[chan.indexPlayer]["l"].isStraight) {
                    isPermit = false;
                }
            }
            if (chan.isTouched) {
                isPermit = false;
            }


            if (isPermit) {
                chan.isTouched = true;
                this._audioSource.stopAllSound();
                this._audioSource.playSound("nunanunong/" + this.words[0]);
                var lbl = new cc.Sprite("#" + this.words[0] + ".png");
                lbl.setPosition(cc.p(75, 159));
                // lbl.setColor(cc.color(255,222,0,255));
                lbl.setScaleX(chan.typePlayer === 1 ? -lbl.getScaleX() : lbl.getScaleX());
                chan.addChild(lbl);
                chan.lbl = lbl;
                this.words.splice(0, 1);

                //end of chanx

                var isEndroud = false;
                if (this.words.length === 0) { // end of round
                    chan.isStraight = false;
                    chan.setTouchEnabled(false);
                    chan.loadTextureNormal("n" + (chan.indexColor + 1) + "_chanb.png", ccui.Widget.PLIST_TEXTURE);
                    isEndroud = true;
                    chan.lbl.removeFromParent(true);

                }
                if (!isEndroud) {
                    if (this.checkIsEndOfLivePlayer(chan)) {
                        // reset toan bo chan con sng
                        this.resetTouchAlive();
                    }
                } else {
                    this.resetTouchAlive();
                    this.words = this.song.slice();
                }

                this.checkGameFinish();


            } else {
                var aa = new Dialog("Bạn phải chơi lần lượt từ trái qua phải");
                aa.show();
                cc.log("khong hop le, phai touch tu dau den cuoi");
            }


        }
    },

    checkGameFinish() {
        var thiz = this;
        for (var i = 0; i < this.listPlayers.length; i++) {
            var chantrai = this.listPlayers[i]['l'];
            var chanphai = this.listPlayers[i]['r'];
            if (!chantrai.isStraight && !chanphai.isStraight) {

                var dia = new Dialog("Chúc mừng bạn " + this.nameList[chantrai.indexPlayer] + " đã chiến thắng");
                var btnBack = new ccui.Button("btn_choiLai.png", "", "", ccui.Widget.PLIST_TEXTURE);
                btnBack.setPosition(317, -92);

                btnBack.setZoomScale(0.01);

                dia.dialogBg.addChild(btnBack);
                dia.closeButton.addClickEventListener(function () {
                    dia.hide();
                    thiz.resetGame();
                });

                btnBack.addClickEventListener(function () {
                    dia.hide();
                    thiz.resetGame();
                });
                dia.show();
                break;
            }

        }
    },

    resetTouchAlive() {
        for (var i = 0; i < this.listPlayers.length; i++) {
            if (this.listPlayers[i]['l'].isStraight) {
                this.listPlayers[i]['l'].isTouched = false;
                if (this.listPlayers[i]['l'].lbl != undefined && this.listPlayers[i]['l'].lbl != null) {
                    this.listPlayers[i]['l'].lbl.removeFromParent(true);
                }
            }
            if (this.listPlayers[i]['r'].isStraight) {
                this.listPlayers[i]['r'].isTouched = false;
                if (this.listPlayers[i]['r'].lbl != undefined && this.listPlayers[i]['r'].lbl != null) {
                    this.listPlayers[i]['r'].lbl.removeFromParent(true);
                }
            }
        }
    },


    checkIsEndOfLivePlayer(chan) {
        var isEnd = true;
        //check sau no
        if (chan.typePlayer == 0 && this.listPlayers[chan.indexPlayer]["r"].isStraight) { // chan trai
            return false;
        }
        for (var i = chan.indexPlayer; i < this.listPlayers.length; i++) {
            if ((this.listPlayers[i]['l'].isStraight && !this.listPlayers[i]['l'].isTouched) || (this.listPlayers[i]['r'].isStraight && !this.listPlayers[i]['r'].isTouched)) {
                isEnd = false;
                break;
            }
        }
        return isEnd;

    },

    onEnter: function () {
        this._super();


    }
});


var IDialog = cc.Node.extend({
    ctor : function () {
        this._super();
        this._isShow = false;
        this._moveEnable = false;
        this.mTouch = cc.rect(0,0,0,0);
        this.setAnchorPoint(cc.p(0.5, 0.5));

        this._maxLeft = 0;
        this._maxRight = cc.winSize.width;
        this._maxBottom = 0;
        this._maxTop = cc.winSize.height;

    },
    onCanvasResize : function () {
        this.colorLayer.setContentSize(cc.winSize);
        if(cc.winSize.height > 1000){
            this.setPosition(cc.winSize.width/2, cc.winSize.height - 500);
        }
        else{
            this.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        }
    },

    _onKeyBackHandler : function () {
        this.hide();
        return true;
    },

    adjustlel : function () {

    },
    show : function (rootNode) {
        this._isShow = true;
        var parentNode = this.getParent();
        if(parentNode){
            this.removeFromParent(true);
            parentNode.removeFromParent(true);
            parentNode = null;
        }

        if(!rootNode){
            rootNode = cc.director.getRunningScene();
        }

        if(rootNode){
            if(rootNode.popupLayer){
                parentNode = rootNode.popupLayer;
            }
            else{
                parentNode = rootNode;
            }

            this.setPosition(cc.winSize.width/2, cc.winSize.height/2);
            if(!this._bgColor){
                this._bgColor = cc.color(0,0,0,200);
            }
            var colorLayer = new cc.LayerColor(this._bgColor, cc.winSize.width, cc.winSize.height);
            this.colorLayer = colorLayer;
            colorLayer.addChild(this);
            parentNode.addChild(colorLayer);
        }
    },
    showWithAnimationScale : function () {
        Dialog.prototype.show.apply(this, arguments);

        var defaultScale = this.getScale();
        this.setScale(0.0);
        var scaleAction = new cc.EaseElasticOut(new cc.ScaleTo(0.7, defaultScale));
        this.runAction(scaleAction);
    },
    showWithAnimationMove : function () {
        Dialog.prototype.show.apply(this, arguments);
        var currentPosition = this.getPosition();
        this.y = cc.winSize.height + this.getContentSize().height/2;
        var moveAction = new cc.EaseBounceOut(new cc.MoveTo(0.7, currentPosition));
        this.runAction(moveAction);
    },
    hide : function () {
        this._isShow = false;
        var parent = this.getParent();
        if(parent && !this.dontclosepoup){
            this.removeFromParent(true);
            parent.removeFromParent(true);
        }
    },

    isShow : function () {
        //return this._running;
        return this._isShow;
    },

    onTouchDialog : function () {

    },

    checkVisible: function () {
        var mNode = this;
        while(mNode){
            if(!mNode.isVisible()){
                return false;
            }
            mNode = mNode.getParent();
        }
        return true;
    },

    onExit : function () {
        this._super();
        this._isShow = false;
    },

    onEnter : function () {
        this._super();
        this._isShow = true;

        this._onMouseMove = function () {
            if(this.checkVisible()){
                return true;
            }
            return false;
        };
        // this.setMouseOverEnable(true);

        var thiz = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches:true,
            onTouchBegan : function (touch, event) {
                return thiz.onTouchBegan(touch, event);
            },
            onTouchMoved : function (touch, event){
                thiz.onTouchMoved(touch, event);
            },
            onTouchEnded : function (touch, event) {
                thiz.onTouchEnded(touch, event);;
            }
        }, this);

        // newui._addMouseScrollEvent(this, function () {
        //     return true;
        // });
    },

    onTouchBegan : function (touch, event) {
        if(!this.checkVisible()){
            return false;
        }
        if(this._moveEnable){
            var p = this.convertToNodeSpace(touch.getLocation());
            if(cc.rectContainsPoint(this.mTouch, p)){
                this.onTouchDialog();
                return true;
            }
            return false;
        }
        else{
            var p = this.convertToNodeSpace(touch.getLocation());
            if(cc.rectContainsPoint(this.mTouch, p)){
                this._touchInside = true;
                this.adjustlel();
            }
            return true;
        }
        return false;
    },

    onTouchMoved : function (touch, event){
        if(this._moveEnable){
            this.moveDialog(touch.getDelta());
        }
    },

    onTouchEnded : function (touch, event) {
        if(this._moveEnable){

        }
        else{
            if(this._touchInside){
                this._touchInside = false;
                return;
            }
            var p = this.convertToNodeSpace(touch.getLocation());
            if(!cc.rectContainsPoint(this.mTouch, p)){
                // this.hide();
            }
        }
        cc.log(this._moveEnable);
    },

    moveDialog : function (ds) {
        this.x += ds.x;
        this.y += ds.y;
        if(this.x < this._maxLeft){
            this.x = this._maxLeft;
        }
        if(this.x > this._maxRight){
            this.x = this._maxRight;
        }
        if(this.y < this._maxBottom){
            this.y = this._maxBottom;
        }
        if(this.y > this._maxTop){
            this.y = this._maxTop;
        }
    }
});

var Dialog = IDialog.extend({
    ctor : function (content) {
        this._super();
        this._marginLeft = 0.0;
        this._marginRight = 0.0;
        this._marginTop = 0.0;
        this._marginBottom = 0.0;

        this.dontclosepoup = false;

        var dialogBg = new cc.Sprite("#bg_dialog.png");
        this.setContentSize(dialogBg.getContentSize());
        var mSize = dialogBg.getContentSize();
        dialogBg.setPosition(cc.p(this.width/2,this.height/2));
        // dialogBg.setAnchorPoint(cc.p(0.0,0.0));
        this.addChild(dialogBg);



        var lbl = new cc.LabelTTF( content, "",30);
        lbl.setPosition(dialogBg.width/2,dialogBg.height/2);
        lbl.setColor(cc.color(0,0,0,255));
        dialogBg.addChild(lbl);
        // var title = cc.Label.createWithBMFont(cc.res.font.font_utmfacebookR_30, "");
        //
        // this.addChild(title,10);


        var thiz = this;


        var closeButton = new ccui.Button("close_dialog.png","","", ccui.Widget.PLIST_TEXTURE);
        closeButton.setPosition(cc.p(622,194));
        dialogBg.addChild(closeButton, 10);
        closeButton.addClickEventListener(function () {
            if(thiz.closeButtonHandler){
                thiz.closeButtonHandler();
            }
        });


        this.dialogBg = dialogBg;

        this.closeButton = closeButton;
        this._paddingBottom = 0;



        this.mTouch = cc.rect(this._marginLeft, this._marginBottom, mSize.width - this._marginRight, mSize.height - this._marginTop);

        var dialogWidth = this.width + 70;
        if(cc.winSize.width < dialogWidth){
            this.setScale(cc.winSize.width / dialogWidth);
        }


    },





    closeButtonHandler : function () {
        this.hide();
    }
});

