/**
 * Created by ext on 8/9/2016.
 */

var SceneNavigator = SceneNavigator || {};
SceneNavigator.toHome = function (message) {
    MiniGameNavigator.hideAll();
    cc._mainScene.removeAllPopup();

    var runningScene = SceneNavigator.getRunningScene();
    if (!(runningScene instanceof HomeScene)) {
        runningScene = new HomeScene();
        SceneNavigator.replaceScene(runningScene);
    }
    runningScene.startHome();

    if (message) {
        var dialog = new MessageDialog();
        dialog.setMessage(message);
        dialog.show();
    }
    if (PlayerMe.isLogin()) {
        //call SceneNavigator.toHome from bameBai
        PlayerMe.isSocketLogin = false;
        GameClient.getInstance().close();
    }
    LobbyClient.getInstance().logOut();

};

SceneNavigator.toLobbyCardScene = function (gameId, msg) {
    var runningScene = SceneNavigator.getRunningScene();
    if (!(runningScene instanceof HomeScene)) {
        runningScene = new HomeScene();
        SceneNavigator.replaceScene(runningScene);
    }
    runningScene.startCardGame(gameId);

    if (msg && msg.length) {
        var dialog = new MessageDialog();
        dialog.setMessage(msg);
        dialog.show(runningScene);
    }


};

cc._mainScene = null;
SceneNavigator.replaceScene = function (newScene) {
    // if (cc._mainScene === null) {
    //     cc._mainScene = new MainScene();
    //     cc.director.replaceScene(cc._mainScene);
    // }
    cc.director.replaceScene(newScene);
};

SceneNavigator.getRunningScene = function () {
    if (cc._mainScene === null) {
        cc._mainScene = new MainScene();
        cc.director.replaceScene(cc._mainScene);
    }
    return cc._mainScene.getRunningScene();
};

SceneNavigator.addBackKeyEvent = function (target) {
    //init global key boardlistener
    if (!SceneNavigator._initGlobalKeyboardEvent) {
        SceneNavigator._initGlobalKeyboardEvent = true;

        var listener = cc.EventListener.create({
            event: cc.EventListener.KEYBOARD,
            onKeyReleased: function (keyCode, event) {
                if (cc.sys.isNative) {
                    if (parseKeyCode(keyCode) === cc.KEY.back) {
                        //cc.log("deptrai1");
                        cc.eventManager.dispatchCustomEvent("keyBackPressed", {used: false});
                    }
                } else {
                    if (keyCode === cc.KEY.escape) {
                        // cc.log("deptrai2");
                        cc.eventManager.dispatchCustomEvent("keyBackPressed", {used: false});
                    }
                }
            }
        });
        cc.eventManager.addListener(listener, 1);
    }


    if (target._onKeyBackHandler === undefined) {
        target._onKeyBackHandler = function () {
            cc.log("onKeyBackHandler");
        };
    }

    var _onEnter = target.onEnter;
    var _onExit = target.onExit;

    target.onEnter = function () {
        _onEnter.apply(target, arguments);

        cc.eventManager.addListener({
            event: cc.EventListener.CUSTOM,
            eventName: "keyBackPressed",
            callback: function (e) {
                if (!e._userData["used"]) {
                    if (target._onKeyBackHandler()) {
                        e._userData["used"] = true;
                    }
                }
            }
        }, target);
    };

    target.onExit = function () {
        _onExit.apply(target, arguments);
        cc.eventManager.removeListeners(target);
    };

};

SceneNavigator.resizeEvent = function (view) {
    if (!cc.sys.isNative) { //add for web
        var _onEnter = view.onEnter;
        if (view.onCanvasResize === undefined) {
            view.onCanvasResize = function () {
                cc.log("onCanvasResize")
            };
        }
        view.onEnter = function () {
            _onEnter.apply(view, arguments);

            view.onCanvasResize();
            cc.eventManager.addListener({
                event: cc.EventListener.CUSTOM,
                eventName: "canvas-resize",
                callback: function () {
                    view.onCanvasResize();
                }
            }, view);
        };
    }
};

SceneNavigator.startGame = function (gameId) {
    if (gameId === GameType.MiniGameNone)
        return;
    if (gameId === GameType.DaBong) {
        ToastDialog.getInstance().show("Game sắp được cập nhật");
        return;
    }

    var moduleName = GameModuleName[gameId];
    var module = ModuleManager.getInstance().getModule(moduleName);
    if (module) {
        if (!module.isLoaded()) {
            if (module._isLoading === true) {
                return;
            }
            module._isLoading = true;
            GlobalEvent.getInstance().postEvent("onLoadModule",
                {
                    module: moduleName,
                    current: 0,
                    target: 100
                }
            );

            var thiz = this;
            var args = arguments;
            SceneNavigator.loadGameWithCallback(gameId, function (success) {
                if (success) {
                    SceneNavigator.startGame.apply(thiz, args);
                }
                module._isLoading = false;
            });
            return;
        }
    } else {
        //load game no module

    }

    if (gameId < 100) {
        var runningScene = new SceneNavigator.getRunningScene();
        if (!(runningScene instanceof HomeScene)) {
            return;
        }
    }

    if (!PlayerMe.isLogin()) {
        ToastDialog.getInstance().show("Bạn chưa đăng nhập");
        return;
    }

    //create game scene
    if (gameId > 100) {
        MiniGameNavigator.showGame.apply(this, arguments);
    } else {
        var runningScene = SceneNavigator.getRunningScene();
        if (runningScene instanceof HomeScene) {
            if (gameId === GameType.Slot20) {
                cc.error("GameType.Slot20", GameType.Slot20);
                SceneNavigator.replaceScene(new Slot20SelectScene());
            } else if (gameId === GameType.Slot20_2) {
                cc.error("GameType.Slot20_2", GameType.Slot20_2);
                SceneNavigator.replaceScene(new SlotTQSelectBetting());
            } else if (gameId === GameType.Slot25) {
                cc.error("GameType.Slot25", GameType.Slot25);
                SceneNavigator.replaceScene(new Slot25SelectBetting());
            } else if (gameId === GameType.SlotWild) {
                cc.error("GameType.SlotWild", GameType.SlotWild);
                SceneNavigator.replaceScene(new SlotWildSelectBetting());
            } else if (gameId === GameType.SlotWild_2) {
                cc.error("GameType.SlotWild_2", GameType.SlotWild_2);
                SceneNavigator.replaceScene(new SlotWild2SelectBetting());
            } else if (gameId === GameType.DaGa) {
                SceneNavigator.replaceScene(new DaGaScene());
            } else if (gameId === GameType.LoSo) {
                SceneNavigator.replaceScene(new LoSoScene());
            } else if (gameId === GameType.BanCa) {
                LoadingDialog.getInstance().show("Đang tải");
                PlayerMe.requestRefreshToken(function () {
                    LoadingDialog.getInstance().hide();
                    if (cc.sys.isNative) {
                        cc.view.setDesignResolutionSize(1280, 720, cc.ResolutionPolicy.SHOW_ALL);
                    }
                    SceneNavigator.replaceScene(new BanCaView());
                });
            } else if (GameType.GAME_BAI_ARRAY.indexOf(gameId) != -1) {
                if (IS_MAINTAIN_CARD_GAME) {
                    var dialog = new MessageOkDialog();
                    dialog.show();
                    dialog.setMessage("Game đang bảo trì");
                    return;
                }
                SceneNavigator.toLobbyCardScene(gameId);
            }
        }
    }
};
SceneNavigator.startGameBai = function (gameData, gameId) {

    if (gameId == GameType.MAUBINH) {
        SceneNavigator.replaceScene(new MauBinhScene(gameData, gameId));
    } else if (gameId == GameType.TLMN) {
        SceneNavigator.replaceScene(new TLMNScene(gameData, gameId));
    } else if (gameId == GameType.SAM) {
        SceneNavigator.replaceScene(new SamScene(gameData, gameId));
    } else if (gameId == GameType.PHOM) {
        SceneNavigator.replaceScene(new PhomScene(gameData, gameId));
    } else if (gameId == GameType.XOCDIA) {
        SceneNavigator.replaceScene(new XocDiaScene(gameData, gameId));
    } else if (gameId == GameType.POKER) {
        SceneNavigator.replaceScene(new PokerScene(gameData, gameId));
    } else if (gameId == GameType.XITO) {
        SceneNavigator.replaceScene(new XitoScene(gameData, gameId));
    } else if (gameId == GameType.LIENG) {
        SceneNavigator.replaceScene(new LiengScene(gameData, gameId));
    } else if (gameId == GameType.CHAN) {
        SceneNavigator.replaceScene(new ChanCaScene(gameData, gameId));
    } else if (gameId == GameType.BACAY) {
        SceneNavigator.replaceScene(new BaCayScene(gameData, gameId));
    } else {
        ToastDialog.getInstance().show("SceneNavigator.startGameBai game chua lam ");
    }
};

var s_loadGameCallbackInstanceId = 0;
SceneNavigator.loadGameWithCallback = function (gameId, cb) {
    var moduleName = GameModuleName[gameId];
    var module = ModuleManager.getInstance().getModule(moduleName);
    if (!module.isLoaded()) {
        s_loadGameCallbackInstanceId++;
        var obj = {
            instanceId: s_loadGameCallbackInstanceId
        };
        obj.finishedCallback = function (eventName, data) {
            if (data["module"] === moduleName) {
                var status = data["status"];
                if (status === ModuleStatus.LoadResourceFinished) {
                    GlobalEvent.getInstance().removeListener(obj);
                    cb(true);
                } else if (
                    status === ModuleStatus.UpdateFailure ||
                    status === ModuleStatus.LoadResourceFailure
                ) {
                    GlobalEvent.getInstance().removeListener(obj);
                    cb(false);
                    // setTimeout(function () {
                    //     SceneNavigator.loadGameWithCallback(cb);
                    // }, 1000);
                }
            }
        };
        GlobalEvent.getInstance().addListener("onLoadModuleStatus", obj.finishedCallback, obj);
        module.loadModule();
    } else {
        cb(true);
    }
};

window._cc_finished_Loading = function () {
    SceneNavigator.replaceScene(new HelloWorldScene());
};